Date.prototype.monthNameByDay = [
    "января", "февраля", "марта",
    "апреля", "мая", "июня",
    "июля", "августа", "сентября",
    "октября", "ноября", "декабря"
];

Date.prototype.getMonthNameByDay = function(month) {
  if (month === 0 || month > 12)
    throw new Error('Wrong month! 1 <= month <= 12');
    return this.monthNameByDay[month - 1];
};

Date.prototype.normalizeDateString = function(date) {
  return ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
};

Date.prototype.fromNow = function(date) {
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
};

Date.prototype.diffMinutes = function (date1, date2) {
  let difference = date1 - date2;
  return Math.round(difference / 60000);
};



