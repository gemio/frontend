export default class UserAction {
  static logOut() {
    window.location.replace(`${process.env.URL_FOLDER}login`);
    return {
      type: 'LOG_OUT'
    }
  }

  static authorizeWithToken(token) {
    return dispatch => {
      dispatch({type: 'AUTH_REQUESTED'});
      fetch(`${process.env.API_URL}/user/auth/${token}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        if (response.status === 404 || response.status ===  500) {
          dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: 'Сервер не отвечает'}});
          return;
        }

        response.json().then((data) => {
          if (response.status === 200) {
            dispatch({type: 'AUTH_SUCCESS', payload: {authorized: true, info: data.data}});
            window.socket = window.io(process.env.LIVE_HOST, {
              path: process.env.LIVE_PATH,
              query: {
                token: data.data._id
              },
              reconnection: true,
              reconnectionDelay: 1000,
              reconnectionDelayMax: 5000,
              reconnectionAttempts: 10,
              pingInterval: 2000,
              pingTimeout: 5000
            });
            window.location.replace(`${process.env.URL_FOLDER}home`);
          }
        });
      }).catch((response) => {
        dispatch({type: 'SET_MESSAGE', payload: response});
      });
    }
  }

  static authorize(user) {
    return dispatch => {
      dispatch({type: 'AUTH_REQUESTED'});
      fetch(`${process.env.API_URL}/user/auth?login=${user.login}&password=${user.password}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        if (response.status === 404 || response.status ===  500) {
          dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: 'Сервер не отвечает'}});
          return;
        }

        response.json().then((data) => {
          if (response.status === 200) {
            dispatch({type: 'AUTH_SUCCESS', payload: {authorized: true, info: data.data}});
            window.socket = window.io(process.env.LIVE_HOST, {
              path: process.env.LIVE_PATH,
              query: {
                token: data.data._id
              },
              reconnection: true,
              reconnectionDelay: 1000,
              reconnectionDelayMax: 5000,
              reconnectionAttempts: 10,
              pingInterval: 2000,
              pingTimeout: 5000
            });
            window.location.replace(`${process.env.URL_FOLDER}home`);
          } else if (response.status === 423) {
            if (data.message.message.includes('"Гемофилик" является закрытым сообществом')) {
              dispatch({type: 'AUTH_NEED_DOCUMENTS', payload: {authorized: false, info: data.data}});
              window.location.href = '/auth-confirm-step';
            } else dispatch({type: 'SET_MESSAGE', payload: data.message});
          } else
            dispatch({type: 'SET_MESSAGE', payload: data.message});
        });
      }).catch((response) => {
        dispatch({type: 'SET_MESSAGE', payload: response});
      });
    }
  }

  static updatePivacyInformation(info) {
    return dispatch => {
      dispatch({type: 'UPDATE_PRIVACY_REQUESTED'});
      fetch(`${process.env.API_URL}/user/privacy`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `login=${info.login}&token=${info.token}&allowReadForm=${info.allowReadForm}&allowReadContacts=${info.allowReadContacts}&allowFriendRequest=${info.allowFriendRequest}&allowComments=${info.allowComments}&allowMessages=${info.allowMessages}`
      }).then(response => {
        if (response.status === 404 || response.status ===  500) {
          dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: 'Сервер не отвечает'}});
          return;
        }

        response.json().then(data => {
          if (response.status === 200) {
            dispatch({type: 'AUTH_SUCCESS', payload: {authorized: true, info: data.data}});
            dispatch({type: 'SET_MESSAGE', payload: data.message});
          }
          else dispatch({type: 'SET_MESSAGE', payload: response});
        });
      }).catch(err => {
        dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: err}});
      });
    }
  }

  static updateUserInformation(info) {
    return dispatch => {
      dispatch({type: 'UPDATE_USER_REQUESTED'});
      fetch(`${process.env.API_URL}/user/public`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `login=${info.login}&token=${info.token + (info.roleSpecialist ? `&roleSpecialist=${info.roleSpecialist}` : '')}&status=${info.status}&phone=${info.phone}&fullName={"name":"${info.fullName.name}","surname":"${info.fullName.surname}","lastname":"${info.fullName.lastname}"}&birthday=${info.birthday}&city=${info.city}&hemoStatus=${info.hemoStatus}&about=${info.about}&social={"vk":"${info.social.vk}", "facebook":"${info.social.facebook}", "google":"${info.social.google}"}`
      }).then(response => {
        if (response.status === 404 || response.status ===  500) {
          dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: 'Сервер не отвечает'}});
          return;
        }

        response.json().then(data => {
          if (response.status === 200) {
            dispatch({type: 'AUTH_SUCCESS', payload: {authorized: true, info: data.data}});
            dispatch({type: 'SET_MESSAGE', payload: data.message});
          }
          else dispatch({type: 'SET_MESSAGE', payload: response});
        });
      }).catch(error => {
        dispatch('UPDATE_USER_FAILED');
      });
    }
  }

  static validateUser(info) {
    return dispatch => {
      dispatch({type: 'VALIDATE_USER_REQUEST'});
      fetch(`${process.env.API_URL}/security/${info._id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `user=${JSON.stringify(info)}`
      }).then(response => {
        if (response.status === 404 || response.status ===  500) {
          dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: 'Сервер не отвечает'}});
          return;
        }

        response.json().then(data => {
          if (response.status === 302 || response.status === 200) {
            if (response.status === 302)
              dispatch({type: 'AUTH_SUCCESS', payload: {authorized: true, info: data.data}});
            dispatch({type: 'SET_MESSAGE', payload: data.message});
          } else
            dispatch({type: 'SET_MESSAGE', payload: response});
        });
      }).catch(err => {
        throw new Error(err);
        //dispatch({type: 'LOG_OUT'});
      });
    };
  }

  static updateStatus(user) {
    return dispatch => {
      dispatch({type: 'AUTH_SUCCESS', payload: {authorized: true, info: user}});
    }
  }
}
