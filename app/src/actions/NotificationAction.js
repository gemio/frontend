export default class NotificationAction {
  static updateNotification(notificationObject) {
    return {
      type: 'NOTIFICATION_UPDATE',
      payload: notificationObject.notification
    };
  }

  static getNotification(user) {
    return dispatch => {
      dispatch({type: 'NOTIFICATION_REQUESTED'});
      fetch(`${process.env.API_URL}/notification/${user.info._id}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then(response => {
        if (response.status === 404 || response.status ===  500) {
          dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: 'Сервер не отвечает'}});
          return;
        }

        response.json().then(data => {
          if (response.status === 200) dispatch(this.updateNotification({notification: data.data}));
          else dispatch({type: 'SET_MESSAGE', payload: data.message});
        }).catch(err => {
          dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: err}});
        })
      }).catch(err => {
        dispatch({type: 'SET_MESSAGE', payload: {type: 'error', message: err}});
      });
    };
  }
}
