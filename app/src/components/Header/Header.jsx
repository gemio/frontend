import React from 'react';
import Nav from '../Nav/Nav';
import HeaderProfile from '../HeaderProfile';

import './Header.scss';

class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <Nav className="header__nav">
          {this.props.children}
        </Nav>
        <HeaderProfile />
      </header>
    );
  }
}

export default Header;
