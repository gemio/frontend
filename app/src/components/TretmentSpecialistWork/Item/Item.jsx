import React from 'react';
import {connect} from 'react-redux';
import AvatarCircle from "../../AvatarCircle/AvatarCircle";
import Button from '../../Button';
import './Item.scss';

class Item extends React.Component {
  AddFunction = () => {
    fetch(`${process.env.API_URL}/doctor/accept/${this.props.user.info._id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `userID=${this.props.link}`
    }).then(response => {
        window.location.reload();
    });
  };

  render() {
    return (
      <div className="TretmantSpecialistWork__Item">
        <a href={`/id/${this.props.link}`}>
          <div className="TretmantSpecialistWork__Item__row">
            <AvatarCircle img={this.props.img} className="TretmantSpecialistWork__Item__row__avatar"/>
            <h2 className="TretmantSpecialistWork__Item__row__header">{this.props.name}</h2>
          </div>
        </a>

        <div className="TretmantSpecialistWork__Item__row__2">
          <Button onClick={this.AddFunction} className=" button--primary-ghost" text="Добавить" />
        </div>
      </div>
    );
  }
}
export default connect(
  state => ({
    user: state.user
  }),
)(Item);