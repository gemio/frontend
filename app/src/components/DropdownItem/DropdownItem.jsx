import React from 'react';
import './DropdownItem.scss';

class DropdownItem extends React.Component {
  render() {
    return (
      <option value={this.props.value} className={(this.props.className || '') + ' dropdown__item'}>
        {this.props.text}
      </option>
    );
  }
}

export default DropdownItem;