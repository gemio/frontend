import React from 'react';
import InputMask from 'react-input-mask';
import './InputSimple.scss';

export default class InputSimple extends React.Component {
  state = {
    value: this.props.onlyNum ? this.props.min : this.props.value || '',
    isReadOnly: this.props.isReadOnly || false
  };

  componentWillReceiveProps(nextProps) {
    if (!nextProps.onlyNum) {
      this.setState({value: nextProps.value || ''});
    }
  }

  handleChange = (e) => {
    if (!this.props.isReadOnly) {
      if (this.props.onlyNum) {
        if ((e.target.value < 0 || e.target.value > this.props.max))
          return;
        if ((e.target.value !== '' && e.target.value !== '1')) {
          let value = e.target.value;
          if (!(!isNaN(parseFloat(value)) && isFinite(value)))
            return;
        }
      }
      this.setState({value: e.target.value});
    }
  };

  render() {
    let alignment = this.props.alignment || 'horizontal';
    let label = (<label className={'input--simple__label input--simple__label--' + alignment}>{this.props.labelText || ''}</label>);

    let input;
    if (this.props.isMultiline)
      input = (<textarea className={(this.props.isLight ? 'light' : '')} id={this.props.id} rows={this.props.rows} cols={this.props.cols} onChange={this.handleChange} value={this.state.value}/>);
    else if (this.props.isMask)
      input = (<InputMask alwaysShowMask={this.props.alwaysShowMask} onChange={this.handleChange} value={this.state.value} inputRef={this.props.inputRef} className={(this.props.isLight ? 'light' : '')} mask={this.props.mask} maskChar=' '/>);
    else
      input = (<input placeholder={this.props.placeholder} className={(this.props.isLight ? 'light' : '')} style={{width: '100%', height: '100%', paddingRight: this.props.onlyNum ? '45px': '0px'}} id={this.props.id} onChange={this.handleChange} value={this.state.value} type={this.props.type || 'text'}/>);

    let onlyNumberLabel;
    if (this.props.onlyNum)
      onlyNumberLabel = (<span className='input--simple__only-num-label'>{this.props.onlyNumLabel}</span>)

    return (
      <div style={this.props.style} className={'input--simple ' + (this.props.className || '')}>
        {label}
        {input}
        {onlyNumberLabel}
        
      </div>
    );
  }
}
