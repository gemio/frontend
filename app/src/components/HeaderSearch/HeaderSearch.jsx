import React from 'react';
import SearchTypeList from '../SearchTypeList';
import {Icon} from 'react-fa';
import './HeaderSearch.scss';

export default class HeaderSearch extends React.Component {
  state = {
    isSearchHidden: true
  };

  toggleSearch = () => {
    this.setState({isSearchHidden: !this.state.isSearchHidden});
  };

  render() {
    const searchInputClass = this.state.isSearchHidden ? 'header__search--hidden' : '';

    return (
      <div className={`header__search ${searchInputClass} ` + (this.props.className || '')}>
        <input placeholder='Поиск' className={`header__search-input`}/>
        <Icon name="search" style={{color: 'white'}} className="header__search-icon" onClick={this.toggleSearch}/>
        <SearchTypeList hidden={this.state.isSearchHidden}/>
      </div>
    );
  }
}
