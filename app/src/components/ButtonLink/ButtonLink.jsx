import React, { Component } from 'react';
import './ButtonLink.scss';

export default class ButtonLink extends Component {
  render() {
    return (
      <span style={this.props.style} onClick={() => {this.props.onClick && this.props.onClick()}} className={'button-link ' + (this.props.className || '')}>{this.props.text}</span>
    );
  }
}