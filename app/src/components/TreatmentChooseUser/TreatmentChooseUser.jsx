import React, {Component} from 'react';
import {connect} from 'react-redux';
import './TreatmentChooseUser.scss';
import {Icon} from 'react-fa';
import Button from '../Button';
import Modal from 'react-modal';
import APIMessage from '../APIMessage';
import InputSimple from '../InputSimple';
import DropdownSimple from '../DropdownSimple';
import DateTimePicker from '../DateTimePicker';
import Dropdown from 'react-simple-dropdown';
import DropdownTrigger  from 'react-simple-dropdown/lib/components/DropdownTrigger';
import DropdownContent  from 'react-simple-dropdown/lib/components/DropdownContent';
import 'react-simple-dropdown/styles/Dropdown.css';
import TreatmentUserItem from "./TreatmentUserItem";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class TreatmentChooseUser extends Component {
  state = {
    active: false,
    selectedUser: undefined,
    acceptedUser: [],
    isAPILoaded: false,
    modalOpened: false,
    errorMessage: ''
  };

  componentDidMount() {
    this.updateUserList();
  }

  updateUserList = () => {
    fetch(`${process.env.API_URL}/doctor/status/${this.props.user.info._id}?status=accepted`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) return;

      response.json().then(data => {
        this.setState({acceptedUser: response.status === 200 ? data.data : data.message.message, isAPILoaded: true});
      });
    });
  };

  _handleUserChange = (item) => {
    this.setState({ selectedUser: item }, () => {
      this.props.userChange(item.userID);
    });
  };

  _handleDrop = () => {
    this.setState({active: !this.state.active})
  };

  _setEditUser = () => {
    this.setState({active: true, selectedUser: undefined}, () => {
      this.props.userChange(undefined);
    });
  };

  toggleModal = () => {
    this.setState({modalOpened: !this.state.modalOpened});
  };

  addRemind = () => {
    const getDate = (date) => {
      date = date.split('   ');
      let tmpDate = date[1].split('.');
      let tmpTime = date[0].split(':');
      return new Date(+tmpDate[2], +tmpDate[1] - 1, +tmpDate[0], +tmpTime[0], +tmpTime[1]);
    };

    let title = document.getElementById('title').value, desc = document.getElementById('description').value, type = document.getElementById('type').value,
      dateStart = document.getElementById('date-start').value, dateEnd = document.getElementById('date-end').value, currentDate = new Date();

    if (!title || !desc || !dateStart || !dateEnd || !type) {
      this.setState({errorMessage: 'Заполните все поля'});
      return;
    }

    if (new Date(dateStart) == 'Invalid Date' || new Date(dateEnd) == 'Invalid date') {
      this.setState({errorMessage: 'Введите корректную дату'});
      return;
    }

    dateStart = getDate(dateStart);
    dateEnd = getDate(dateEnd);

    if (dateStart < currentDate || dateEnd < currentDate) {
      this.setState({errorMessage: 'Дата начала или конца не может быть раньше текущей даты'});
      return;
    } else if (dateStart > dateEnd) {
      this.setState({errorMessage: 'Дата начала не может быть позже чем дата окончания'});
      return;
    }

    const remind = {
      title: title,
      type: type,
      description: desc,
      rangeDate: `${dateStart.toISOString()}IO${dateEnd.toISOString()}`
    };

    fetch(`${process.env.API_URL}/doctor/remind/${this.props.user.info._id}?userID=${this.state.selectedUser.userID}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `remind=${JSON.stringify(remind)}`
    }).then(response => {
      if (response.status === 200) {
        if (this.props.onAddSuccess) {
          this.toggleModal();
          this.props.onAddSuccess(this.state.selectedUser.userID);
        }
      }
    });
  };

  render() {
    const customStylesModal = {
      content : {
        padding: '25px 35px',
        display: 'flex',
        height: 'fit-content',
        width: '600px',
        algorithm: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        top: '50%',
        left: '50%',
        bottom: 'initial',
        right: 'initial',
        transform: 'translate(-50%, -50%)',
        overflow: 'hidden'
      },

      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };

    let userItem, angleIcon = (<Icon style={{marginLeft: 5}} name={this.state.active ? 'angle-up' : 'angle-down'}/>);
    if (this.state.acceptedUser.length > 0) {
      userItem = this.state.acceptedUser.map((user, index) => (
        <TreatmentUserItem key={index} onClick={() => { this._handleUserChange(user) }} {...user}/>
      ));
    }

    return (
      <div className='plan-calendar'>
        {
          !this.state.selectedUser ?
            <Dropdown active={this.state.active} className={"plan-calendar__user-dropdown"}>
              <DropdownTrigger className='plan-calendar__trigger' onClick={this._handleDrop}>Выбрать пользователя {angleIcon}</DropdownTrigger>
              <DropdownContent>
                <ul>{userItem}</ul>
              </DropdownContent>
            </Dropdown> :

            <div className='plan-calendar__user-current'>
              <h4 className='plan-calendar__trigger plan-calendar__trigger--no-cursor'>Выбранный пользователь</h4>
              <TreatmentUserItem {...this.state.selectedUser} handleAdd={this.toggleModal} handleEdit={this._setEditUser} isChoosen={true}/>
            </div>
        }

        <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.modalOpened} onRequestClose={this.toggleModal}>
          <Icon name="times" className="modal__close" onClick={this.toggleModal}/>
          <div className="remind-modal__add-fullName">Пользователь: <strong>{this.state.selectedUser ? this.state.selectedUser.fullName : ''}</strong></div>

          <div className="remind-modal__date">
            <DateTimePicker id='date-start' showTimeSelect={true} title="Дата начала" className="remind-modal__add-datepicker"/>
            <DateTimePicker id='date-end' showTimeSelect={true} title="Дата окончания" className="remind-modal__add-datepicker"/>
          </div>

          <div className="remind-modal__add-field remind-modal__add-type">
            <label>Тип напоминания</label>
            <DropdownSimple id='type' items={['Онлайн-консультация', 'Осмотр специалиста', 'Лабороторные обследования', 'Санаторно-курортное лечение']}/>
          </div>

          <InputSimple labelText='Название напоминания' id='title' className="remind-modal__add-field"/>
          <InputSimple isMultiline={true} rows='5' labelText='Детальное описание' id='description' className="remind-modal__add-area"/>

          <APIMessage className='remind-modal__message' text={this.state.errorMessage} status='error'/>
          <Button onClick={this.addRemind} text='Добавить напоминание' className='remind-modal__add-button button--primary-small-round'/>
        </Modal>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(TreatmentChooseUser);
