import React, {Component} from 'react';
import AvatarCircle from '../AvatarCircle';
import {Icon} from 'react-fa';
import './TreatmentUserItem.scss';

export default class TreatmentUserItem extends Component {
  render() {
    return (
      <li onClick={this.props.onClick} className={'plan-treatment__user-item'}>
        <AvatarCircle className='plan-treatment__user-avatar' img={this.props.avatar}/>
        <div className="plan-treatment__user-fullName">{this.props.fullName}</div>

        {
          this.props.isChoosen ?
            <div className='plan-treatment__user-actions'>
              <div onClick={this.props.handleAdd} className="plan-treatment__user-add-remind">
                <span>Добавить напоминание</span> <Icon name='plus'/>
              </div>

              <div onClick={this.props.handleEdit} className="plan-treatment__user-change">
                <span>Изменить</span> <Icon name='pencil'/>
              </div>
            </div>: null
        }
      </li>
    );
  }
}
