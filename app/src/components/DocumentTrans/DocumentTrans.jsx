import React from 'react';
import  Avatar from '../AvatarCircle/index';
import "./DocumentTrans.scss";
import Card from '../Card';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import Button from '../Button/index';
import {connect} from "react-redux";
import Modal from 'react-modal';
import {Icon} from 'react-fa';
import PDF from '../PDF';
import "../Admin/UserItem";

class DocumentTrans extends React.Component {
  state = {
    modalIsOpen: false,
    srcimage: ""
  };

  openModal = (src) => {
    this.setState({modalIsOpen: true, srcimage: src});
  };

  closeModal = () => {
    this.setState({modalIsOpen: false, srcimage: ""});
  };

  Accept = (id) => {
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/updateDocumentStatus/${this.props.index}/${id}?status=verified`, {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      window.location.reload();
    });
  };

  Cancel = (id) => {
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/updateDocumentStatus/${this.props.index}/${id}?status=canceled`, {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      console.log(response);
      window.location.reload();
    });
  };

  render() {
    const customStylesModal = {
      content : {
        maxWidth: '670px',
        top: '50px',
        left: '50%',
        right: 'auto',
        marginRight: '-50%',
        transform: 'translateX(-50%)',
        zIndex: '10000',
        maxHeight: 'max-content',
        padding: '34px'
      },

      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };
    return (
        <Card className={"DocumentTrans"}>
          <div className={"DocumentTrans__flex"}>
            <a href={"/id/"+this.props.index}> <Avatar img={this.props.image} className="DocumentTrans__Avatar" /></a>
            <div className={"DocumentTrans__Avatar__name"}>
              <a href={"/id/"+this.props.index} >{this.props.name}</a>
            </div>
          </div>
          <div className={"DocumentTrans__Doc"}>
            <div className={"DocumentTrans__Doc__ment"}>
              {
                 this.props.document.length > 0 ?
                 this.props.document.map(item => ((
                   <div className={"Image"} key={item.id}>
                     {
                       item.img.includes('pdf') ?
                         <PDF className={"images"} onClick={() => {this.openModal(item.img)}}/> :
                         <img alt='document' src={item.img} className={"images"} onClick={() => {this.openModal(item.img)}} />
                     }
                     <div className={"DocumentTrans__button"}>
                        <Button className={"DocumentTrans__button__btn button--orange-ghost"} onClick={() => {this.Cancel(item.id)}} text='Отклонить'/>
                        <Button className={"DocumentTrans__button__btn button--primary-ghost"}  onClick={() => {this.Accept(item.id)}} text='Подтвердить'/>
                      </div>
                   </div>
                 ))): <CardEmptyTitle>нет документов для подтверждения</CardEmptyTitle>
              }
            </div>
          </div>
          <Modal shouldCloseOnOverlayClick={true}  className={"DocumentTrans__modal"} style={customStylesModal} isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal} >
            {
              this.state.srcimage.includes('pdf') ?
               <PDF className={"Modal__src__UserItem"}/> : <img alt='document' src={this.state.srcimage} className={"Modal__src__UserItem"}/>
            }

            <a className='download-document' href={this.state.srcimage}>Скачать</a>
            <Icon onClick={() => {this.closeModal()}} name='times' className={"IconModal"}/>
          </Modal>
        </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
)(DocumentTrans);