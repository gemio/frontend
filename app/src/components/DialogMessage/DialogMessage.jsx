import React from 'react';
import './DialogMessage.scss';
import AvatarCircle from "../AvatarCircle";

export default class DialogMessage extends React.Component {
  render() {
    return (
      <div className="dialog-message">
        <div className="dialog-message__avatar"><AvatarCircle img={this.props.image} alt='avatar'/></div>
        <div className="dialog-message__info">
          <span className="dialog-message__fullName">{this.props.name}</span>
          <span className="dialog-message__day-ago">{this.props.dateAgo}</span><br/>
          <div className="dialog-message__text">{this.props.text}</div>
        </div>
      </div>
    );
  }
}

