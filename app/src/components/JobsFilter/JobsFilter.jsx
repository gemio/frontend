import React, {Component} from 'react';
import {Icon} from 'react-fa';
import Select from 'react-select';
import './JobsFilter.scss';

export default class JobsFilter extends Component {
  state = {
    collapsed: true,
    filterExp: [],
    filterCity: []
  };

  toggleCollapse = () => {
    this.setState({collapsed: !this.state.collapsed});
  };

  _onFilter = () => {
    fetch(`${process.env.API_URL}/job-search/filter`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `filter=${JSON.stringify({exp: this.state.filterExp, city: this.state.filterCity})}`
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (this.props.onFilter) this.props.onFilter(response);
    });
  };

  onFilterExpApply = (options) => {
    this.setState({filterExp: options.map(option => option.value)}, () => {
      this._onFilter();
    });
  };

  onFilterCityApply = (options) => {
    this.setState({filterCity: options.map(option => option.value)}, () => {
      this._onFilter();
    });
  };

  render() {
    let exp = ['любой', 'не требуется', 'менее 1 года', 'от 1 до 3 лет', 'от 3 до 6 лет', 'более 6 лет'];
    let city = [
      'Абакан',
      'Азов',
      'Александров',
      'Алексин',
      'Альметьевск',
      'Анапа',
      'Ангарск',
      'Анжеро-Судженск',
      'Апатиты',
      'Арзамас',
      'Армавир',
      'Арсеньев',
      'Артем',
      'Архангельск',
      'Асбест',
      'Астрахань',
      'Ачинск',
      'Балаково',
      'Балахна',
      'Балашиха',
      'Балашов',
      'Барнаул',
      'Батайск',
      'Белгород',
      'Белебей',
      'Белово',
      'Белогорск (Амурская область)',
      'Белорецк',
      'Белореченск',
      'Бердск',
      'Березники',
      'Березовский (Свердловская область)',
      'Бийск',
      'Биробиджан',
      'Благовещенск (Амурская область)',
      'Бор',
      'Борисоглебск',
      'Боровичи',
      'Братск',
      'Брянск',
      'Бугульма',
      'Буденновск',
      'Бузулук',
      'Буйнакск',
      'Великие Луки',
      'Великий Новгород',
      'Верхняя Пышма',
      'Видное',
      'Владивосток',
      'Владикавказ',
      'Владимир',
      'Волгоград',
      'Волгодонск',
      'Волжск',
      'Волжский',
      'Вологда',
      'Вольск',
      'Воркута',
      'Воронеж',
      'Воскресенск',
      'Воткинск',
      'Всеволожск',
      'Выборг',
      'Выкса',
      'Вязьма',
      'Гатчина',
      'Геленджик',
      'Георгиевск',
      'Глазов',
      'Горно-Алтайск',
      'Грозный',
      'Губкин',
      'Гудермес',
      'Гуково',
      'Гусь-Хрустальный',
      'Дербент',
      'Дзержинск',
      'Димитровград',
      'Дмитров',
      'Долгопрудный',
      'Домодедово',
      'Донской',
      'Дубна',
      'Евпатория',
      'Егорьевск',
      'Ейск',
      'Екатеринбург',
      'Елабуга',
      'Елец',
      'Ессентуки',
      'Железногорск (Красноярский край)',
      'Железногорск (Курская область)',
      'Жигулевск',
      'Жуковский',
      'Заречный',
      'Зеленогорск',
      'Зеленодольск',
      'Златоуст',
      'Иваново',
      'Ивантеевка',
      'Ижевск',
      'Избербаш',
      'Иркутск',
      'Искитим',
      'Ишим',
      'Ишимбай',
      'Йошкар-Ола',
      'Казань',
      'Калининград',
      'Калуга',
      'Каменск-Уральский',
      'Каменск-Шахтинский',
      'Камышин',
      'Канск',
      'Каспийск',
      'Кемерово',
      'Керчь',
      'Кинешма',
      'Кириши',
      'Киров (Кировская область)',
      'Кирово-Чепецк',
      'Киселевск',
      'Кисловодск',
      'Клин',
      'Клинцы',
      'Ковров',
      'Когалым',
      'Коломна',
      'Комсомольск-на-Амуре',
      'Копейск',
      'Королев',
      'Кострома',
      'Котлас',
      'Красногорск',
      'Краснодар',
      'Краснокаменск',
      'Краснокамск',
      'Краснотурьинск',
      'Красноярск',
      'Кропоткин',
      'Крымск',
      'Кстово',
      'Кузнецк',
      'Кумертау',
      'Кунгур',
      'Курган',
      'Курск',
      'Кызыл',
      'Лабинск',
      'Лениногорск',
      'Ленинск-Кузнецкий',
      'Лесосибирск',
      'Липецк',
      'Лиски',
      'Лобня',
      'Лысьва',
      'Лыткарино',
      'Люберцы',
      'Магадан',
      'Магнитогорск',
      'Майкоп',
      'Махачкала',
      'Междуреченск',
      'Мелеуз',
      'Миасс',
      'Минеральные Воды',
      'Минусинск',
      'Михайловка',
      'Михайловск (Ставропольский край)',
      'Мичуринск',
      'Москва',
      'Мурманск',
      'Муром',
      'Мытищи',
      'Набережные Челны',
      'Назарово',
      'Назрань',
      'Нальчик',
      'Наро-Фоминск',
      'Находка',
      'Невинномысск',
      'Нерюнгри',
      'Нефтекамск',
      'Нефтеюганск',
      'Нижневартовск',
      'Нижнекамск',
      'Нижний Новгород',
      'Нижний Тагил',
      'Новоалтайск',
      'Новокузнецк',
      'Новокуйбышевск',
      'Новомосковск',
      'Новороссийск',
      'Новосибирск',
      'Новотроицк',
      'Новоуральск',
      'Новочебоксарск',
      'Новочеркасск',
      'Новошахтинск',
      'Новый Уренгой',
      'Ногинск',
      'Норильск',
      'Ноябрьск',
      'Нягань',
      'Обнинск',
      'Одинцово',
      'Озерск (Челябинская область)',
      'Октябрьский',
      'Омск',
      'Орел',
      'Оренбург',
      'Орехово-Зуево',
      'Орск',
      'Павлово',
      'Павловский Посад',
      'Пенза',
      'Первоуральск',
      'Пермь',
      'Петрозаводск',
      'Петропавловск-Камчатский',
      'Подольск',
      'Полевской',
      'Прокопьевск',
      'Прохладный',
      'Псков',
      'Пушкино',
      'Пятигорск',
      'Раменское',
      'Ревда',
      'Реутов',
      'Ржев',
      'Рославль',
      'Россошь',
      'Ростов-на-Дону',
      'Рубцовск',
      'Рыбинск',
      'Рязань',
      'Салават',
      'Сальск',
      'Самара',
      'Санкт-Петербург',
      'Саранск',
      'Сарапул',
      'Саратов',
      'Саров',
      'Свободный',
      'Севастополь',
      'Северодвинск',
      'Северск',
      'Сергиев Посад',
      'Серов',
      'Серпухов',
      'Сертолово',
      'Сибай',
      'Симферополь',
      'Славянск-на-Кубани',
      'Смоленск',
      'Соликамск',
      'Солнечногорск',
      'Сосновый Бор',
      'Сочи',
      'Ставрополь',
      'Старый Оскол',
      'Стерлитамак',
      'Ступино',
      'Сургут',
      'Сызрань',
      'Сыктывкар',
      'Таганрог',
      'Тамбов',
      'Тверь',
      'Тимашевск',
      'Тихвин',
      'Тихорецк',
      'Тобольск',
      'Тольятти',
      'Томск',
      'Троицк',
      'Туапсе',
      'Туймазы',
      'Тула',
      'Тюмень',
      'Узловая',
      'Улан-Удэ',
      'Ульяновск',
      'Урус-Мартан',
      'Усолье-Сибирское',
      'Уссурийск',
      'Усть-Илимск',
      'Уфа',
      'Ухта',
      'Феодосия',
      'Фрязино',
      'Хабаровск',
      'Ханты-Мансийск',
      'Хасавюрт',
      'Химки',
      'Чайковский',
      'Чапаевск',
      'Чебоксары',
      'Челябинск',
      'Черемхово',
      'Череповец',
      'Черкесск',
      'Черногорск',
      'Чехов',
      'Чистополь',
      'Чита',
      'Шадринск',
      'Шали',
      'Шахты',
      'Шуя',
      'Щекино',
      'Щелково',
      'Электросталь',
      'Элиста',
      'Энгельс',
      'Южно-Сахалинск',
      'Юрга',
      'Якутск',
      'Ялта',
      'Ярославль'
    ];
    city = city.map(city => {
      return {
        label: city,
        value: city
      }
    });
    exp = exp.map(exp => {
      return {
        label: exp,
        value: exp
      }
    });

    return (
      <div className={'jobs-filter'}>
        <div onClick={this.toggleCollapse} className="jobs-filter__collapse"><Icon name={this.state.collapsed ? 'angle-down' : 'angle-up'}/>
          {this.state.collapsed ? 'Развернуть фильтр' : 'Свернуть фильтр'}
        </div>

        <div className={"filter " + (this.state.collapsed ? '' : 'filter--visible')}>
          <div className="jobs-filter__field">
            <label>Опыт: </label>
            <Select onChange={this.onFilterExpApply} placeholder='Выбрать опыт...' isMulti options={exp} name="exp" className="exp-select" classNamePrefix="select"/>
          </div>

          <div className="jobs-filter__field">
            <label>Город: </label>
            <Select onChange={this.onFilterCityApply} placeholder='Выбрать города...'  isMulti options={city} name="city" className="city-select" classNamePrefix="select"/>
          </div>
        </div>
      </div>
    );
  }
}

