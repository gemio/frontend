import React from 'react';
import Input from '../Input';
import Button from '../Button';
import Form from '../Form';
import APIMessage from '../APIMessage';
import "./ResetPasswordForm.scss";

class ResetPasswordForm extends React.Component {
  state = {
    isRequestSuccess: false,
    responseMessage: '',
    passwordLabel:'Введите пароль'
  };

  render() {
      let regex = {
      password: /[a-zA-Z0-9]{8}/,
      pasbool:false
    };

    let resetPass = () => {
      let user = {
        password: document.getElementById('password').value,
        repeatPassword: document.getElementById('repeatPassword').value,
        id: window.location.href.split('/')[4]
      };

      if (user.password !== user.repeatPassword) {
        this.setState({errorText: 'Пароли не совпадают'});
        return;
      }

      fetch(`${process.env.API_URL}/user/resetPassword/${user.id}?password=${user.password}`, {
        method: 'PATCH',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        response.json().then((data) => {
          this.setState({responseMessage: data.message.message, isRequestSuccess: response.status === 200}, () => {
            if (this.state.isRequestSuccess) setTimeout(() => {
              window.location.href = '/login'
            }, 3000)
          });
        });
      });
  };

  return (
    <Form className="reset-password__form">
      <APIMessage className="reset-password__message" status={this.state.isRequestSuccess ? 'success' : 'error'} text={this.state.responseMessage}/>
      <Input required={true} validateRegex={regex.password} id="password" className="reset-password__form__field" type="password" labelText={this.state.passwordLabel}/>
      <Input required={true} id="repeatPassword" className="reset-password__form__field" type="password" labelText="Потверждение пароля"/>
      <Button onClick={resetPass} text="Потвердить" className="button--primary reset-password__form__submit"/>
    </Form>
  );
  }
}

export default ResetPasswordForm;
