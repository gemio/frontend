import React from 'react';
import './AvatarCircle.scss';

export default class AvatarCircle extends React.Component {
  render() {
    return (
      <img onClick={this.props.onClick} className={'avatar-circle ' + (this.props.className || '')} src={this.props.img} alt={this.props.alt || 'gem'}/>
    );
  }
}
