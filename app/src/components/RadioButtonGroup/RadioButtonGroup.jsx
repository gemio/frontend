import React from 'react';
import './RadioButtonGroup.scss';

export default class RadioButtonGroup extends React.Component {
  render() {
    return (
      <div className={'radio-button-group ' + (this.props.className + '')}>
        <div className='radio-button-group__title'>{this.props.title}</div>
        {this.props.children}
      </div>
    );
  }
}
