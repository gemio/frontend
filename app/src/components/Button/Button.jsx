import React from 'react';
import {Icon} from 'react-fa';
import './Button.scss';

class Button extends React.Component {
  render() {
    let icon;
    if (this.props.isIcon) {
      icon = (
        <Icon name={this.props.iconName}/>
      );
    }

    return (
      <button style={this.props.style} onClick={this.props.onClick} type={this.props.type || ''} className={'button ' + (this.props.className || '')}>
        {this.props.isIcon ? icon : this.props.text}
      </button>
    );
  }
}

export default Button;
