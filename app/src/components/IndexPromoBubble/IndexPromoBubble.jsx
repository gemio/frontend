import React from 'react';
import img1 from '../../img/profile-temp.png';
import img2 from '../../img/profile-temp2.jpeg';
import img3 from '../../img/default.png';
import imgCircle from '../../img/shapes/shape-circle.svg';
import './IndexPromoBubble.scss';

export default class IndexPromoBubble extends React.Component {
  render() {
    return (
      <div className="promo__bubbles">
        <img className="promo__bubbles-image" src={img1} alt="gem-avatar"/>
        <img className="promo__bubbles-image" src={img2} alt="gem-avatar"/>
        <img className="promo__bubbles-image" src={img3} alt="gem-avatar"/>

        <img className="promo__bubbles-image" src={img2} alt="gem-avatar"/>
        <img className="promo__bubbles-image" src={img3} alt="gem-avatar"/>
        <img className="promo__bubbles-image" src={img1} alt="gem-avatar"/>

        <img className="promo__bubbles-image" src={img3} alt="gem-avatar"/>
        <img className="promo__bubbles-image" src={img1} alt="gem-avatar"/>
        <img className="promo__bubbles-image" src={img2} alt="gem-avatar"/>

        <img className="promo__bubbles-shape" src={imgCircle} alt="gem-shape"/>
        <img className="promo__bubbles-shape" src={imgCircle} alt="gem-shape"/>
        <img className="promo__bubbles-shape" src={imgCircle} alt="gem-shape"/>
        <img className="promo__bubbles-shape" src={imgCircle} alt="gem-shape"/>
        <img className="promo__bubbles-shape" src={imgCircle} alt="gem-shape"/>
      </div>
    );
  }
}
