import React, { Component } from 'react';
import {Icon} from 'react-fa';
import AvatarCircle from '../../AvatarCircle';
import NotFriend from './NotFriend/NotFriend';
import './TreatmentSpecialistItem.scss';

export default class TreatmentSpecialistItem extends Component {
  render() {
    let friendWarning;
    if (!this.props.item.isFriends) {
      friendWarning = (<NotFriend pending={this.props.pending} />);
    }

    return (
      <li className={'treatment-specialist ' + (this.props.className || '')} onClick={() => this.props.handleItem(this.props.item)}>
        <AvatarCircle img={this.props.item.avatar} className="treatment-specialist__avatar"/>

        <div className="treatment-specialist__user-info">
          <div className="treatment-specialist__user-fullName">{this.props.item.fullName}</div>
          {friendWarning}
        </div>
      </li>
    );
  }
}
