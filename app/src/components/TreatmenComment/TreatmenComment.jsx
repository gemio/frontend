import React, {Component} from 'react';
import {connect} from "react-redux";
import {Icon} from 'react-fa';
import AvatarCircle from  '../AvatarCircle';
import CardTitle from '../Card/CardEmptyTitle';
import Dropdown from 'react-simple-dropdown';
import SpecialistItem from './SpecialistItem';
import SpecialistChoosen from './SpecialistItem/SpecialistChoosen';
import SpecialistSteps from './SpecialistSteps';
import DropdownTrigger  from 'react-simple-dropdown/lib/components/DropdownTrigger';
import DropdownContent  from 'react-simple-dropdown/lib/components/DropdownContent';
import DialogMessage from  '../DialogMessage';
import InputSimple from '../InputSimple';
import Button from '../Button';
import moment from "moment/moment";
import 'react-simple-dropdown/styles/Dropdown.css';
import './TreatmentComent.scss';

class TreatmenComment extends Component {
  state = {
    active: true,
    MedicalList: [],
    DoctorSelect: false,
    SelectDoctor: false,
    AddItem: false,
    Accepted: [],
    Pending: [],
    item: [],
    message: [],
  };

  componentWillMount() {
    this.update();
  }

  update = () => {
    fetch(`${process.env.API_URL}/doctor/status/${this.props.user.info._id}?status=accepted`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      response.json().then(data => {
        this.setState({Accepted: data.data});
        if (data.data.length > 0) {
          let d = [];

          for (let item of data.data) {
            this.setState({item: item});
            d = item;
          }

          this.Getmessage(d);
        }
      });
    });

    fetch(`${process.env.API_URL}/doctors/${this.props.user.info._id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      response.json().then(data => {
        this.setState({MedicalList: data.data});
      });
    });

    this.GetPending();
  };

  GetPending = () => {
    fetch(`${process.env.API_URL}/doctor/status/${this.props.user.info._id}?status=${'pending'} `, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      response.json().then(data => { 
        this.setState({Pending: data.data});
      });
    });
  };


  HandleDrop = () => {
    this.setState({active: !this.state.active, SelectDoctor: false, DoctorSelect: false});
  };

  HandleItem = (id) => {
    this.setState({SelectDoctor: true, DoctorSelect: id, active: false});
  };

  Datetime = (dat) => {
    let diff = moment(new Date()).diff(moment(new Date(dat)), 'days');
    let date = moment(new Date(dat)).format('DD.MM.YYYY');
    return diff < 2 ? moment(new Date().fromNow(new Date(dat))).fromNow() : date;
  };

  AddMedical = () => {
    this.setState({AddItem: !this.state.AddItem});
  };

  Getmessage = (id) => {
    fetch(`${process.env.API_URL}/doctor/message/${this.props.user.info._id}?userID=${id.userID}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 200) {
        response.json().then(data => {
          this.setState({message: data.data});
        });
      }
    });
  };

  AddMessage = () => {
    let dat = document.getElementById('sendMessageTreatment').value;
    fetch(`${process.env.API_URL}/doctor/message/${this.props.user.info._id}?userID=${this.state.item.userID} `, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `text=${dat}`
    }).then(response => {
      document.getElementById('sendMessageTreatment').value="";
      this.Getmessage(this.state.item); 
    });
  };

  render() {
    let messages, specialists;
    if (this.state.Accepted.length === 1) {
      if (this.state.message) {
        messages = this.state.message.map((item, index) => (
          <DialogMessage key={index} image={item.userFrom.avatar} dateAgo={this.Datetime(item.date)} text={item.text} name={item.userFrom.fullName}/>
        ));
      } else
        messages = (<CardTitle>Нет сообщений</CardTitle>);
    } else {
      if (this.state.MedicalList) {
        specialists = this.state.MedicalList.map((item, index) => (
          <SpecialistItem key={index} handleItem={this.HandleItem} pending={this.state.Pending} item={item}/>
        ));
      }
    }

    return (
      <div className='TreatmenComment'>
        {
          this.state.Accepted.length === 1 ?
            <div className="TreatmenComment__Commenting">
              <div className="TreatmenComment__Commenting__block">
                <AvatarCircle img={this.state.item.avatar} className="TreatmenComment__user-avatar" />
                <a href={`/id/${this.state.item.userID}`} className="TreatmenComment__user-link">{this.state.item.fullName}</a>
              </div>

              <div className={"TreatmenComment__Commenting__comments"}>{messages}</div>
              <div className={"TreatmenComment__Commenting__Addcomments"}>
                <InputSimple id="sendMessageTreatment" isMultiline={true} rows="1" cols="80"/>
                <Button onClick={this.AddMessage} className="button--primary dialog__send" text="Отправить"/>
              </div>
            </div>

            :<div>
              <Dropdown active={this.state.active} className={"TreatmenComment__Dropdown"}>
                <DropdownTrigger onClick={this.HandleDrop}>
                  {!this.state.SelectDoctor ? 'Выбрать специалиста' : 'Изменить выбор'} <Icon name={this.state.active ? 'angle-up' : 'angle-down'}/>
                </DropdownTrigger>

                <DropdownContent className={"TreatmenComment__Dropdown__content"}>
                  <ul>{specialists}</ul>
                </DropdownContent>
              </Dropdown>

              {
                <div className={"TreatmenComment__Messenger"}>
                  {this.state.SelectDoctor ?

                    <div className={(this.state.AddItem ? "TreatmenComment__Messenger__Add " : "") + "TreatmenComment__Dropdown__content__block__content "}>
                      {this.state.AddItem ?
                        <SpecialistSteps handleAdded={this.update} doctor={this.state.DoctorSelect}/>
                        :
                        <SpecialistChoosen pending={this.state.Pending} doctor={this.state.DoctorSelect} handleAddDoctor={this.AddMedical}/>
                      }
                    </div> : <div/>

                  }
                </div>
              }
          </div>
        }

      </div>);
  }
}
export default connect(
  state => ({
    user: state.user
  })

)(TreatmenComment);
