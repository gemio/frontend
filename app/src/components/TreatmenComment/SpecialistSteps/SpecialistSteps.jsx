import React, {Component} from 'react';
import {connect} from 'react-redux';
import Button from '../../Button';
import {Icon} from 'react-fa';
import './SpecialistSteps.scss';

class SpecialistSteps extends Component {
  state = {
    friendRequestStatus: 'not-friend',
    doctorRequest: 'not'
  };

  componentDidMount() {
    this.setState({friendRequestStatus: this.props.doctor.isFriends ? 'accepted' : 'not-friend'});
    this.checkRequested();
  }

  checkRequested = () => {
    const friendRequest = () => {
      fetch(`${process.env.API_URL}/friend/${this.props.user.info._id}/isFriend/${this.props.doctor.id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.status === 200) {
          response.json().then(data => {
            this.setState({friendRequestStatus: data.data});
          });
        }
      });
    };

    const doctorRequest = () => {
      fetch(`${process.env.API_URL}/doctor/user-status/${this.props.user.info._id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.status === 200) {
          response.json().then(data => {
            this.setState({doctorRequest: data.data.status});
          });
        }
      });
    };

    friendRequest();
    doctorRequest();
  };

  sendFriendRequest = () => {
    fetch(`${process.env.API_URL}/friend/${this.props.user.info._id}/add/${this.props.doctor.id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }).then(response => {
      if (response.status === 412 || response.status === 200)
        this.setState({friendRequestStatus: 'requested'});
    });
  };

  sendDoctorRequest = () => {
    fetch(`${process.env.API_URL}/doctor/${this.props.user.info._id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body:`userID=${this.props.doctor.id}`
    }).then(response => {
      if (response.status === 412 || response.status === 200)
        this.setState({doctorRequest: 'pending'});
    });
  };

  render() {
    const addFriendButton = () => {
      if (this.state.friendRequestStatus === 'not-friend') {
        return (<Button text='Добавить в друзья' onClick={this.sendFriendRequest} className='button--primary-small-round'/>);
      } else if (this.state.friendRequestStatus === 'requested') {
        return (<Button text='Отправлено' className='button--disabled'/>);
      } else if (this.state.friendRequestStatus === 'accepted') {
        return (<Icon name='check'/>);
      }
    };

    const addDoctorButton = () => {
      if (this.state.doctorRequest === 'not' && this.state.friendRequestStatus !== 'accepted')
        return (<Button text='Не в друзьях' className='button--disabled'/>);
      if (this.state.doctorRequest === 'not') {
        return (<Button text='Отправить запрос' onClick={this.sendDoctorRequest} className='button--primary-small-round'/>);
      } else if (this.state.doctorRequest === 'pending') {
        return (<Button text='Отправлено' className='button--disabled'/>);
      } else if (this.state.doctorRequest === 'accepted') {
        return (<Icon name='check'/>);
      }
    };

    if (this.state.friendRequestStatus === 'accepted' && this.state.doctorRequest === 'accepted') {
      this.props.handleAdded && this.props.handleAdded();
    }

    return (
      <div className="treatment-steps">
        <ol className="treatment-steps__list">
          <li>
            Добавить пользователя в друзья
            {addFriendButton()}
          </li>

          <li>
            Отправить запрос на добавление в качестве лечащего врача
            {addDoctorButton()}
          </li>
        </ol>

        <Button onClick={this.checkRequested} text='Обновить' className='button--primary-small-round treatment-steps__update'/>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  })

)(SpecialistSteps);
