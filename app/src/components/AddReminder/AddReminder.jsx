import React from 'react';
import Col from '../Col';
import Form from '../Form';
import Button from '../Button';
import APIMessage from '../APIMessage';
import DateTimePicker from '../DateTimePicker';
import "./AddReminder.scss";
import Input from '../Input';
import { connect } from 'react-redux';
import Prevention from '../Prevention';
import AddRemind from '../AddRemind';
import AddConsultation from  '../AddConsultation';

class AddReminder extends React.Component {
  state = {
    isRemindAdded: false,
    statusText: '',
    roleItems: ['1', '2'],
    text: "Профилактика",
    body: ""
  };

  render() {
    let sendRemind = (e) => {
     if(this.state.text === "Заявка на консультацию")
     {}
     else {
       let inputs = e.getElementsByTagName('input');
       //date
       let get__date = inputs[0].value.replace('  ', '').split(' ')[1];
       //tinme
       let get__time = inputs[0].value.replace('  ', '').split(' ')[0];


       let dates = new Date(get__date.split('.')[2], (get__date.split('.')[1] - 1), get__date.split('.')[0], get__time.split(':')[0], get__time.split(':')[1]);

       if (dates < new Date()) {
         this.setState({isRemindAdded: false, statusText: "Нельзя поставить на старую дату"});
         return;
       }
       let body;
       if (this.state.isRemindAdded === false) {
         document.getElementById('role').value = this.state.text;
       }
       let med = e.getElementsByClassName('radio-button__value');
       let dateAndTime = inputs[0].value.replace('  ', '').split(' ');
       if (this.state.text === 'Профилактика' || this.state.text === 'Пункция') {
         let medicament = med[inputs[1].checked ? 0 : inputs[2].checked ? 1 : 2].innerHTML;
         let dosage = inputs[4].value;
         let injection = {
           type: 'инъекция',
           date: dateAndTime[1],
           time: dateAndTime[0],
           medicament: medicament,
           dosage: dosage,
         };
         body = `token=${this.props.user.info._id}&type=${this.state.text}&date=${injection.date}&time=${injection.time}&dosage=${injection.dosage}&medicament=${injection.medicament}`;
       } else {
         let about = document.getElementById('about').value;
         body = `token=${this.props.user.info._id}&type=${this.state.text}&date=${dateAndTime[1]}&time=${dateAndTime[0]}&text=${about}`;
       }

       fetch(`${process.env.API_URL}/remind`, {
         method: 'POST',
         headers: {
           'Content-Type': 'application/x-www-form-urlencoded'
         },
         body: body
       }).then((response) => {
         if (response.status === 404 || response.status === 500) {
           this.setState({errorText: 'В данный момент сервер недоступен! Попробуйте позже'});
           return;
         }

         response.json().then((data) => {
           this.setState({isRemindAdded: response.status === 200, statusText: data.message.message});
           if (response.status === 200) {
             if (this.props.addHandler) this.props.addHandler();
             else window.location.reload();
           }
         });
       }).catch((response) => {
         throw new Error('An error was occured. Object for debug: ' + response);
       });
     }
    };

    let getRole = () => {
      return new Promise ((resolve, reject) => {
        fetch(`${process.env.API_URL}/remind/types`, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
        }).then((response) => {
          if (response.status === 404 || response.status === 500) {
            reject();
            return;
          }

          resolve(new Promise ((resolve, reject) => {
            response.json().then((data) => {
              resolve(data);
            });
          }));
        }).catch((response) => {
          reject(new Error('An error was occured. Object for debug: ' + response));
        });
      })
    };

     if (this.state.roleItems[0] === '1') {
      getRole().then((data) => {
        let arrayWithType = this.state.roleItems;
        arrayWithType = [];
        data.data.forEach(function(item, i, arr) { arrayWithType.push(item.type); });
        this.setState({roleItems: arrayWithType});
      });
    }

    const onChange = (role) => {
      this.setState({text:role.target.value});
    };

    return (
      <Col xs="12" className="add-remind">
        <h2 className="add-remind__header">Добавить напоминание</h2>
        <APIMessage status={this.state.isRemindAdded ? 'success' : 'error'} text={this.state.statusText}/>
        <Form className='add-remind__form' submitCallback={sendRemind}>
          <div className="add-remind__block">
            <DateTimePicker showTimeSelect={true} title="Дата и время" className="add-remind__date"/>

            <div className="add-remind__DropDownList">
              <h2 className="add-remind__DropDownList__header">Тип</h2>
              <Input items={this.state.roleItems.join('&')} onChange={onChange}  required={true} id="role" className="add-remind__DropDownList__Input" type="select" />
            </div>
          </div>
          {this.state.text === "Профилактика" ? <Prevention/> : <AddRemind />}
          <Button type="submit" className={(this.state.text === "Заявка на консультацию" ? " AddReminder__button " : "") + " button--primary " } text="Добавить"/>
        </Form>
      </Col>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({})
)(AddReminder);
