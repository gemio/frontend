import React from 'react';

class Form extends React.Component {
  constructor() {
    super(...arguments);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.props.submitCallback)
      this.props.submitCallback(e.target);
  }

  render() {
    return (
      <form className={(this.props.className || '')} autoComplete="off" onSubmit={this.handleSubmit || ''} id={this.props.formID} encType={this.props.encType}>
        {this.props.children}
      </form>
    );
  }
}

export default Form;
