import React from 'react';
import Card from '../Card';
import './PostForm.scss';

export default class PostForm extends React.Component {
  render() {
    return (
      <Card cardShadow='no-shadow' className="PostForm">
        <div className="PostForm__Header">
        {this.props.children}
        </div>
        
      </Card>
    )
  }
}