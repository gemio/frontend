import React from 'react';
import RadioButtonGroup from '../RadioButtonGroup';
import RadioButton from '../RadioButton';
import './ToogleButton.scss';

export default class ToogleButton extends React.Component {
  render() {
    return (
      <RadioButtonGroup title={this.props.title} className={'toggle-button ' + (this.props.className || '')}>
        <RadioButton isChecked={this.props.leftChecked} name={this.props.groupName} value={this.props.valueLeft} />
        <RadioButton isChecked={this.props.rightChecked} name={this.props.groupName} value={this.props.valueRight} />
      </RadioButtonGroup>
    );
  }
}
