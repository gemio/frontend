import React from 'react';
import Card from '../Card';
import Button from '../Button';
import Progress from '../Progress';

import './MoneyHelp.scss';

export default class MoneyHelp extends React.Component {
  render() {
    const numberWithCommas = (x) => {return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");}

    return (
      <Card cardShadow='no-shadow' className={'money-help ' + (this.props.className || '')}>
        <div className="money-help__image"><img src={this.props.img} alt="gem-help" className="money-help__image__src" /></div>
        <div className="money-help__title">{this.props.title}</div>
        <div className="money-help__progress">
          <div className="money-help__left">
            <div className="money-help__description">всего необходимо</div>
            <div className="money-help__cost">{numberWithCommas(this.props.total)} руб.</div>
          </div>

          <div className="money-help__right">
            <div className="money-help__description">осталось собрать</div>
            <div className="money-help__cost">{numberWithCommas(this.props.remained)} руб.</div>
          </div>
        </div>

        <Progress className='money-help__progress-bar' percent={this.props.percent} color="#2ec4b6"/>
        <Button className="button--primary button--rounded" text="ПОМОЧЬ"/>
      </Card>
    );
  }
}
