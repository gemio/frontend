import React, {Component} from 'react';
import DropdownSimple from '../../DropdownSimple';
import {Icon} from 'react-fa';
import './AdditionalLanguage.scss';

export default class AdditionalLanguage extends Component {
  render() {
    const languages = [
        'азербайджанский',
        'английский',
        'арабский',
        'армянский',
        'баскский',
        'белорусский',
        'болгарский',
        'голландский',
        'грузинский',
        'датский',
        'доломитский',
        'испанский',
        'итальянский',
        'казахский',
        'керекский',
        'киргизский',
        'китайский',
        'кумыкский',
        'латинский',
        'литовский',
        'монгольский',
        'мордовские',
        'немецкий',
        'польский',
        'португальский',
        'румынский',
        'русский',
        'сербский',
        'словацкий',
        'словенский',
        'татарский',
        'турецкий',
        'удмуртский',
        'украинский',
        'финский',
        'французский',
        'хакасский',
        'хорватский',
        'чероки',
        'чеченский',
        'чешский',
        'чукотский',
        'чувашский',
        'шведский',
        'швейцарско-ретороманский',
        'шумерский',
        'эстонский',
        'якутский',
        'японский',
        ];
    const languageLevel = [
      'Базовые знания',
      'Читаю профессиональную литературу',
      'Могу свободно проводить интервью',
      'Свободно владею'
    ];

    return (
      <div className="summary__language-item">
        <label>Дополнительный язык: </label>
        <DropdownSimple items={languages}/>
        <DropdownSimple className='summary__language-level' items={languageLevel}/>
        <Icon onClick={this.props.deleteHandler} name='times'/>
      </div>
    );
  }
}
