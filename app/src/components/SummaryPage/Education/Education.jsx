import React, {Component} from 'react';
import DropdownSimple from '../../DropdownSimple';
import InputSimple from '../../InputSimple';
import {Icon} from 'react-fa';
import './Education.scss';

export default class Education extends Component {
  render() {
    const level = [
      'Высшее',
      'Неоконченное высшее',
      'Среднее специальное',
      'Среднее'
    ];

    return (
      <div className="summary__education-item">
        <div className="summary__education-level">
          <label>Уровень: </label>
          <DropdownSimple items={level}/>
        </div>

        <div className="summary__education-subject">
          <InputSimple labelText='Учебное заведение:' placeholder='напр. СПБКИТ'/>
        </div>

        <div className="summary__education-specialist">
          <InputSimple labelText='Специализация:' placeholder='напр. Программирование'/>
        </div>

        <div className="summary__education-year">
          <InputSimple value='' onlyNum={true} max={new Date().getFullYear() + 50} min={1980} labelText='Год окончания:' placeholder='напр. 2018'/>
        </div>

        <Icon onClick={this.props.deleteHandler} name='times'/>
      </div>
    );
  }
}
