import React, {Component} from 'react';
import {Icon} from 'react-fa';
import './CommitIcon.scss';

export default class CommitIcon extends Component {
  render() {


    return (
      <div className={'comments ' + (this.props.className || '')}>
        <Icon name={this.props.className === "Subject__EndBlock"?'folder':'comment'}/>
        {this.props.count}
      </div>
    );
  }
}
