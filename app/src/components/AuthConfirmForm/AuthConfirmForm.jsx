import React, {Component} from 'react';
import {connect} from 'react-redux';
import Form from '../Form';
import './AuthConfirmForm.scss';
import FileUploadButton from "../FileUploadButton";

export default class AuthConfirmForm extends Component {
  state = {
    documents: [],
    message: ''
  };

  render() {
    const sendDocuments = (e) => {
      console.log(e.target);
      /*let data = new FormData();
      for (let i = 0; i < e.target)
      data.append('avatar', e.target.files[0]);
      e.target.value = '';
      fetch(`${process.env.API_URL}/user/${this.props.user.info._id}/avatar`, {
        method: 'POST',
        body: data
      }).then(response => {
        window.location.reload(true);
      }).catch(err => {console.log(err)});*/
    };

    return (
      <Form handleSubmit={(e) => {e.preventDefault()}} encType="multipart/form-data" className='auth-confirm__form'>
        <FileUploadButton uploadHandler={sendDocuments} text="Добавить документы" buttonClass="button--primary auth-confirm__submit"/>
        <div className="auth-confirm__message">{this.state.message}</div>
      </Form>
    );
  }
}