import React from 'react';
import './Input.scss';

import OpenPasswordEye from '../OpenPasswordEye';
import InputValidate from '../InputValidate';
import DropdownIcon from "../DropdownIcon/DropdownIcon";
import DropdownItem from '../DropdownItem';

class Input extends React.Component {
  constructor() {
    super(...arguments);

    this.state = {
      floating: true,
      labelClass: 'floating',
      value: '',
      validateStatus: -1,
      requireStatus: false,
      needRepaint: false,
      dropItems: this.props.items === undefined || this.props.items === '1&2' ? ['test', 'asd'] : this.props.items.split('&'),
      dropOpened: false,
    };

    this._onBlur = this._onBlur.bind(this);
    this._onFocus = this._onFocus.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  _onBlur() {
    if (this.state.value.length === 0)
      this.setState({
        floating: false,
        labelClass: '',
        requireStatus: this.props.required || this.state.requireStatus
      });

    // Validate input
    if (this.props.validateRegex) {
      this.setState({
        validateStatus: this.props.validateRegex.test(this.state.value) ? 1 : 0,
        needRepaint: true});
    }
  }

  _onFocus() {
    this.setState({floating: true, labelClass: 'floating'});
  }

  componentWillMount() {
    if (this.props.type === 'select') {
      this.setState({dropItems: this.props.items});
    }
  }

  componentDidMount() {
    this.setState({
      floating: this.state.value.length !== 0,
      labelClass: this.state.value.length !== 0 ? 'floating' : this.props.type === 'select' ? 'floating' : ''
    });
  }

  handleChange(e) {
    this.setState({
      value: e.target.value,
      requireStatus: e.target.value.length === 0 ? this.props.required || this.state.requireStatus : false
    });
  }

  render() {
    let icon, input;
    if (this.props.type === 'password') {
      if (!this.props.id)
        throw new Error("Input with type 'password' should have an id!");
      icon = (<OpenPasswordEye id={this.props.id} />);
      input = (<input id={this.props.id} onChange={this.handleChange} onBlur={this._onBlur} onFocus={this._onFocus} type={this.props.type} />);
    } else if (this.props.type === 'select') {
      let items = this.props.items;
      if (typeof items === 'string')
        items = items.split('&');
      icon = (<DropdownIcon isOpened={false} />);
      input = (
        <select className="dropdown" id={this.props.id} value={this.state.value} onChange={(e) => {this.handleChange(e); if (this.props.onChange) this.props.onChange(e)}} onBlur={this._onBlur} onFocus={this._onFocus} type={this.props.type}>
          {
            items.map(item => (
              (<DropdownItem key={item} value={item} text={item} />)
            ))
          }
        </select>

      );
    } else {
      if (this.state.validateStatus === 1)
        icon = (<InputValidate isSuccess={this.state.validateStatus}/>);
      input = (<input id={this.props.id} onChange={this.handleChange} onBlur={this._onBlur} onFocus={this._onFocus} type={this.props.type}  />);
    }
    return (
      <div id={this.props.idBlock || ''} className={(this.props.className || '') + (this.state.requireStatus || this.state.validateStatus === -1 ? ' none' : this.state.validateStatus === 0 ? ' error' : ' success') + " input"}>
        <label className={this.state.labelClass}>{this.props.labelText || ''}</label>
        {input}
        {icon}
      </div>
    );
  }
}

export default Input;
