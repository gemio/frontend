import React from 'react';
import "./LegendItem.scss";

export default class LegendItem extends React.Component {
  render() {
    return (
      <div className={'legend-item legend-item--' + (this.props.color || '')}>
        <span className="legend-item__text">{this.props.text}</span>
      </div>
    );
  }
}
