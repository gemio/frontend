import React from 'react';

class SlideMenuNews extends React.Component {
  render() {
    return (
      <ul>
        {this.props.children}
      </ul>
    );
  }
}

export default SlideMenuNews;