import React, {Component} from 'react';
import './WikiListItem.scss';

export default class WikiListItem extends Component {
  render() {
    return (
      <li className="wiki__item">{this.props.text}</li>
    );
  }
}
