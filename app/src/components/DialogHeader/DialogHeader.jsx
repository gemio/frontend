import React from 'react';
import {NavLink} from 'react-router-dom';
import {Icon} from 'react-fa';
import './DialogHeader.scss';

export default class DialogHeader extends React.Component {
  render() {
    return (
      <div className='dialog__header'>
        <NavLink to="/dialogs" className="dialog__back"><Icon name="angle-left"/>Назад</NavLink>
        <div className="dialog__fullName">{this.props.fullName}</div>
        <div className="dialog__avatar"><img src={this.props.avatar} alt="avatar"/></div>
      </div>
    );
  }
}
