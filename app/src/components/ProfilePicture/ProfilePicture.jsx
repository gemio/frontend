import React from 'react';
import Card from '../Card';
import FileUploadButton from '../FileUploadButton';
import { connect } from 'react-redux';
import './ProfilePicture.scss';
import Modal from 'react-modal';
import Button from "../Button/Button";
import {Icon} from 'react-fa';
Modal.setAppElement('#root');

class ProfilePicture extends React.Component {
  state = {
    isFriend: false,
    isAPILoaded: false,
    isMessageOpened: false,
    message: ''
  };

  componentDidMount() {
    if (this.props.isEditable) return;

    fetch(`${process.env.API_URL}/friend/${this.props.user.info._id}/isFriend/${this.props.userID}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({isAPILoaded: true});
        return;
      }

      response.json().then(data => {
        this.setState({isFriend: response.status === 200 ? data.data : false, isAPILoaded: true})
      }).catch(err => {

      });
    }).catch(err => {

    });
  }

  toggleModalMessage = () => {
    this.setState({isMessageOpened: !this.state.isMessageOpened});
  };

  render() {
    const avatarUpload = (e) => {
      if (e.target.files[0].size / 1024 /1024 > 1) {
        e.target.value = '';
        this.setState({message: 'Превышен максимально допустимый размер файла', isMessageOpened: true});
        return;
      }

      let data = new FormData();
      data.append('avatar', e.target.files[0]);
      e.target.value = '';
      fetch(`${process.env.API_URL}/user/${this.props.user.info._id}/avatar`, {
        method: 'POST',
        body: data
      }).then(response => {
        if (response.status === 200 || response.status === 404) window.location.reload();
        else {
          response.json().then(data => {
            this.setState({message: data.message.message, isMessageOpened: true});
          });
        }
      }).catch(err => {console.log(err)});
    };

    const requestFriend = (e) => {
      if (this.state.isFriend === 'accepted' || this.state.isFriend === 'requested') {
        return;
      }

      fetch(`${process.env.API_URL}/friend/${this.props.user.info._id}/add/${this.props.userID}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        }
      }).then(response => {
        if (response.status === 404 || response.status ===  500) {
          console.warn('Сервер не отвечает');
          return;
        }
        this.setState({isFriend: response.status === 200 ? 'requested' : false});
      }).catch(err => {
        console.warn(err);
      });
    };

    const removeFriend = (e) => {

    };

    let buttons = (<div />);
    console.log(this.state.isFriend);
    if (!this.props.isEditable && this.state.isAPILoaded)
      buttons = (
        <div className='profile-picture__actions'>
          <Button onClick={() => {window.location.href = `/dialog/${this.props.userID}`}}
                  className='button--primary-small-round'
                  text={'Отправить сообщение'}/>
          <Button onClick={() => {requestFriend()}}
                  className={`button--${this.state.isFriend ? 'light-gray' : 'primary-small-round'}`}
                  text={this.state.isFriend && this.state.isFriend !== 'not-friend' ? this.state.isFriend === 'accepted' ? 'Убрать из друзей' : 'Отменить запрос' : 'Добавить в друзья'}/>
        </div>
      );
    else if (this.props.isEditable) {
      buttons = (
        <form onSubmit={(e) => e.preventDefault()} className="profile__form" encType="multipart/form-data">
          <FileUploadButton fileName="avatar" uploadHandler={avatarUpload} className="profile__edit" buttonClass="button--light-gray" text="Редактировать"/>
        </form>
      );
    }

    const customStylesModal = {
      content : {
        top: '50px',
        left: '50%',
        right: 'auto',
        marginRight: '-50%',
        transform: 'translateX(-50%)',
        zIndex: '10000',
        padding: '35px 45px',
        bottom: 'initial'
      },

      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };

    return (
      <Card cardShadow='no-shadow' className={this.props.className}>
        <img className="profile__avatar" src={!this.props.isEditable ? this.props.avatar : this.props.user.info.avatar} alt="avatar" />
        {buttons}

        <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.isMessageOpened} onRequestClose={this.toggleModalMessage} >
          <div className="profile__avatar-message">{this.state.message}</div>
          <Button onClick={this.toggleModalMessage} style={{ width: '100%', marginTop: 15, height: 30}} className='button--primary-small-round' text='Закрыть'/>
          <Icon onClick={this.toggleModalMessage} name='times'/>
        </Modal>
      </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({})
)(ProfilePicture);
