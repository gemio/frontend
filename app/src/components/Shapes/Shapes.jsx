import React from 'react';

//Image
import shape_circle           from '../../img/shapes/shape-circle.svg';
import shape_left_behind_card from '../../img/shapes/shape-left-behind-card.svg';
import shape_left_bottom      from '../../img/shapes/shape-left-bottom.svg';
import shape_right_top        from '../../img/shapes/shape-right-top.svg';

//Styles
import "./Shapes.scss";

class Shapes extends React.Component {
  render() {
    return (
    	<div className='shapes'>
      	<img src={shape_circle} className="shape shape__1" alt="gem-shape"/>
      	<img src={shape_left_behind_card} className="shape shape__2" alt="gem-shape"/>
      	<img src={shape_left_bottom} className="shape shape__3" alt="gem-shape"/>
      	<img src={shape_right_top} className="shape shape__4" alt="gem-shape"/>
      	<img src={shape_circle} className="shape shape__5" alt="gem-shape"/>
    	</div>
    );
  }
}

export default Shapes;
