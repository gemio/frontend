import React from 'react';
import Card from '../Card';
import AvatarCircle from '../AvatarCircle';
import PostPicture from '../Post__picture';
import PostDetails from '../PostDetails';
import './Entry.scss';
import Modal from 'react-modal';
import {Icon} from 'react-fa';

Modal.setAppElement('#root');

export default class Entry extends React.Component {
  state = {
    modalIsOpen: false,
    block: false,
    Entryblock: false
  };

  openModal = () => {
    this.setState({modalIsOpen: true,stat:false});
  };

  closeModal = () => {
    this.setState({modalIsOpen: false,stat:false});
  };

  toggleAdditionalPostMenu = () => {
    this.setState({Entryblock: !this.state.Entryblock});
  };

  render() {
    const customStylesModal = {
      content : {
        top: '50px',
        left: '50%',
        right: 'auto',
        marginRight: '-50%',
        transform: 'translateX(-50%)',
        zIndex: '10000',
        padding: '34px'
      },

      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };

    return (
	   <Card cardShadow='no-shadow' className="entry">
	   		<div className="entry__header">
          <AvatarCircle className='entry__avatar' img={this.props.img}/>

          <div className="entry__information">
            <div className="entry__full-name">{this.props.name} {this.props.isSticky ? <span>запись закреплена</span> : ''} </div>
            <div className="entry__date-ago">{this.props.date}</div>
          </div>

          { this.props.Myentry ? <Icon name=" fa-ellipsis-h" onClick={this.toggleAdditionalPostMenu}/>:<div/>}

            <div className={!this.state.Entryblock ? "none-block" : "entry__block__menu"}>
              { this.props.Myentry ?
                <div>
                  <p onClick={() => {this.props.onStickyPost(); this.toggleAdditionalPostMenu()}}>{this.props.isSticky ? 'Открепить' : 'Закрепить'}</p>
                  <p onClick={() => {this.props.onDeletePost(); this.toggleAdditionalPostMenu()}}>Удалить</p>
                </div> :<div/>

              }
	   		    </div>
          </div>
	   		<div className="entry__content">{this.props.text}</div>
        <PostPicture images={this.props.image} />

	   		<div className="entry__footer" onClick={this.openModal}>
	   			<Icon name=" fa-comment"/>
	   			<p>Комментировать</p>
	   		</div>

       <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal} >
         <PostDetails id={this.props.id} key={this.props.date} text={this.props.text} images={this.props.image} comment={this.props.post} avatar={this.props.img} name={this.props.name} date={this.props.date}/>
         <Icon onClick={() => {this.closeModal()}} name='times'/>
       </Modal>

	   </Card>
    );
  }
}
