import React from 'react';
import "./LastMessage.scss";

export default class LastMessage extends React.Component {
  render() {
    return (
      <div onClick={() => {window.location.replace(`${process.env.URL_FOLDER}dialog/${this.props.idDialog}`);}} data-dialogurl={this.props.idDialog} className="last-message">
      	<img src={this.props.avatar} alt="last-message" className="last-message__image" />
        <div className="last-message__user">
          <div className="last-message__full-name">{this.props.name}</div>
          <div className="last-message__date-ago">{this.props.date}</div>
        </div>
        <div className="last-message__text">{this.props.text}</div>
      </div>
    );
  }
}
