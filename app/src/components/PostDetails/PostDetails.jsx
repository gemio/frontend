import React, {Component} from 'react';
import {connect} from 'react-redux';
import AvatarCircle from '../AvatarCircle';
import './PostDetails.scss';
import Card from  "../Card";
import Button from '../Button/index';
import InputSimple from '../InputSimple';
import CommentPostItem from  '../ComentPost';
class PostDetails extends Component {
  state = {
    comment: this.props.comment
  };

  updateComments = () => {
    fetch(`${process.env.API_URL}/post/${this.props.user.info.id}/details?postId=${this.props.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      response.json().then(data => {
        if (response.status === 200) this.setState({comment: data.data.comments});
        else console.log(data.message);
        document.getElementById('post').value = '';
      });
    });
  };

  Post = () => {
    let comment = document.getElementById('post').value;
    if (comment.trim() === "") return;

    fetch(`${process.env.API_URL}/post/${this.props.user.info._id}/${this.props.id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body:`text=${comment}`,
    }).then((response) => {
      if (response.status === 200) this.updateComments();
    }).catch(err => {});
  };

  getCommentPostRef = (node) => { this._postComment = node };

  render() {
    console.log(this.state.comment);
    let contentText = this.props.text, contentImages;
    if (this.props.images) contentImages = this.props.images.map((src, index) => (<img key={index} src={src} alt='gem-post'/>));

    return (
      <div className={"row"}>
        {this.props.images.length >0 ?
          <div className='post-details col-md-8'>
            <div className="post-details__content">
              {contentImages}
            </div>
          </div>
          :<div/>
        }

      <div className={(this.props.images.length > 0 ? "col-md-4" : " post-detalis--all") + " post-details__comet"}>
          <div className="post-details__header">
            <div className="post-details__avatar">
              <AvatarCircle img={this.props.avatar}/>
            </div>

            <div className="post-details__user">
              <div className="post-details__name">{this.props.name}</div>
              <div className="post-details__date">{this.props.date}</div>
            </div>
          </div>
          <div className={"post-details__text"}>{contentText}</div>
          <div className={"post-details__coment-big"}>
            {
              this.state.comment.length > 0 ? this.state.comment.map(item => ( <CommentPostItem key={item.date} img={item.avatar} href={`/id/${item.id}`} name={item.fullName} text={item.text} data={item.date}/>)):<div/>
            }
          </div>

          <Card className={"post-details__newcoment"}>
            <div className={"post-details__newcoment-flex"}>
              <AvatarCircle img={this.props.user.info.avatar} className={" posting"}/>
              <InputSimple id="post" value="" cols="16" rows="1" isMultiline={true}/>
            </div>
            <Button className={"PostDetalis__button button--primary"} text='Отправить' onClick={this.Post}/>
          </Card>
      </div>
    </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  })
)(PostDetails);
