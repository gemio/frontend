import React, {Component} from 'react';

export default class FullName extends Component {
  render() {
    let fullName = this.props.fullName;
    if (this.props.isFatherNameIncluded) fullName = `${fullName.surname} ${fullName.name} ${fullName.lastName}`;
    else fullName = `${fullName.surname} ${fullName.name}`;

    return (
      <div className='user-full-name'>{fullName}</div>
    );
  }
}
