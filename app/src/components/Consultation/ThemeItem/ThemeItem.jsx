import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Icon} from 'react-fa';
import Card from '../../Card';
import './ThemeItem.scss';

export default class ThemeItem extends Component {
  render() {
    const redirectToTheme = () => {
      window.location.href = `/questions?theme=${this.props.id}&specialist=${this.props.specialist}`;
    };

    return (
      <Card onClick={redirectToTheme} className='qa-theme'>
        <Icon name='comments-o'/>
        <div className="qa-theme__title">{this.props.title}</div>
      </Card>
    );
  }
}
