import React, {Component} from 'react';
import {connect} from 'react-redux';
import AvatarCircle from '../../AvatarCircle';
import { Link } from 'react-router-dom';
import './QuestionComment.scss';
import moment from 'moment';

export default class QuestionComment extends Component {
  render() {
    let diff = moment(new Date()).diff(moment(new Date(this.props.comment.date)), 'days');
    let date = moment(new Date(this.props.comment.date)).format('DD.MM.YYYY');

    return (
      <div className='question-details__comment'>
        <div className="question-details__comment-header">
          <AvatarCircle img={this.props.comment.user.avatar}/>
          <Link to={`/id/${this.props.comment.user.userID}`} className="question-details__fullname">{this.props.comment.user.fullName}</Link>
          <div className="question-details__comment-date">{diff < 2 ? moment(new Date().fromNow(new Date(this.props.comment.date))).fromNow() : date}</div>
        </div>

        <div className="question-details__comment-text">{this.props.comment.text}</div>
      </div>
    );
  }
}

