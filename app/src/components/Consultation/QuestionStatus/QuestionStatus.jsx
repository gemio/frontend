import React, {Component} from 'react';
import {connect} from 'react-redux';
import './QuestionStatus.scss';

export default class QuestionStatus extends Component {
  render() {
    return (
      <div className={'qa-question__status ' + (this.props.isClosed ? 'qa-question__status--visible' : '')}>
        Закрыт
      </div>
    );
  }
}

