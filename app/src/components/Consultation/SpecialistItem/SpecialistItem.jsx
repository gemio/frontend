import React, {Component} from 'react';
import {connect} from 'react-redux';
import CardEmptyTitle from '../../Card/CardEmptyTitle';
import ThemeItem from '../ThemeItem';
import './SpecialistItem.scss';

export default class SpecialistItem extends Component {
  state = {
    themes: []
  };

  componentDidMount() {
    this._getThemes();
  }

  _getThemes = () => {
    fetch(`${process.env.API_URL}/specialist/themes?specialist=${this.props.specialist}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 204) {
        this.setState({themes: [], isAPILoaded: true});
        return;
      }

      response.json().then((data) => {
        this.setState({themes: response.status === 200 ? data.data.themes : data.message.message, isAPILoaded: true});
      });
    }).catch((response) => {});
  };

  render() {
    let themes;
    if (!this.state.isAPILoaded) themes = (<CardEmptyTitle>Загрузка...</CardEmptyTitle>);
    else if (typeof this.state.themes !== 'string' && this.state.themes.length > 0) {
      themes = this.state.themes.map(theme => {
        return (<ThemeItem specialist={this.props.specialist} key={theme.id} title={theme.text} id={theme.id}/>);
      });
    } else if (this.state.themes.length === 0) themes = (<CardEmptyTitle>Нет специалистов</CardEmptyTitle>);

    return (
      <div className='qa-specialist'>
        <div className="qa-specialist__header">{this.props.specialist === '/' ? 'Общие темы' : this.props.specialist}</div>

        <div className="qa-specialist__content">
          {themes}
        </div>
      </div>
    );
  }
}
