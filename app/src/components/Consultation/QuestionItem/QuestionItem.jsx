import React, {Component} from 'react';
import AvatarCircle from '../../AvatarCircle';
import {Icon} from 'react-fa';
import moment from 'moment';
import Status from '../QuestionStatus';
import './QuestionItem.scss';
import CommIcon from '../../CommitIcon';
export default class QuestionItem extends Component {
  render() {
    let diff = moment(new Date()).diff(moment(new Date(this.props.question.dateCreated)), 'days');
    let date = moment(new Date(this.props.question.dateCreated)).format('DD.MM.YYYY');

    return (
      <div onClick={() => { window.location.href = `/question?id=${this.props.question._id}` }} className='qa-question'>
        <AvatarCircle img={this.props.question.owner.avatar}/>

        <div className="qa-question__middle">
          <div className="qa-question__text">{this.props.question.text} <Status isClosed={this.props.question.isClosed}/></div>
          <div className="qa-question__created">
            <span className="qa-question__date">Создан: {diff < 2 ? moment(new Date().fromNow(new Date(this.props.question.dateCreated))).fromNow() : date}</span>
            <span className="qa-question__user">Вопрос задал: {this.props.question.owner.fullName}</span>
          </div>
        </div>

        <CommIcon count={this.props.question.comments}/>

      </div>
    );
  }
}

