import React from 'react';
import { Link } from 'react-router-dom';
import Card from '../Card';


import "./NewsGlobal.scss";

export default class NewsGlobal extends React.Component {
  render() {
    return(
    <a href={this.props.link || '#'}  className="NewsGlobal">
      <Card className="NewsGlobal__Block">
          <div className="NewsGlobal__Block__date">
            {this.props.date}
          </div>
          <div className="NewsGlobal__Block__image">
            <img src={this.props.img} className="NewsGlobal__Block__image__img" alt = {this.props.header}/>
          </div>
          <div className="NewsGlobal__Block__text">
            <h2 className="NewsGlobal__Block__text__header">{this.props.header}</h2>
            <p className="NewsGlobal__Block__text__txt">{this.props.text}</p>
          </div>
      </Card>
    </a>
  );
  }
  }