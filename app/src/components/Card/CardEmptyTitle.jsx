import React, {Component} from 'react';

export default class CardEmptyTitle extends Component {
  render() {
    return (
      <h4 className='card__empty-title'>{this.props.children || this.props.text}</h4>
    );
  }
}
