import React from 'react';
import './card.scss';

class card extends React.Component {
  render() {
    return (
      <div onClick={this.props.onClick} style={this.props.style} className={(this.props.className || '') + " card card--" + (this.props.cardShadow || 'default')}>
        {this.props.children}
      </div>
    );
  }
}

export default card;