import React from 'react';
import {Icon} from 'react-fa';
import './DropdownIcon.scss';

class DropdownIcon extends React.Component {
  render() {
    let status = {
      name: this.props.isOpened ? 'angle-up' : 'angle-down'
    };

    return (
      <Icon className="dropdown-icon" name={status.name} />
    );
  }
}

export default DropdownIcon;