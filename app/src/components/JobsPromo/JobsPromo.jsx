import React from 'react';
import img from '../../img/Jobs/1.png';
import {connect} from "react-redux";
import './JobsPromo.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class JobsPromo extends React.Component {
  state = {
    isUserHaveSummary: false,
    isUserHaveJobWithStatusModeration: false
  };

  componentDidMount() {
    const getSummary = () => {
      fetch(`${process.env.API_URL}/user/${this.props.user.info.id}/jobSummary`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.status === 500 || response.status === 404) {
          console.log('Сервер не отвечает');
          return;
        }

        if (response.status === 200) this.setState({isUserHaveSummary: true});
      }).catch(err => {
        throw new Error(err);
      });
    };

    const getUserModerationJobs = () => {
      fetch(`${process.env.API_URL}/jobs/moderation-user/${this.props.user.info._id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.status === 500 || response.status === 404) {
          console.log('Сервер не отвечает');
          return;
        }

        if (response.status === 200) this.setState({isUserHaveJobWithStatusModeration: true});
      }).catch(err => {

      });
    };

    getSummary();
    getUserModerationJobs();
  }

  render() {
    /*const addSummary = () => { window.location.href = '/summary'; };
    const goToSummaryPage = () => { window.location.href = `/summary-profile/${this.props.user.info.id}` };
    const goToUserModerationJobs = () => {  };

    let summaryBubble;
    if (this.state.isUserHaveSummary)
      summaryBubble = (<IconBubble onClick={goToSummaryPage} text='Посмотреть свое резюме' className='JobsPromo__bubble' iconName='address-card-o' background={'transparent'} border={'white'}/>);
    else
      summaryBubble = (<IconBubble onClick={addSummary} text='Написать резюме' className='JobsPromo__bubble' iconName='address-card-o' background={'transparent'} border={'white'}/>);

    let moderationBubble;
    if (this.state.isUserHaveJobWithStatusModeration)
      moderationBubble = (<IconBubble onClick={goToUserModerationJobs} text='Ожидают модерации' className='JobsPromo__bubble' iconName='check' background={'transparent'} border={'white'}/>);*/

    return (
      <div className={"JobsPromo"}>
        <div className={"JobsPromo__image"}>
          <img src={img} alt={"Картинка работы"} className={"JobsPromo__image__img"} />
        </div>

        <div className={"JobsPromo__text"}>
          <h2 className={"JobsPromo__text__header"}>{`Количество вакансий: ${this.props.col} шт.`}</h2>
        </div>
      </div>
    );
  }
}

/*
<div className="JobsPromo__bubbles">
  {summaryBubble}
  <IconBubble text='Банк резюме' className='JobsPromo__bubble' iconName='book' background={'transparent'} border={'white'}/>
  {moderationBubble}
</div>
* */

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(JobsPromo);
