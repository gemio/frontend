import React, {Component} from 'react';
import TreeView from  'react-treeview';
import {Icon} from 'react-fa';
import './WikiStuct.scss';
import Modal from 'react-modal';
import Card from '../Card';
import Input from '../Input';
import Button from "../Button/Button";
import InputSimple from '../InputSimple';
import {connect} from 'react-redux';
import image from  '../../img/None.png';

class WikiStruct extends Component {
  state = {
    collapse: true,
    modalIsOpen: false,
    type: 'раздела',
    ImageNewPost: image,
    ErrorList: ""
  };

  toggleModal = () => {
    this.setState({modalIsOpen: !this.state.modalIsOpen});
  };

  getState = (id) => {
    fetch(`${process.env.API_URL}/lib?id=${id}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      response.json().then(data => {
        this.props.handle && this.props.handle(data.data);
      });
    });
  };

  onChange = () => {
    const type = document.getElementById('type').value;
    if (type === 'Статья') this.setState({type: "статьи"});
    else this.setState({type: "раздела"});
  };

  AddTheme = (id) => {
    const title = document.getElementById('header_wiki').value;
    if (!title) {
      this.setState({ErrorList: 'Заполните все поля!'});
      return;
    }

    if (this.state.type !== "статьи") {
      fetch(`${process.env.API_URL}/lib/folder/${this.props.user.info._id}?parentId=${id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `title=${title}`
      }).then(response => {
        if (response.status === 200) {
          this.toggleModal();
          this.props.onUpdate && this.props.onUpdate();
        }
      }).catch(err => {
        this.setState({ErrorList: err});
      });
    } else {
      let image = document.getElementById('myFile').files;
      let about = document.getElementById('about').value;

      if (image.length === 0 || !about) {
        this.setState({ErrorList: 'Заполните все поля'}, () => {
          document.getElementById('about').value = about;
        });
        return;
      }

      let data = new FormData();

      data.append('image', image[0]);
      data.append('info', JSON.stringify({title: title, text: about}));
      data.append('parent', id);

      fetch(`${process.env.API_URL}/lib/book/${this.props.user.info._id}`, {
        method: 'POST',
        body: data
      }).then(response => {
        if (response.status === 200) {
          this.props.onUpdate && this.props.onUpdate();
          this.toggleModal();
        }
      }).catch(err => {
        this.setState({ErrorList: err});
      });
    }
  };

  DropTheme = (id) => {
    fetch(`${process.env.API_URL}/lib/${this.props.user.info._id}?id=${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }).then(response => {
      this.props.onUpdate && this.props.onUpdate();
    });
  };

  FileUpload = () => {
    const about = document.getElementById('about').value;
    let file = document.querySelector('#myFile').files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      this.setState({ImageNewPost: reader.result}, () => {
        document.getElementById('about').value = about;
      });
    };

    if (file) reader.readAsDataURL(file);
  };

  expand = () => {
    this.setState({collapse: !this.state.collapse});
  };

  render() {
    const customStylesModal = {
      content : {
        padding: '25px 35px',
        display: 'flex',
        height: 'fit-content',
        width: 'fit-content',
        algorithm: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        top: '50%',
        left: '50%',
        bottom: 'initial',
        right: 'initial',
        transform: 'translate(-50%, -50%)'
      },

      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };

    const nodeLabel = (
      <div className={"wiki-structure__label"}>
        <span onClick={this.expand}>{this.props.nodeLabel}</span>

        {
          this.props.isAdmin ? 
          <span className={"wiki-structure__actions"}>
            <Icon name="plus" className="wiki-structure__icon wiki-structure__icon--green" onClick={this.toggleModal} />
            <Icon name="minus" className="wiki-structure__icon wiki-structure__icon--red" onClick={() => { this.DropTheme(this.props.id); }} />
          </span> : null
        }
      </div>
    );

    let children;
    if (this.props.children && this.props.children.length > 0) {
      children = this.props.children.map(item => {
        if (item.isBook)
          return (<div className="wiki-structure__item" key={item._id} onClick={() => this.getState(item._id)} id={item._id}>{item.name}</div>);
        else
          return (<WikiStruct isAdmin={this.props.isAdmin} onUpdate={this.props.onUpdate} key={item._id} handle={this.props.handle} user={this.props.user} id={item._id} children={item.children} nodeLabel={item.name}/>)
      });
    }

    return (
      <TreeView defaultCollapsed={this.state.collapse} collapsed={this.state.collapse} nodeLabel={nodeLabel}>
        {children}

        <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.modalIsOpen} onRequestClose={this.toggleModal}>
          <Icon name="times" className="Icon__close__wiki" onClick={this.toggleModal}/>
          <div className="wiki-structure__error">{this.state.ErrorList}</div>

          <div className="wiki-structure__field">
            <div className="wiki-structure__subtitle">Тип добавления</div>
            <Input items={['Раздел', 'Статья'].join('&')} onChange={this.onChange} id="type" type="select" className={"wiki-structure__type"} />
          </div>

          {this.state.type === "статьи" ?
            <div>
              <div className={"wiki-structure__field"}>
                <div className={"wiki-structure__subtitle"}>Заголовок статьи:</div>
                <Input id="header_wiki" className="wiki-structure__add-title" type="text" />
              </div>

              <InputSimple id="about" labelText="Текст статьи" cols="30" rows="10" className="wiki-structure__text" isMultiline={true}/>

              <div className={"wiki-structure__image"}>
                <img src={this.state.ImageNewPost} alt='book'/>
                <input id="myFile" type="file" onChange={this.FileUpload}/>
              </div>
            </div> :

            <div className={"wiki-structure__field"}>
              <div className={"wiki-structure__subtitle"}>Заголовок раздела</div>
              <Input id="header_wiki" className="wiki-structure__add-title" type="text" />
            </div>
          }

          <Button onClick={() => this.AddTheme(this.props.id)} text={`Добавление ${this.state.type}`} className='button--primary-ghost wiki-structure__add-button' />
        </Modal>
      </TreeView>
    );
  }
}

export default connect (
  state => ({
  user: state.user
})) (WikiStruct);