import React from 'react';
import Input from '../Input';
import "./AddConsiltation.scss";
import Button from '../Button';
import {connect} from "react-redux";
class AddConsultation extends React.Component {
  state = {
    typeItem:[],
    isActive:false,
    typeQuest:[]
  };

  AddQuest = () => {
    fetch(`${process.env.API_URL}/specialist/question/${this.props.user.info._id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      BODY: `specialist=${document.getElementById("type").value}&theme=${document.getElementById("Question").value}&text=${document.getElementById("Question").value}`,
    }).then((response) => {
      console.log(response);
    });

    }

  componentDidMount()
  {
    let dat="";
    fetch(`${process.env.API_URL}/specialists`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      response.json().then((data) =>{
        this.setState({typeItem: data.data.specialists});
        dat = data.data.specialists[0];
        let func=[];
        fetch(`${process.env.API_URL}/specialist/themes?specialist=${dat}`, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
        }).then((response) => {
          response.json().then((data) =>{
            for (let theme of data.data.themes) func.push(theme.text)
            this.setState({typeQuest: func});
          });
        });
      });
    });
  }

  render() {
    console.log(this.props.user);
    const onChange = (type) => {
      let dat = document.getElementById("type").value, func = [];
      fetch(`${process.env.API_URL}/specialist/themes?specialist=${dat}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        response.json().then((data) => {
          for (let theme of data.data.themes) func.push(theme.text)
          this.setState({typeQuest: func});
        });
      });

    };
    return (
      <div className={"AddConsultation row"}>
        <div className={"AddConsultation__Type col-md-6"}>
          <h2 className="AddConsultation__Type__header">Тип</h2>
          <Input items={this.state.typeItem.join('&')}   required={true} id="type" className="AddConsultation__Type__Input" onChange={onChange} type="select" />
        </div>
        <div className={"AddConsultation__Question col-md-6"}>
          <h2 className="AddConsultation__Type__header">Тип</h2>
          <Input items={this.state.typeQuest.join('&')}   required={true} id="role" className="AddConsultation__Type__Input" type="select" />
        </div>
        <div className={"AddConsultation__Type__down col-md-12"}>
          <h2 className="AddConsultation__Type__header">Ваш вопрос:</h2>
          <textarea className={"AddConsultation__Type__TextArea"} id={"Question"} />
        </div>
        <Button onClick={this.AddQuest} className='button--primary-ghost AddConsultation__Type__Button' text='Добавить'/>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
)(AddConsultation);