import {Tabs} from 'react-web-tabs';
import React from 'react';
import './TabControlHome.scss';

export default class TabControlHome extends React.Component {
  render() {
    return (
      <Tabs className={this.props.className} defaultTab={this.props.defaultTabID} onChange={this.props.onChange}>
        {this.props.children}
      </Tabs>
    );
  }
}
