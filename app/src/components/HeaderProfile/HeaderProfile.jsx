import React from 'react';
import {Icon} from 'react-fa';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import AvatarCircle from '../AvatarCircle';
import NotificationCount from '../NotificationCount';
import HeaderDrop from '../HeaderDrop';
import './HeaderProfile.scss';

const UserAction = require('../../actions/UserAction.js');

 class HeaderProfile extends React.Component {
   state = {
     notificationCount: 0,
     visible:"profile--hidden"
   };

   Click = () => {
     this.setState({visible: this.state.visible === "profile--active" ? "profile--hidden" : "profile--active"});
   };

   render() {
    let profile;
    if (this.props.user.authorized) {
      let count = this.props.notification.friends + this.props.notification.messages;
      profile = (
        <div style={{cursor: 'pointer'}} className={'header__profile profile'}>
          <div className="profile__visible">
            <AvatarCircle onClick={() => {window.location.href = '/home'}} img={this.props.user.info.avatar}/>
            <NotificationCount isPrimary={true} className="profile__noticication-count" count={count}/>
            <Icon name="angle-down" onClick={this.Click} />
          </div>
          <div className="profile__dropdown profile--hidden"/>
          <HeaderDrop onClickHandle={this.Click} className={this.state.visible}/>
        </div>
      );
    } else if (!this.props.user.authorized && !this.props.user.info._id)
      profile = (
        <div className={'header__profile profile profile--not-authorize'}>
          <Link to="/login" className="profile__authorize">ВХОД</Link>
        </div>
      );
    else if (!this.props.user.authorized && this.props.user.info._id)
      profile = (
        <div className={'header__profile profile profile--not-authorize'}>
          <Link onClick={this.props.onLogout} to="/login" className="profile__authorize">ВЫХОД</Link>
        </div>
      );

    return (
      <div>
        {profile}
      </div>
    );
  }
}
export default connect(
  state => ({
    user: state.user,
    notification: state.notification
  }),

  dispatch => ({
    onLogout: () => {
      dispatch(UserAction.default.logOut());
    }
  })
)(HeaderProfile);
