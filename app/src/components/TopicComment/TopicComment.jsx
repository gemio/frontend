import React from 'react';
import "./TopicComment.scss";

export default class TopicComment extends React.Component {
  render() {
    return (
      <div className={"TopicComment "+ (this.props.className || '')}>
        <div className={"row"}>
          <div className={"col-md-3 TopicComment__Left"}>
          <a href={`/id/${this.props.href}`}><img src={this.props.image}/>
            <p>{this.props.author}</p>
          </a>
          </div>
          <div className={"col-md-9 TopicComment__Right"}>
            <div className={"TopicComment__Right__date"}>{this.props.date}</div>
            <div className={"TopicComment__Right__content"}>{this.props.text}</div>
          </div>
        </div>
      </div>
    );
  }
}