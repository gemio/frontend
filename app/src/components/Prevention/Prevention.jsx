import React from 'react';
import InputSimple from '../InputSimple';
import Radio from '../RadioButton/Radio';

import './Prevention.scss';

class Prevention extends React.Component {
  state = {
    type: 'Immunate'
  };

  render() {
    const handleChange = (e) => {this.setState({type: e.target.value});};

    return (
      <div className="Prevention">
        <div style={{display: 'flex'}} className="Prevention__radios">
          <Radio name='medicament' value='Immunate' checked={this.state.type === 'Immunate'} handleChange={handleChange}/>
          <Radio name='medicament' value='CSL Behring' checked={this.state.type === 'CSL Behring'} handleChange={handleChange}/>
          <Radio name='medicament' value='Octanate' checked={this.state.type === 'Octanate'} handleChange={handleChange}/>
        </div>
        <InputSimple onlyNum={true} max={5000} min={100} onlyNumLabel="ME" labelText="Дозировка" className="add-remind__dosage"/>
      </div>
    );
  }
}

export default Prevention;