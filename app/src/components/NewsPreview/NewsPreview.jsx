import React from 'react';
import "./NewsPreview.scss";

export default class NewsPreview extends React.Component {
  render() {
    return (
      <div className="news-preview">
        <div className="news-preview__full-date">
          <div className="news-preview__day">{this.props.day}</div>
          <div className="news-preview__date">{this.props.date}</div>
        </div>

        <div className="news-preview__content">
          <div className="news-preview__title">{this.props.title}</div>
          <div className="news-preview__text">{this.props.text}</div>
          <a className="news-preview__link" href={this.props.link}>Подробнее</a>
        </div>

        <img className="news-preview__image" src={this.props.image} alt="gem-news"/>
      </div>
    );
  }
}
