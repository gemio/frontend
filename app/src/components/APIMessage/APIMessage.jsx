import React from 'react';
import './APIMessage.scss';

export default class APIMessage extends React.Component {
  render() {
    return (
      <div className={(this.props.className || '') + ` api__message api__message--${this.props.status}`}>
        {this.props.text}
      </div>
    );
  }
}
