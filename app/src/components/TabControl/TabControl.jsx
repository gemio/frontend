import {Tabs} from 'react-web-tabs';
import React from 'react';
import './TabControl.scss';

export default class TabControl extends React.Component {
  render() {
    const pushTabState = (id) => {
      window.history.pushState({id: id}, id, `?tab=${id}`);
      if (this.props.onChange) this.props.onChange(id);
    };
    let tab = window.history.state ? window.history.state.id ? window.history.state.id : '' : '';

    return (
      <Tabs className={this.props.className} defaultTab={this.props.defaultTab || tab} onChange={(e) => {!this.props.noState && pushTabState(e)}} vertical={this.props.vertical}>
        {this.props.children}
      </Tabs>
    );
  }
}
