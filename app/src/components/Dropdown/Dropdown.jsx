import React from 'react';
import './Dropdown.scss';

class Dropdown extends React.Component {
  render() {
    return (
      <select className={(this.props.className || '') + ' dropdown'} id={this.props.id} value={this.props.value} onChange={this.props.handleChange} onBlur={this.props.onBlur} onFocus={this.props.onFocus} type={this.props.type}>
        {this.props.children}
      </select>
    );
  }
}

export default Dropdown;
