import React from 'react';
import Form from '../Form';
import Input from '../Input';
import Button from '../Button';
import "./ForgotPasswordForm.scss";
import APIMessage from '../APIMessage';

export default class ForgotPasswordForm extends React.Component {
  state = {
    messageText: ''
  };

  render() {
    let apiAuth = () => {
      let user = {email: document.getElementById('email').value};
      fetch(`${process.env.API_URL}/user/resetPassword/link?email=${user.email}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        response.json().then((data) => {this.setState({errorText: data.message.message});});
      });
    };
   
    return (
      <div>
        <APIMessage className="forgot__title__coment__m" status={this.state.isLoginSuccess ? 'success' : 'error'} text={this.state.errorText} />
        <h5 className="forgot__title__coment">Укажите ваш адрес электронной почты: </h5>
        
        <Form className="forgot__form" submitCallback={apiAuth}>
          <Input required={true} id="email" className="forgot__email" type="text" labelText="Email"/>
          <Button type="submit" text="ВОССТАНОВИТЬ ПАРОЛЬ" className="button--primary forgot__submit"/>
        </Form>
      </div>
    );
  }
}


