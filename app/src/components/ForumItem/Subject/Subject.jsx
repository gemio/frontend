import React from 'react';
import CommitIcon from '../../CommitIcon';
import {Icon} from 'react-fa';
import "./Subject.scss";
import { Link } from 'react-router-dom';
class Subject extends React.Component {
  render() {
    return (
      <div className={"Subject"} >
        <Icon name='folder-open' className={"Subject__image"}/>
        <div className={"Subject__header"}>
          <a href={"/forum/"+this.props.id} > <h4 className={"Subject__header__header"}>{this.props.header}</h4>      </a>
          <div className={"Subject__header__str"}>{

            this.props.str.length > 0 ?
            this.props.str.map((item,index) => ((<a key={item.id} href={`/forum/${item.id}`} className={"Subject__header__str__link"}>{item.root}{(index+1)!== this.props.str.length ?(","):("") }</a>))):""
          }</div>
        </div>
          <CommitIcon count={this.props.count} className={"Subject__EndBlock"}/>
      </div>
    );
  }
}

export default Subject;