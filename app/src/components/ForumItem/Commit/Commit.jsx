import React from 'react';

//Поключение элементов
import CommitIcon from '../../CommitIcon';
import { Link } from 'react-router-dom';

// Подключение стилий
import "./Commit.scss";
class Commit extends React.Component {
  constructor() {
    super(...arguments);
  }


  render() {
    return (
      <div className={"CommitForumItem"}>
        <img src={this.props.image} className={"CommitForumItem__image"}/>
        <div className={"CommitForumItem__header"}>
          <h4 className={"CommitForumItem__header__head"}> <a href={"/topic/"+this.props.topicid}> {this.props.header}</a></h4>
            <h5 className={"CommitForumItem__header__Author"}>Автор: <span className={"CommitForumItem__header__Author__Auth"}><a href={"/id/"+this.props.href} >{this.props.Author}</a></span> {this.props.date}</h5>
        </div>
        <div className={"CommitForumItem__EndBlock"}>
          <CommitIcon count={this.props.answer} className={"CommitForumItem__CentralBlock__item"}/>
          {
            this.props.comments.user ?  < img src = {this.props.comments.user.avatar} className={"CommitForumItem__EndBlock__image"}/>:<div/>
          }
          <div className={"CommitForumItem__EndBlock__block"}>
            <p className={"CommitForumItem__EndBlock__block__Author"}><span className={"CommitForumItem__header__str__link"}><a href={"/id/"+(this.props.comments.user ? this.props.comments.user.userID : "")}> {this.props.comments.user ? this.props.comments.user.fullName : ""}</a> </span></p>
            <p className={"CommitForumItem__EndBlock__block__date"}>{this.props.olddate}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Commit;