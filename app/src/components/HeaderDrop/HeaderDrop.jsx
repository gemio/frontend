import React from 'react';
import Card from '../Card';
import SlideMenuNav from "../SlideMenuNav";
import Item from "../Item";
import {connect} from "react-redux";
import "./HeaderDrop.scss";
const UserAction = require('../../actions/UserAction.js');

 class HeaderDrop extends React.Component {
   menuItemClick = () => {
     if (this.props.onClickHandle) this.props.onClickHandle();
   };

  render() {
     return (
      <Card className={this.props.className + "  HeaderDrop"} >
      	<div className="slide-menu">
          <SlideMenuNav>
            <Item onClick={this.menuItemClick} isActive={window.location.href.includes('/home')} isLink={true} link={`${process.env.URL_FOLDER}home`} showIcon={true} margin="15" iconName="home" text="Моя страница"/>
            <Item onClick={this.menuItemClick} isActive={window.location.href.includes('/treatment') && !window.location.href.includes('treatment-')} isLink={true} link={`${process.env.URL_FOLDER}treatment`} showIcon={true} margin="15" iconName="calendar" text="Журнал"/>
            <Item onClick={this.menuItemClick} isActive={window.location.href.includes('/treatment-plan')} isLink={true} showIcon={true} margin="15" iconName="plus" link={`${process.env.URL_FOLDER}treatment-plan`} text="План лечения"/>
            <Item onClick={this.menuItemClick} isActive={window.location.href.includes('/dialog')} notificationCount={this.props.notification.messages} isLink={true} link={`${process.env.URL_FOLDER}dialogs`} showIcon={true} margin="15" iconName="envelope" text="Сообщения"/>
            <Item onClick={this.menuItemClick} isActive={window.location.href.includes('/friends')} notificationCount={this.props.notification.friends} isLink={true} link={`${process.env.URL_FOLDER}friends`} showIcon={true} margin="15" iconName="user" text="Друзья"/>
            <Item onClick={this.menuItemClick} isActive={window.location.href.includes('/forum')} isLink={true} link={`${process.env.URL_FOLDER}forum`} showIcon={true} margin="15" iconName="group" text="Форум"/>
            <Item onClick={this.menuItemClick} isActive={window.location.href.includes('/profileEdit')} isLink={true} link={`${process.env.URL_FOLDER}profileEdit`} showIcon={true} margin="15" iconName="cog" text="Настройки"/>
            <Item onClick={this.props.onLogout} isActive={false} isLink={true} showIcon={true} margin="15" iconName="power-off" text="Выйти"/>
          </SlideMenuNav>
          </div>
      </Card>
    );
  }
}
export default connect(
  state => ({
    notification: state.notification,
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLogout: () => {
      dispatch(UserAction.default.logOut());
    }
  })
)(HeaderDrop);