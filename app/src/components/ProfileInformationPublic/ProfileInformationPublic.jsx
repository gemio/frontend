import React from 'react';


class ProfileInformationPublic extends React.Component {
  render() {
    return (
      <div className={(this.props.className || '') + ' profile-info__public'}>
        <ul>
          {this.props.children}
        </ul>
      </div>
    );
  }
}

export default ProfileInformationPublic;