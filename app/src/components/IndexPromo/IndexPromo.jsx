import React from 'react';
import { Link } from 'react-router-dom';
import Col from '../Col';
import Container from '../Container';
import Row from '../Row';
import Button from '../Button';

import IndexPromoBubble from '../IndexPromoBubble';
import './IndexPromo.scss';

export default class IndexPromo extends React.Component {
  render() {
    return (
      <Container className="promo">
        <Row>
          <Col lg="6" xs="12" className="promo__left">
            <h1 className="promo__title">Гемофилик</h1>
            <h2 className="promo__subtitle">сообщество больных гемофилией</h2>

            <div className="promo__description">
              площадка для обмена информацией, взаимопомощи и<br/>
              проведения консультаций между больными,<br/>
              родителями и врачами по всей России
            </div>

           <Link to="registration"><Button className="promo__join button--primary button--rounded" text="ПРИСОЕДИНИТЬСЯ"/></Link>
          </Col>

          <Col lg="6" xs="12" className="promo__right">
            <IndexPromoBubble />
          </Col>
        </Row>
      </Container>
    );
  }
}
