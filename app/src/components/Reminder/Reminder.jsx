  import React from 'react';
import Card from '../Card';
import {Icon} from 'react-fa';
import './Reminder.scss';
import { connect } from 'react-redux';

class Reminder extends React.Component {
  state = {
    type: this.props.type
  };

  render() {
    const handleClick = (key,token) => {
       fetch(`${process.env.API_URL}/remind/${token}?id=${key}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.status === 500 || response.status === 404) {
          this.setState({posts: '', remindsLoaded: true});
          return;
        }
         window.location.reload();
      }).catch(err => {

      });
    };

    let typeClassName = 'reminder__type--primary';
    const { type } = this.state;
    if (type === 'профилактика' || type === 'Онлайн-консультация') typeClassName = 'reminder__type--warning';
    else if (type === 'Санаторно-курортное лечение') typeClassName = 'reminder__type--purple';
    else if (type === 'Осмотр специалиста') typeClassName = 'reminder__type--blue';

    return (
      <Card cardShadow='no-shadow' className={"reminder "+ this.props.className}>
       
        <div className={typeClassName + " reminder__type"}>{this.props.type}</div>
        <div className="reminder__text">{this.props.text}</div>
        <div className="reminder__date">{this.props.date}</div>
        <Icon className="reminder__close" name="times"  onClick={() => {handleClick(this.props.id,this.props.user.info._id)}} />
          
      </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  })
)(Reminder);