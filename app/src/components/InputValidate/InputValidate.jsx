import React from 'react';
import {Icon} from 'react-fa';
import './InputValidate.scss';

class InputValidate extends React.Component {
  render() {
    let validateIcon = {
      name: this.props.isSuccess ? 'check' : '',
      className: this.props.isSuccess ? 'input__validate-icon--success' : ''
    };

    return (
      <Icon className={(validateIcon.className || '') + ' input__validate-icon'} name={validateIcon.name}/>
    );
  }
}

export default InputValidate;
