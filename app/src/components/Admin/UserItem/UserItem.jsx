import React from 'react';
import  Avatar from '../../AvatarCircle/index';
import Button from '../../Button/index';
import {connect} from "react-redux";
import "./UserItem.scss";
import Modal from 'react-modal';
import {Icon} from 'react-fa';
import DropdownSimple from '../../DropdownSimple';

class UserItem extends React.Component {
  state = {
    modalIsOpen: false,
    srcimage: "",
    isChangingRole: false
  };

  openModal = (src) => {
    this.setState({modalIsOpen: true, srcimage: src});
  };

  closeModal = () => {
    this.setState({modalIsOpen: false, srcimage: ""});
  };

  Delete = () =>  {
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/removeUser?userID=${this.props.userid}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (this.props.actionHandler) this.props.actionHandler();
      else window.location.reload();
    });
  };

  Deactivate = () => {
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/deactivate?userID=${this.props.userid}`, {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (this.props.actionHandler) this.props.actionHandler();
      else window.location.reload();
    });
  };

  DownloadDocument = () => {
    const link = document.createElement('a');
    link.setAttribute('href', this.state.srcimage);
    link.setAttribute('download', this.state.srcimage);
    link.style = 'display: none';
    document.body.appendChild(link);
    link.click();
    document.removeChild(link);
  }

  Activate = () => {
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/regConfirm?userID=${this.props.userid}`, {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (this.props.actionHandler) this.props.actionHandler();
      else window.location.reload();
    });
  };

  toggleRoleChange = () => {
    this.setState({isChangingRole: !this.state.isChangingRole});
  };

  changeRole = () => {
    let role = document.getElementsByClassName(`user-item__roles-${this.props.userid}`)[0].value;
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/role?userID=${this.props.userid}&role=${role}`, {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (this.props.actionHandler) {
        this.toggleRoleChange();
        this.props.actionHandler();
      } else window.location.reload();
    });
  };

  render() {
    const customStylesModal = {
      content : {
        maxWidth: '670px',
        top: '50px',
        position:'absolute',
        left: '50%',
        right: 'auto',
        marginRight: '-50%',
        transform: 'translateX(-50%)',
        zIndex: '10000',
        maxHeight: 'max-content',
        padding: '34px',

      },

      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };
    const roles = [
      'Пользователь',
      'Специалист-консультант',
      'Администратор',
      'Модератор'
    ];

    let documents;
    if (!this.props.isConfirmed && this.props.document && this.props.document.length > 0) {
      documents = (
        <div className={"user-item__documents-container"}>
          <div className={"user-item__documents-header"}>Документы</div>

          <div className={"user-item__documents"}>
            {this.props.document.map((item, index) => ((<img key={index} src={item} alt={"document"}  onClick={() => {this.openModal(item)}} />)))}
          </div>
        </div>
      );
    }

    return (
      <div className="user-item">
        <Avatar img={this.props.image} className="user-item__avatar" />

        <div className="user-item__info">
          {this.props.isConfirmed ?
            <span className="user-item__full-name">
              <span onClick={() => {window.location.href = `/id/${this.props.userid}`}}>{this.props.name}</span>

              <div className={"user-item__edit-role" + (this.state.isChangingRole ? " user-item__edit-role--visible" : '')}>
                <DropdownSimple selectedItem={this.props.type} className={`user-item__roles-${this.props.userid}`} items={roles}/>
                <Icon onClick={this.changeRole} name='check'/>
                <Icon onClick={this.toggleRoleChange} name='times'/>
              </div>
            </span>
            :
            <p className="user-item__full-name-ConfirmedNone">{this.props.name}</p>
          }

            <p  onClick={this.props.activit ? this.toggleRoleChange :""}
               className={"user-item__role " + (this.state.isChangingRole ? '' : 'user-item__role--visible')}>
              {this.props.type}
              {this.props.activit ?  <Icon onClick={this.toggleRoleChange} className='user-item__role-change' name='pencil'/>:"" }
            </p>


        </div>
        {
          !this.props.isDocument ?
            this.props.isConfirmed ?
            <div className='user-item__actions'>
              <Button onClick={this.Delete} className='button--orange-ghost' text='Удалить аккаунт'/>
              <Button onClick={this.Deactivate} className='button--gray-ghost' text='Деактивировать аккаунт'/>
            </div> :

            <div className={(!this.props.isConfirmed ? 'user-item__actions--not-confirm ' : '') + 'user-item__actions'}>
              <Button onClick={this.Activate} className='button--primary-ghost' text='Подтвердить акаунт'/>
              <Button onClick={this.Deactivate} className='button--orange-ghost' text='Отклонить заявку'/>
            </div>

          :<div/>
        }
        {documents}
        <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal} className={" ForumUserItem "} >
          <div>
            <img alt='document' src={this.state.srcimage} className={"Modal__src__UserItem"}/>
            <Icon onClick={() => {this.closeModal()}} name='times'/>
          </div>
          <div className={"Modal__src__downloads"} onClick={this.DownloadDocument}><Icon name='arrow-down'/>Скачать файл</div>
        </Modal>
      </div>
    )
  }
}
export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),
)(UserItem);
