import React from 'react';
import './WikiList.scss';

class WikiList extends React.Component {
  render() {
    return (
      <div>
        <h2 className="wiki__list-title">{this.props.title}</h2>
        <ul className={"wiki__list " + (this.props.className || '')}>
          {this.props.children}
        </ul>
      </div>
    );
  }
}

export default WikiList;
