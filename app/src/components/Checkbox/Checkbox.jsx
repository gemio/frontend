import React, {Component} from 'react';
import './Checkbox.scss';

export default class Checkbox extends Component {
  render() {
    return (
      <label className="checkbox__container">
        <input type="checkbox"/>
        <span className="checkbox__custom" />
        {this.props.text}
      </label>
    );
  }
}
