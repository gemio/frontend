import React from 'react';
import './FriendList.scss';

export default class FriendList extends React.Component {
  render() {
    return (
      <ul className="friends__list">
        {this.props.children}
      </ul>
    );
  }
}
