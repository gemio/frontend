import React from 'react';
import {Icon} from 'react-fa';
import { Link } from 'react-router-dom';
import NotificationCount from '../NotificationCount';
import './Item.scss';

class Item extends React.Component {
  state = {
    showIcon: this.props.showIcon || false,
    isLink: this.props.isLink || false
  };

  render() {
    let icon = '';
    if (this.state.showIcon)
      icon = ( <Icon style={{display: 'inline-block', textAlign: 'right', width: '15px'}} name={this.props.iconName} /> );

    let item = (<span style={{marginLeft:  (this.props.margin || 10) + 'px'}}>{this.props.text}</span>);
    if (this.state.isLink)
      item = (<Link to={this.props.link || '#'}>{item}</Link>);

    return (
      <li onClick={this.props.onClick} className={(this.props.className || '') + ' item ' + (this.props.isActive ? 'item--active' : '')}>
        {icon} {item}
        <NotificationCount isPrimary={false} className="slide-menu__notification-count" count={this.props.notificationCount || 0}/>
      </li>
    );
  }
}

export default Item;
