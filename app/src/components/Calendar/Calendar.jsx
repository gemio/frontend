import React from 'react';

import Calend from 'react-calendar'
import './Calendar.scss';

class Calendar extends React.Component {
  constructor() {
    super(...arguments);

    this.state = {
      date: new Date()
    }
  }

  render() {
    return (
    	<div>
        <Calend locale="ru-RU" onChange={this.props.onChange} value={this.state.date} selectRange={true} />
      </div>
    );
  }
}

export default Calendar;
