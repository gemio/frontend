import React, { Component } from 'react';
import './RadioButton.scss';

export default class Radio extends Component {
  render() {
    return (
      <label className={'radio-button' + (this.props.className || '')}>
        <input value={this.props.value} name={this.props.name} type="radio" checked={this.props.checked || false} onChange={this.props.handleChange}/>
        <span className="radio-button__value">{this.props.value}</span>
        <span className="radio-button__container"/>
      </label>
    );
  }
}
