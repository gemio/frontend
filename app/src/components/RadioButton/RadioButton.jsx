import React, {Component} from 'react';
import './RadioButton.scss';

export default class RadioButton extends Component {
  state = {
    preChecked: false,
    radio: this.props.value,
    checked: this.props.isChecked || false
  };

  handleChange = (event) => this.setState({radio: event.target.value});

  componentWillReceiveProps() {
    let preChecked = this.state.preChecked;
    if (!preChecked && this.props.isChecked) {
      this.setState({preChecked: true, checked: true});
    } else
      this.setState({checked: this.state.radio === this.props.value});
  }

  render() {
    return (
      <label className={'radio-button' + (this.props.className || '')}>
        <input name={this.props.name} type="radio" checked={this.state.checked} onChange={this.handleChange}/>
        <span className="radio-button__value">{this.props.value}</span>
        <span className="radio-button__container"/>
      </label>
    );
  }
}
