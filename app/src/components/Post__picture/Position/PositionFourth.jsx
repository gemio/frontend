import React from 'react';


import './position.scss';

export default class PositionFourth extends React.Component {
  render() {
    return (
      <div className="row">
        <div className="col-6 post_picture__row">
          <img alt='gem-post' src={this.props.images[0]} className="position__image_four"  />
          <img alt='gem-post' src={this.props.images[1]} className="position__image_four" />
        </div>

        <div className="col-6 post_picture__row post_picture_Right">
          <img alt='gem-post' src={this.props.images[2]} className="position__image_four" />
          <img alt='gem-post' src={this.props.images[3]} className="position__image_four" />
        </div>
      </div>
    )
  }
}