import React from 'react';

import './position.scss';

export default class PositionFive extends React.Component {
  render() {
    return (
      <div className="row">
        <div className="col-4 post_picture__row">
          <img alt='gem-post' src={this.props.images[0]} className="position__image" />
          <img alt='gem-post' src={this.props.images[1]} className="position__image" />
        </div>

        <div className="col-4 post_picture__row">
          <img alt='gem-post' src={this.props.images[2]} className="position__image" />
          <img alt='gem-post' src={this.props.images[3]} className="position__image" />
        </div>

        <div className="col-4 post_picture__row">
          <img alt='gem-post' src={this.props.images[4]} className="position__image__5" />
        </div>
      </div>
    )
  }
}