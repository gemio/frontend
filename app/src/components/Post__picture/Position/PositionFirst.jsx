import React from 'react';


import './position.scss';


export default class PositionFirst extends React.Component {
  render() {
    return (
      <div className="col-12 post_pictury__row">
      	<img alt='gem-post' src={this.props.images[0]} className="position__image_one"/>
      </div>
    )
  }
}