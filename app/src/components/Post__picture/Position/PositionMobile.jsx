import React from 'react';
import Col from '../../Col';
import Row from '../../Row';
import { Link } from 'react-router-dom';

import './position.scss';

export default class PositionMobile extends React.Component {
  render() {
    return (
      <Row>
        <Col xs="12" className="post_picture__row position__relative">
          <img alt='gem-post' src={this.props.images[0]} className="position__image__6" />
          <Link to="/post" className="position__number">
            <h2 className="position__number__cell">{`+${this.props.images.length}`}</h2>
          </Link>
        </Col>
      </Row>
    )
  }
}