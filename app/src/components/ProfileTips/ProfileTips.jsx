import React from 'react';
import './ProfileTips.scss';
import Card from '../Card';

class ProfileTips extends React.Component {
  render() {
    return (
      <Card cardShadow='no-shadow' className="profile-tips__card">
        <ul className="profile-tips">
          {this.props.children}
        </ul>
      </Card>
    );
  }
}

export default ProfileTips;