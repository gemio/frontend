import React from 'react';
import Card from '../Card';
import Checkbox from '../Checkbox';

import "./SearchTypeList.scss";

class SearchTypeList extends React.Component {
  render() {
    return (
      <Card className={`search-filter ${this.props.hidden ? 'search-filter--hidden' : ''}`}>
        <form className={`search-filter__form`}>
          <Checkbox text='Пользователи'/>
          <Checkbox text='Новости'/>
          <Checkbox text='Посты'/>
          <Checkbox text='Форум'/>
          <Checkbox text='Вакансии'/>
          <Checkbox text='Пользователи'/>
          
          <div className="search-filter__submit">Найти</div>
        </form>
      </Card>
    );
  }
}

export default SearchTypeList;
