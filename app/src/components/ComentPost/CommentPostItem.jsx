import React, { Component } from 'react';
import AvatarCircle from '../AvatarCircle';
import moment from 'moment';
import "./CommentPostItem.scss";

export default class CommentPostItem extends Component {
  render() {
    let diff = moment(new Date()).diff(moment(new Date(this.props.data)),'days');
    let date = moment(new Date(this.props.data)).format('DD.MM.YYYY');

    return (
      <div className={"post-comment"}>
        <div className="post-comment__user">
          <AvatarCircle img={this.props.img} className ="post-comment__avatar"/>
          <a href={this.props.href} className={"post-comment__full-name"}>{this.props.name}</a>
        </div>

        <div className={"post-comment__text"}>{this.props.text}</div>
        <div className={"post-comment__date"}> {diff < 2 ? moment(new Date().fromNow(new Date(this.props.data))).fromNow() :date} </div>
      </div>
    );
  }
}
