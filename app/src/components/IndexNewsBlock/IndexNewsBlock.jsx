import React from 'react';
import { Link } from 'react-router-dom';
import './IndexNewsBlock.scss';

export default class IndexNewsBlock extends React.Component {
  render() {
    return (
      <div className="IndexNewsBlock">
        <img alt="gem" src={this.props.image} className="IndexNewsBlock__image__img" />
        <div className="IndexNewsBlock__body">
          <div className="IndexNewsBlock__body__title">{this.props.title}</div>

          <div className="IndexNewsBlock__body__date">
            <div className="IndexNewsBlock__body__date__time">{this.props.time}</div>
            <div className="IndexNewsBlock__body__date__dat">{this.props.date}</div>
          </div>

          <div className="IndexNewsBlock__body__More">
            <Link to={this.props.link || '/'}>Подробнее</Link>
          </div>
        </div>
      </div>
    );
  }
}
