import React from 'react';
import './ProfileInformationAbout.scss';

class ProfileInformationAbout extends React.Component {
  render() {
    return (
      <div className={(this.props.className || '') + ' profile-info__about'}>
        <span>Обо мне:</span>
        <span className='profile-info__about-text'>
          {this.props.children}
        </span>
      </div>
    );
  }
}

export default ProfileInformationAbout;