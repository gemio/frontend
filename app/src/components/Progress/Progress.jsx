import React from 'react';
import './Progress.scss';

export default class Progress extends React.Component {
  render() {
    return (
      <div className={'progress ' + (this.props.className || '')}>
        <div style={{width: this.props.percent + '%', background: this.props.color}} className='progress__fill'></div>
      </div>
    );
  }
}
