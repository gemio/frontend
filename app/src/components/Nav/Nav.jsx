import React from 'react';
import NavItem from '../NavItem';
import {Icon} from 'react-fa';
import './Nav.scss';

class Nav extends React.Component {
  state = {
    collapsed: true
  };

  toggleMenu = () => {
    this.setState({collapsed: !this.state.collapsed});
  };

  render() {
    const collapsedClass = this.state.collapsed ? '' : ` ${this.props.className}--active`;
    return (
      <ul className={this.props.className + collapsedClass}>
        {this.props.children.map((menuItem, index) => {
          if (menuItem.type.name === 'NavItem')
           return <NavItem key={index} className={menuItem.props.className} onClick={this.toggleMenu} isActive={menuItem.props.isActive} isInline={menuItem.props.isInline} link={menuItem.props.link} text={menuItem.props.text} />
          else
            return menuItem;
        })}
        <li className="header__nav-burger d-md-none" onClick={this.toggleMenu}>
          <Icon name="bars"/>
        </li>
      </ul>
    );
  }
}

export default Nav;

