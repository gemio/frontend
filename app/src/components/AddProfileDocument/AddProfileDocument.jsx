import React from 'react';
import { connect } from 'react-redux';
import ProfileDocument from '../ProfileDocument';
import FileUploadButton from '../FileUploadButton';
import './AddProfileDocument.scss';

class AddProfileDocument extends React.Component {
  render() {
    const uploadDocument = (e) => {
      let data = new FormData();
      data.append('document', e.target.files[0]);
      e.target.value = '';
      fetch(`${process.env.API_URL}/user/${this.props.user.info._id}/document`, {
        method: 'POST',
        body: data
      }).then(response => {
        response.json().then(data => {
          if (this.props.addDocumentHandler) this.props.addDocumentHandler(data);
          else window.location.reload();
        });
      }).catch(err => {console.log(err)});
    };

    return (
      <ProfileDocument className="profile-edit__documents-add ">
        <form onSubmit={(e) => e.preventDefault()} encType="multipart/form-data">
          <FileUploadButton fileName="document" uploadHandler={uploadDocument} className="profile-edit__upload" buttonClass="button--primary" text="Добавить"/>
        </form>
      </ProfileDocument>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({})
)(AddProfileDocument);