import React from 'react';
import './ReminderBigList.scss';
export default class ReminderBigList extends React.Component {
  render() {
    return (
      <ul className={'reminder-big__list ' + (this.props.className || '')}>
        {this.props.children}
      </ul>
    );
  }
}
