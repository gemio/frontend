import React from 'react';
import IndexCommunicationCard from '../IndexCommunicationCard';
import image from '../../img/shapes/shape-egg.svg';
import "./IndexCommunication.scss";

export default class IndexCommunication extends React.Component {
  render() {
     return (
      <div className="Communication">
        <div className="Communication__block__Above">
          <div className="grid-item">
            <h1 className="Communication__block__Above__header">Общение и обмен знаниями</h1>
            <p className="Communication__block__Above__text">
              площадка для обмена информацией, взаимопомощи и проведения консультаций между больными, родителями и врачами по всей России
            </p>
          </div>
          <IndexCommunicationCard className="Communication__block__Card__right__block "
                                  nav="/jobs"
                                  text="Поиск работы"
                                  name="На сегодняшний день трудоустройство для больных с гемофилией является важным шагом в жизни. В это период важно найти не просто место работы, а вакансию, где будет комфортно."
                                  tag=" "
                                  log="Подробнее"/>
          <IndexCommunicationCard className="Communication__block__Card__bottom__block "
                                  nav="/registration"
                                  text="Дневник и план лечения" name="Фиксируйте изменения в течение болезни, обострения, процедуры, вносите данные результатов анализов, а также другие события."
                                  tag="При домашнем лечении очень важно постоянно вести дневник переливаний."
                                  log="Зарегистрироваться"/>
          <IndexCommunicationCard className="Communication__block__Card__right__bottom__block "
                                  nav="/wiki"
                                  text="База знаний"
                                  name="Единый достоверный источник информации. С возможностью задать и получить ответ на вопрос от специалиста."
                                  tag="Как получить лекарства? Какие необходимы документы? и другие важные вопросы в единой базе заний."
                                  log="Подробнее"/>
        </div>
        <div className="Communication__block">
          <div className="Communication__block__image">
            <img alt="gem-egg" className="Communication__block__image__image" src={image} />
          </div>
        </div>
      </div>
    );
  }
}
