import React from 'react';

import Row from '../Row';
import Col from '../Col';
import Card from '../Card';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import ReminderBigList from '../ReminderBigList';
import ReminderBig from '../ReminderBig';
import AddReminder from '../AddReminder';
import CalendarIO from '../CalendarIO/Calendar';
import Reminder from '../Reminder';
import '../../Styles/Treatment.scss';
import {connect} from "react-redux";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Treatment extends React.Component {
  state = {
    reminds: [],
    remindis: [],
    remindsLoaded: false,
    remindisLoaded: false,
    date: new Date(),
  };
  
  loadRemindOnCurrentDate = date => {

    let dates = {
      from: date ? date.from ? date.from : date[0] : new Date(),
      to: date ? date.to ? date.to : date[1] : new Date()
    };

    let paramDate = `${dates.from.normalizeDateString(dates.from)}IO${dates.to.normalizeDateString(dates.to)}`;
    fetch(`${process.env.API_URL}/remind/${this.props.user.info._id}?range=${paramDate}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      
      if (response.status === 500 || response.status === 404) {
        this.setState({remindisLoaded: true});
        return;
      }

      if (response.status === 412){
        this.setState({remindis:[] ,remindisLoaded: true});
        return
      }

      response.json().then(data => {

        if (response.status === 204|| response.status === 412 )
          this.setState({remindis:[],remindisLoaded: true});
        else
          this.setState({remindis: data.reminds, remindisLoaded: true});
       
      }).catch(err => {});
    }).catch(err => {
      throw new Error(err);
    });
  };

  loadAllReminds() {
    fetch(`${process.env.API_URL}/remind/${this.props.user.info._id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({reminds: '', remindsLoaded: true});
        return;
      }

      if (response.status === 412){
        this.setState({remindsLoaded: true});
        return;
      }

      response.json().then(data => {
        this.setState({reminds: data.reminds, remindsLoaded: true});
      });
    }).catch(err => {
      throw new Error(err);
    });
  };

  onChange = date => {
    let dates = {
      from: date[0],
      to: date[1]
    };
    this.loadRemindOnCurrentDate(dates);
  };

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();
    this.loadRemindOnCurrentDate();
    this.loadAllReminds();
  }

  render() {
    return (
      <div style={{height: '100%', paddingTop: '25px'}}>
        <Row style={{height: '100%'}}>
         <Col lg="8" md={"12"}>
            <div className={"d-block d-lg-none"}> <CalendarIO value={this.state.date} id="cal" locale="ru-RU" onChange={this.onChange} selectRange={true} className={"Mobile__CalendarIO"} /> </div>
            <ReminderBigList className="reminds-big">
              { !this.state.remindisLoaded ? (<CardEmptyTitle>Загрузка...</CardEmptyTitle>) : this.state.remindsLoaded && this.state.remindis.length > 0 ?
                this.state.remindis.map(item => ((<ReminderBig key={item.id} time={item.time} date={item.date} title={item.text} type={item.type} />))) :
                (<CardEmptyTitle>Нет напоминаний</CardEmptyTitle>)
              }
            </ReminderBigList>
            <Card cardShadow='no-shadow' className="add-remind__card">
              <AddReminder addHandler={() => {this.loadRemindOnCurrentDate(); this.loadAllReminds();}}/>
            </Card>
           <div className={"d-block d-lg-none"}>
             <h2 className={"Mobile__header__remind"}>Ваши напоминания</h2>
               <div className="reminds-mobile">
                 { !this.state.remindsLoaded ? (<CardEmptyTitle>Загрузка...</CardEmptyTitle>) : this.state.remindsLoaded && this.state.reminds.length > 0 ?
                   this.state.reminds.map((item, index) => ((item.isPlan === true? <Reminder key={index} date={""} type={item.type} text={item.title} id={item.id} className={"Mobile__Reminder"} /> :<Reminder key={index} date={item.date} type={item.type} text={item.text} id={item.id} className={"Mobile__Reminder"} />))) :
                   (<CardEmptyTitle>Нет напоминаний</CardEmptyTitle>)
                 }
               </div>
           </div>
          </Col>

          <Col lg="4"  className="Treatment__Right">
            <CalendarIO value={this.state.date} id="cal" locale="ru-RU" onChange={this.onChange} selectRange={true} className={" d-xs-block d-lg-block d-md-none d-sm-none d-none"}/>
            <div className="reminds-small d-xs-block d-lg-block d-md-none d-sm-none d-none">
              { !this.state.remindsLoaded ? (<CardEmptyTitle>Загрузка...</CardEmptyTitle>) : this.state.remindsLoaded && this.state.reminds.length > 0 ?
                this.state.reminds.map((item, index) => (( item.isPlan === true? <Reminder key={index} date={item.date} type={item.type} text={item.title} id={item.id} className={"Remind__Consult"} /> : <Reminder key={index} date={item.date} type={item.type} text={item.text} id={item.id} /> ))) :
                  (<CardEmptyTitle>Нет напоминаний</CardEmptyTitle>)
              }
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(Treatment);
