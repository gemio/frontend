//Model
import React, {Component} from 'react';
import {connect} from "react-redux"
import  '../../Styles/NewsPage.scss';
import NewsGlobal from  '../NewsGlobal';
import Card from  '../Card';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import Button from "../Button/Button";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class NewsPage extends Component {
  state = {
    Loaded: false,
    News: [],
    offset: 0,
    isAdmin: false
  };

  updateNews = () => {
    let userUrl = this.props.user && this.props.user.info && this.props.user.info._id ? `&userID=${this.props.user.info.id}` : '';
    let url = `${process.env.API_URL}/news/preview?count=8&offset=${this.state.offset}${userUrl}`;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      response.json().then((data) => {
        let months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Ноября', 'Декабря'];
        let news = data.data.map(msg => {
          return {
            day: new Date(msg.date).getDate(),
            date: String(months[new Date(msg.date).getMonth()])+" "+String(new Date(msg.date).getFullYear()),
            title: msg.title,
            text: msg.text,
            link : msg.link,
            image: msg.img,
          };
        });

        this.setState({News: this.state.News.concat(news), Loaded: true, offset: this.state.offset + news.length, isLast: news.length < 8});
      });
    });
  };

  Security = () => {
    fetch(`${process.env.API_URL}/security/${this.props.user.info._id}/admin`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      this.setState({isAdmin: response.status === 200});
    }).catch(err => {});
  };

  componentWillMount() {
    this.updateNews();
    this.Security();
  }

  render() {
    let creatorNews;
    if (this.state.isAdmin)
      creatorNews = (<Button onClick={() => {window.location.href = '/news-create'}} className=" button--primary-ghost NewsPage__create" text="Создать новость"/>);

    return (
      <div className="container NewsPage">
        <div className="NewsPage__block">
          {creatorNews}

          { !this.state.Loaded ? (<CardEmptyTitle>Загрузка...</CardEmptyTitle>) : this.state.Loaded && this.state.News.length > 0 ?
            this.state.News.reverse().map(item => ((
              <div  key={item.link} className="NewsPage__block__item"><NewsGlobal className='index-money' date={item.day+" "+ item.date} img={item.image}  header={item.title} text={item.text} link={item.link} /></div>
            ))) :
            (<CardEmptyTitle>Нет новостей</CardEmptyTitle>)
          }
        </div>

        {!this.state.isLast ? <div onClick={this.updateNews} className="NewsPage__more">Показать еще</div> : null}
      </div>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(NewsPage);