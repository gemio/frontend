import React from 'react';
import {connect} from "react-redux";
import Card from '../Card';
import InputSimple from '../InputSimple';
import AdditionalLanguage from '../SummaryPage/AdditionalLanguage';
import Education from '../SummaryPage/Education';
import DropdownSimple from '../DropdownSimple';
import LocationSearch from  '../LocationSearchInput';
import Button from  '../Button/index';
import ButtonLink from '../ButtonLink';
import Radio from '../RadioButton/Radio';
import "../../Styles/summary.scss";
import APIMessage from "../APIMessage/APIMessage";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class summary extends React.Component {
  state = {
    gender: 'Мужской',
    country: 'Russian Federation',
    region: '',
    languages: [
      'азербайджанский',
      'английский',
      'арабский',
      'армянский',
      'баскский',
      'белорусский',
      'болгарский',
      'голландский',
      'грузинский',
      'датский',
      'доломитский',
      'испанский',
      'итальянский',
      'казахский',
      'керекский',
      'киргизский',
      'китайский',
      'кумыкский',
      'латинский',
      'литовский',
      'монгольский',
      'мордовские',
      'немецкий',
      'польский',
      'португальский',
      'румынский',
      'русский',
      'сербский',
      'словацкий',
      'словенский',
      'татарский',
      'турецкий',
      'удмуртский',
      'украинский',
      'финский',
      'французский',
      'хакасский',
      'хорватский',
      'чероки',
      'чеченский',
      'чешский',
      'чукотский',
      'чувашский',
      'шведский',
      'швейцарско-ретороманский',
      'шумерский',
      'эстонский',
      'якутский',
      'японский',
    ],
    cities: [
      'Австралия',
      'Австрия',
      'Азербайджан',
      'Албания',
      'Алжир',
      'Аргентина',
      'Армения',
      'Афганистан',
      'Бангладеш',
      'Барбадос',
      'Бахрейн',
      'Беларусь',
      'Бельгия',
      'Боливия',
      'Бразилия',
      'Ватикан',
      'Великобритания',
      'Венгрия',
      'Венесуэла',
      'Вьетнам',
      'Гамбия',
      'Гвинея',
      'Германия',
      'Гондурас',
      'Гонконг',
      'Греция',
      'Грузия',
      'Дания',
      'Доминика',
      'Египет',
      'Израиль',
      'Индия',
      'Индонезия',
      'Ирландия',
      'Испания',
      'Италия',
      'Кабо-Верде',
      'Казахстан',
      'Китай',
      'Колумбия',
      'Коморы',
      'Кыргызстан',
      'Латвия',
      'Литва',
      'Люксембург',
      'Молдова',
      'Нидерланды',
      'Норвегия',
      'Объединенные Арабские Эмираты',
      'Польша',
      'Португалия',
      'Россия',
      'Румыния',
      'США',
      'Северная Корея',
      'Словакия',
      'Таджикистан',
      'Туркменистан',
      'Узбекистан',
      'Украина',
      'Филиппины',
      'Финляндия',
      'Франция',
      'Центральноафриканская Республика',
      'Черногория',
      'Чехия',
      'Чили',
      'Швейцария',
      'Швеция',
      'Эстония',
      'Южная Корея',
      'Ямайка',
      'Япония'
    ],
    countLanguage: 1,
    countEducation: 1,
    isSummaryAddedSuccess: '',
    messageSummaryApi: ''
  };

  addEducation = () => {
    this.setState({countEducation: this.state.countEducation + 1 > 5 ? 5 : this.state.countEducation + 1});
  };

  removeEducation = () => {
    this.setState({countEducation: this.state.countEducation - 1 < 0 ? 0 : this.state.countEducation - 1});
  };

  addLanguage = () => {
    this.setState({countLanguage: this.state.countLanguage + 1 > 5 ? 5 : this.state.countLanguage + 1});
  };

  removeLanguage = () => {
    this.setState({countLanguage: this.state.countLanguage - 1 < 0 ? 0 : this.state.countLanguage - 1});
  };

  sendSummary = () => {
    const getEducation = () => {
      const educations = document.getElementsByClassName('summary__education-item');
      const names = document.querySelectorAll('.summary__education-subject input');
      const levels = document.querySelectorAll('.summary__education-level select');
      const specializations = document.querySelectorAll('.summary__education-specialist input');
      const years = document.querySelectorAll('.summary__education-year input');
      let obj = [];
      for (let i = 0; i < educations.length; i++) {
        obj.push({
          name: names[i].value,
          level: levels[i].value,
          specialization: specializations[i].value,
          yearOfEnd: years[i].value
        });
      }

      return obj;
    };

    const getLanguage = () => {
      let obj = [
        {
          isPrimary: true,
          name: document.getElementsByClassName('language')[0].value
        }
      ];

      const items = document.getElementsByClassName('summary__language-item');
      const ln = document.querySelectorAll('.summary__language-item select');
      for (let i = 1; i < items.length; i++) {
        obj.push({
          name: ln[i].value,
          level: ln[i + 1].value
        });
      }

      return obj;
    };

    const summary = {
      gender: this.state.gender,
      birth: new Date().toISOString(),
      phone: document.querySelectorAll('.summary__phone input')[0].value,
      city: document.getElementsByClassName('location-search-input')[0].value,
      nationality: document.getElementsByClassName('nationality')[0].value,
      education: getEducation(),
      language: getLanguage()
    };

    fetch(`${process.env.API_URL}/job/${this.props.user.info._id}/summary`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `summaryUser=${JSON.stringify(summary)}`
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 200) {
        window.location.href = `/summary-profile/${this.props.user.info.id}`;
        return;
      }

      response.json().then(data => {
        this.setState({isSummaryAddedSuccess: response.status === 200, messageSummaryApi: data.message.message});
      }).catch(err => {
        console.log(err);
      })
    });
  };

  render() {
    const genderChange = (e) => {this.setState({gender: e.target.value});};
    const phone = this.props.user.authorized && this.props.user.info && this.props.user.info.phone !== "7 (   )    -  -" ? this.props.user.info.phone : ''

    let languageItems = [];
    for (let i = 0; i < this.state.countLanguage; i++) {
      languageItems.push((
        <AdditionalLanguage deleteHandler={this.removeLanguage} key={i}/>
      ));
    }

    let educationItems = [];
    for (let i = 0; i < this.state.countEducation; i++) {
      educationItems.push((
        <Education deleteHandler={this.removeEducation} key={i}/>
      ));
    }

    return (
      <Card cardShadow='no-shadow' className='summary'>
        <h3 className='summary__title'>Создание резюме</h3>

        <InputSimple labelText='Телефон: ' className='summary__phone' value={phone} alwaysShowMask={true} isMask={true} mask='+7 (999) 999-99-99'/>

        <div className="summary__gender">
          <span>Ваш пол: </span>
          <Radio name='gender' value='Мужской' checked={this.state.gender === 'Мужской'} handleChange={genderChange}/>
          <Radio name='gender' value='Женский' checked={this.state.gender === 'Женский'} handleChange={genderChange}/>
        </div>

        <div className="summary__location">
          <label>Город проживания: </label>
          <LocationSearch />
        </div>

        <div className="summary__nationality">
          <label>Гражданство: </label>
          <DropdownSimple className='nationality' selectedItem='Россия' items={this.state.cities}/>
          <ButtonLink className='summary__nationality-add' text='Добавить гражданство'/>
        </div>

        <div className="summary__education">
          <h3 className='summary__title'>Образование</h3>
          {educationItems}
          <ButtonLink style={{display: this.state.countEducation === 5 ? 'none' : 'block'}} onClick={this.addEducation} className='summary__education-add' text='Добавить'/>
        </div>

        <div className="summary__language">
          <h3 className='summary__title'>Владение языками</h3>

          <div className="summary__language-item">
            <label>Родной язык: </label>
            <DropdownSimple className='language' selectedItem='русский' items={this.state.languages}/>
          </div>

          {languageItems}
          <ButtonLink style={{display: this.state.countLanguage === 5 ? 'none' : 'block'}} onClick={this.addLanguage} className='summary__language-add' text='Добавить'/>
        </div>

        <APIMessage status={this.state.isSummaryAddedSuccess ? 'success' : 'error'} text={this.state.messageSummaryApi}/>
        <Button onClick={this.sendSummary} className='button--primary summary__save' text='Сохранить'/>
      </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(summary);
