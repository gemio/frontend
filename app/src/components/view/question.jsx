import React, {Component} from 'react';
import {connect} from 'react-redux';
import Card from '../Card';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import SpecialistItem from '../Consultation/SpecialistItem';
import QuestionItem from '../Consultation/QuestionItem';
import Button from '../Button';
import '../../Styles/question.scss';
import {Icon} from 'react-fa';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class QuestionPage extends Component {
  state = {
    specialists: [],
    questions: [],
    userQuestions: [],
    isAPILoaded: false,
    isQuestions: false,
    isQuery: false,
  };

  componentDidMount() {
    if (window.location.search !== '') {
      this.setState({isQuery: true});
      let query = require('../../lib/query-string').parse(window.location.search);
      if (query.theme && query.theme !== 'all') {
        this._getQuestionByThemeId(query.theme);
      }
      else this._getSpecialists();
    } else {
      this._getSpecialists();
      this._getUserQuestions();
    }
  }

  _getQuestionByThemeId = (themeId) => {
    fetch(`${process.env.API_URL}/specialist/questions/theme?themeId=${themeId}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 204) {
        this.setState({questions: [], isAPILoaded: true, isQuestions: true});
        return;
      }

      response.json().then((data) => {
        this.setState({questions: response.status === 200 ? data.data : data.message.message, isAPILoaded: true, isQuestions: true});
      });
    }).catch((response) => {});
  };

  _getSpecialists = () => {
    fetch(`${process.env.API_URL}/specialists`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 204) {
        this.setState({specialists: [], isAPILoaded: true});
        return;
      }

      response.json().then((data) => {
        this.setState({specialists: response.status === 200 ? data.data.specialists : data.message.message, isAPILoaded: true});
      });
    }).catch((response) => {});
  };

  _getUserQuestions = () => {
    fetch(`${process.env.API_URL}/specialist/questions/user/${this.props.user.info._id}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status !== 204) {
        response.json().then(data => {
          this.setState({isAPILoaded: true, userQuestions: response.status === 200 ? data.data : data.message.message})
        })
      } else this.setState({isAPILoaded: true, userQuestions: []})
    }).catch((response) => {});
  };

  render() {
    const getLinkToAddQuestion = () => {
      if (window.location.search !== '') {
        let query = require('../../lib/query-string').parse(window.location.search);
        if (query.specialist) return query.specialist;
        else return 'Врач';
      }
    };

    let specialists;
    if (!this.state.isAPILoaded) specialists = (<CardEmptyTitle>Загрузка...</CardEmptyTitle>);
    else if (typeof this.state.specialists !== 'string' && this.state.specialists.length > 0) {
      specialists = this.state.specialists.map(specialist => {
        return (<SpecialistItem key={specialist} specialist={specialist}/>);
      });
    } else if (this.state.specialists.length === 0) specialists = (<CardEmptyTitle>Нет специалистов</CardEmptyTitle>);

    let questions;
    if (typeof this.state.questions !== 'string' && this.state.questions.length > 0) {
      questions = this.state.questions.map((question, index) => {
        return (<QuestionItem key={index} question={question}/>);
      })
    } else if (this.state.questions.length === 0) questions = (<CardEmptyTitle>Нет вопросов на выбранную тему</CardEmptyTitle>);

    let userQuestions;
    if (typeof this.state.userQuestions !== 'string' && this.state.userQuestions.length > 0) {
      userQuestions = this.state.userQuestions.map((question, index) => {
        return (<QuestionItem key={index} question={question}/>);
      })
    } else if (this.state.userQuestions.length === 0) userQuestions = (<CardEmptyTitle>Нет открытых вопросов</CardEmptyTitle>);

    return (
      <Card cardShadow='no-shadow' className='questions'>
        <h3 className="questions__title">Консультации со специалистом</h3>

        {this.state.isQuery ? null :
          <div className="questions__user-question">
            <h4 className='questions__subtitle'>Ваши вопросы</h4>
            {userQuestions}
          </div>
        }

        <div className={"specialists " + (this.state.isQuery ? 'specialist--query' : '')}>
          {this.state.isAPILoaded ? this.state.isQuestions ?
            <div>
              <div className='questions__back' onClick={() => { window.location.href = `/questions`}}>
                <Icon name='arrow-left'/> Перейти к списку тем
              </div>
              {questions}
            </div> : specialists : null}
        </div>

        <Button onClick={() => { window.location.href = `/question-add${this.state.isQuestions ? `?specialist=${getLinkToAddQuestion()}` : ''}` }} className='button--gray-ghost questions__add' text='Задать вопрос'/>
      </Card>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },
  })
)(QuestionPage);
