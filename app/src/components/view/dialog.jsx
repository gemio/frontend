//Model
import React, { Component } from "react";
import { connect } from 'react-redux';
import DialogMessage from '../DialogMessage';
import APIMessage from '../APIMessage';
import Card from "../Card";
import CardEmptyTitle from '../Card/CardEmptyTitle';
import DialogHeader from '../DialogHeader';
import Button from '../Button';
import InputSimple from '../InputSimple';
import moment from 'moment';
import "../../Styles/dialog.scss";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class message extends Component {
  constructor() {
    super(...arguments);
    let url = window.location.href.split('/');
    this.state = {
      isMessageLoaded: false,
      companionID: url[url.length - 1],
      messages: [],
      companionFullName: '',
      companionAvatar: '',
      privacyLock: false,
      errorMessage: ''
    };

    this.updateMessage = (msg) => {
      let date = new Date(msg.date);
      this.setState({messages: this.state.messages.concat([{
        text: msg.text,
        dateFromNow: moment(new Date().fromNow(date)).fromNow(),
        date: date,
        status: msg.status,
        fullName: msg.fullNameFrom
      }])});
    };
  }

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }

    this.props.onLoad();
    if (!this.state.isMessageLoaded) {
      fetch(`${process.env.API_URL}/message/from/${this.props.user.info._id}/to/${this.state.companionID}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        response.json().then((data) => {
          let messages = [];
          if (response.status !== 412) {
            messages = data.data.messages.map(msg => {
              let diff = moment(new Date()).diff(moment(new Date(msg.date)), 'days');
              let date = moment(new Date(msg.date)).format('DD.MM.YYYY');
              return {
                text: msg.text,
                dateFromNow: diff < 2 ? moment(new Date().fromNow(new Date(msg.date))).fromNow() : date,
                date: msg.date,
                status: msg.status,
                fullName: msg.userFullName,
                avatar: msg.avatar
              };
            });
          }
          this.setState({
            isMessageLoaded: true,
            messages: messages,
            errorText: data.message.message,
            companionFullName: data.data.userFullName,
            companionAvatar: data.data.avatar
          });
        });
      }).catch((response) => {});
    }
  }

  componentDidMount() {
    window.socket.on('new message', this.updateMessage);
  }

  componentWillUnmount() {
    window.socket.removeListener('new message', this.updateMessage);
  }

  componentDidUpdate() {
    let card = document.getElementsByClassName("dialog")[0];
    card.scrollTop = card.scrollHeight;
  }

  render() {
    const sendMessage = () => {
      let user = this.props.user.info;
      let message = {
        fromUser: user._id,
        toUser: this.state.companionID,
        textMessage: document.getElementById('sendMessageText').value.trim()
      };

      if (message.textMessage === '') return;

      fetch(`${process.env.API_URL}/message`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `fromUser=${message.fromUser}&toUser=${message.toUser}&textMessage=${message.textMessage}`
      }).then(response => {
        if (response.status === 412) {
          response.json().then(data => {
            this.setState({privacyLock: true, errorMessage: data.message.message});
          });

          return;
        }

        let date = new Date();
        this.setState({messages: this.state.messages.concat([{
            text: message.textMessage,
            dateFromNow: moment(new Date().fromNow(date)).fromNow(),
            date: date,
            status: 'pending',
            avatar: this.props.user.info.avatar,
            fullName: `${user.public.fullName.surname} ${user.public.fullName.name}`

        }])});
        document.getElementById('sendMessageText').value="";
      });
    };


    let messages;
    if (!this.state.isMessageLoaded)
      messages = (<CardEmptyTitle>Загрузка...</CardEmptyTitle>);
    else if (this.state.isMessageLoaded && this.state.messages.length > 0) {
      messages = this.state.messages.map((message, index) => {
        if (index !== 0 && this.state.messages[index - 1].fullName === message.fullName) {
          if (new Date().diffMinutes(new Date(message.date), new Date(this.state.messages[index - 1].date)) < 1)
            return (<div key={index} className="dialog-message__text--same-user">{message.text}</div>);
        }
        return (<DialogMessage key={index} image={message.avatar} name={message.fullName} dateAgo={message.dateFromNow} text={message.text} />);
      });
    } else
      messages = (<CardEmptyTitle>Нет сообщений</CardEmptyTitle>);

    return (
      <div style={{height: '100%'}}>
        <Card cardShadow='no-shadow' className="dialog">
          <DialogHeader avatar={this.state.companionAvatar} fullName={this.state.companionFullName}/>
          {messages}
        </Card>

        <APIMessage className='dialog__error' status='error' text={this.state.errorMessage}/>

        <div className='dialog__footer'>
          <InputSimple id="sendMessageText" isMultiline={true} rows="1" cols="50"/>
          <Button onClick={sendMessage} className="button--primary dialog__send" text="Отправить"/>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(message);
