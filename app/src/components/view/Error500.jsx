//Model
import React, { Component } from "react";
import { connect } from 'react-redux';
import "../../Styles/Error500.scss";

class Error500 extends Component {
  constructor() {
    super(...arguments);

    this.state = {
      link__parent:''
    };
  }

 
  render() {
       return (
      <div className="Error500">
        <h1 className="Error500__title">Error 500</h1>
        <hr/>
        <div className="Error500__text">
          Sorry server is off
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  })
)(Error500);
