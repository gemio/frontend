import React, { Component } from "react";
import { connect } from 'react-redux';
import Row from "../Row";
import Col from "../Col";
import Shapes from "../Shapes";
import Card from '../Card';
import ForgotPasswordForm from "../ForgotPasswordForm";
import "../../Styles/ForgotPassword.scss";

const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class ForgotPassword extends Component {
  componentWillMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onClose();
  }

  render() {
    return (
      <div style={{height: '100%'}}>
        <Row style={{height: 'calc(100% - 55px)', overflow: 'hidden'}}>
          <Shapes/>
          <Col xs="1" md="3"/>
          <Col style={{height: '100%'}} className="forgot__column" xs="10" md="6">
            <Card className="forgot__card">
              <Row style={{height: '100%', width:'100%', marginLeft:'0px'}}>
                <Col xs="12">
                  <h4 className="forgot__title">Восстановление пароля</h4>
                  <ForgotPasswordForm />
                </Col>
              </Row>
            </Card>
          </Col>
          <Col xs="1" md="3"/>
        </Row>
      </div>
     );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(ForgotPassword);
