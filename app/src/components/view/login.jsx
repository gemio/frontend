import React from 'react';
import {connect} from "react-redux";
import Shapes from '../Shapes';
import Card from '../Card';
import Col from '../Col';
import Row from '../Row';
import {NavLink} from 'react-router-dom';

import '../../Styles/login.scss';
import LoginForm from '../LoginForm';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Login extends React.Component {
  componentWillMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onClose();
  }

  render() {
    return (
      <div style={{height: '100%'}}>
        <Row style={{height: '100%', overflow: 'hidden'}}>
          <Shapes/>
          <Col xs="1" md="3"/>
          <Col className="login__column" xs="10" md="6">
            <Card className="login__card">
              <Row style={{height: '100%', width:'100%', marginLeft:'0px'}}>
                <Col className="login__left" xs="12" lg="7">
                  <h4 className="login__title">Вход</h4>

                  <LoginForm />

                  <div style={{color: '#9da1a7', fontSize: '14px', marginTop: '20px'}}>Еще нет аккаунта?</div>
                  <NavLink className="login__register" to={`${process.env.URL_FOLDER}registration`}>ЗАРЕГИСТРИРУЙТЕСЬ</NavLink>
                </Col>

                <Col className="login__right d-none d-lg-flex" lg="5">
                  <div className="login__involvement">
                    Среда для общения, взаимопомощи
                    между больными, родителями и врачами
                    по всей России
                  </div>
                </Col>

              </Row>
            </Card>
          </Col>
          <Col xs="1" md="3"/>
        </Row>
      </div>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(Login);
