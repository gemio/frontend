//Model
import React, { Component } from "react";
import { connect } from 'react-redux';
import VacancyId from '../VacancyId';
import "../../Styles/Vacancy.scss";

const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Vacancy extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      Vacancy: [],
      Loaded:false
    };
  }
  componentWillMount() {
    fetch(`${process.env.API_URL}/job?id=${this.props.match.params.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({Vacancy: '', Loaded: false});
        return;
      }

      if (response.status === 412){
        this.setState({Loaded: false});
        return;
      }

      response.json().then(data => {
        this.setState({Vacancy: data.data, Loaded:true});
      });
    }).catch(err => {
      throw new Error(err);
    });
  }
  render() {
    return (
      <div className="Vacancy">
        <VacancyId header={this.state.Loaded ? this.state.Vacancy[0].title : ''}
                   price={this.state.Loaded ? this.state.Vacancy[0].price : ''}
                   person={this.state.Loaded ? this.state.Vacancy[0].company : ''}
                   text={this.state.Loaded ? this.state.Vacancy[0].text : ''}
                   city={this.state.Loaded ? this.state.Vacancy[0].city : ''}
                   adr={this.state.Loaded ? this.state.Vacancy[0].address : ''}
                   exp={this.state.Loaded ? this.state.Vacancy[0].exp : ''}
                   phone={this.state.Loaded ? this.state.Vacancy[0].contacts : ''}/>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.hideMenu());
    }
  })
)(Vacancy);