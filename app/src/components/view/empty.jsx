import React from 'react';
import {connect} from "react-redux";
import SlideMenuAction from '../../actions/SlidemenuAction';

class EmptyPage extends React.Component {
  componentDidMount() {
    if (window.location.href.includes('wiki') ||
        window.location.href.includes('qa')   ||
        window.location.href.includes('news') ||
        window.location.href.includes('jobs') ||
        window.location.href.includes('donate')
    )
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onClose();
  }

  render() {
    return (
      <div>
        <h3>Страница в разработке</h3>
      </div>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.unhideMenu());
    }
  })
)(EmptyPage);
