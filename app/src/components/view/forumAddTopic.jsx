import React, {Component} from 'react';
import {connect} from 'react-redux';
import Card from '../Card';
import CardTitle from '../Card/CardEmptyTitle';
import Button from '../Button';
import InputSimple from '../InputSimple';
import APIMessage from '../APIMessage';
import '../../Styles/forumAddTopic.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class ForumAddTopic extends Component {
  state = {
    themeDetails: [],
    isAPILoaded: false,
    addMessageApi: ''
  };

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();
  }

  componentDidMount() {
    this._getThemeIdDetails();
  }

  _getThemeIdDetails = () => {
    fetch(`${process.env.API_URL}/forum/theme-details?themeId=${this.props.match.params.themeId}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status !== 204) {
        response.json().then(data => {
          this.setState({isAPILoaded: true, themeDetails: response.status === 200 ? data.data : data.message.message});
        })
      } else window.location.href = '/forum';
    }).catch((response) => {});
  };

  createTopic = () => {
    const themeId = this.state.themeDetails.id, text = document.getElementById('text').value, title = document.getElementById('title').value;
    if (!text || !themeId || !title) {
      this.setState({addMessageApi: 'Пожалуйста, заполните все поля'});
      return;
    }

    fetch(`${process.env.API_URL}/forum/${this.props.user.info._id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `themeId=${themeId}&text=${text}&title=${title}`
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      response.json().then(data => {
        if (response.status === 200 && data.data.id) window.location.href = `/topic/${data.data.id}`;
        else {
          this.setState({addMessageApi: data.message.message}, () => {
            document.getElementById('text').value = text;
            document.getElementById('title').value = title;
          });
        }
      });
    });
  };

  render() {
    const { themeDetails } = this.state;

    return (
      <Card className='topic-add' cardShadow='no-shadow'>
        <CardTitle>Создание топика</CardTitle>

        <div className="topic-add__field">
          <label>Тема:</label>
          <strong>{themeDetails.themeName ? themeDetails.themeName : ''}</strong>
        </div>

        <InputSimple id='title' className="topic-add__field topic-add__field-topic" labelText='Заголовок:'/>
        <InputSimple id="text" className="topic-add__field topic-add__field-text" labelText='Текст:' cols="20" rows="5" isMultiline={true}/>

        <APIMessage status={this.state.addMessageApi ? 'error' : 'success'} text={this.state.addMessageApi}/>
        <Button onClick={this.createTopic} text='Создать' className='button--primary-small-round topic-add__submit'/>
      </Card>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },
  })
)(ForumAddTopic);
