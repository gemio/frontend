import React from 'react';
import {connect} from "react-redux";

import Col from '../Col';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import TabControl from '../TabControl';
import ReminderBigList from '../ReminderBigList';
import ReminderBig from '../ReminderBig';
import CalendarIO from '../CalendarIO/Calendar';
import Card from '../Card';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import Row from '../Row';
import Legend from '../Legend';
import BigCalendar from '../BigCalendar';
import '../../Styles/TreatmentPlan.scss';
import TreatmentSpecialistWork from  '../TretmentSpecialistWork';

import TreatmenComment from  '../TreatmenComment';
import TreatmentChooseUser from '../TreatmentChooseUser';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class TreatmentPlan extends React.Component {
  state = {
    reminds: [],
    remindsLoaded: false,
    date: new Date(),
    bigdate: [],
    isSpecialist: false
  };

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }

    this.props.onLoad();
    this.reloadTreatment();
    this._isSpecialist();
    this.loadRemindOnCurrentDate();
  }

  reloadTreatment = () => {
    fetch(`${process.env.API_URL}/remind/${this.props.user.info._id}?isPlan=true`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({reminds: '', remindsLoaded: true});
        return;
      }

      if (response.status === 412){
        this.setState({remindsLoaded: true});
        return;
      }

      response.json().then(data => {
        let events =[];
        if (data.data.length > 0) {
          events = data.data.map((item, index) => {
            return {
              key:index,
              id: index,
              type: item.type,
              startDate: new Date(item.startDate),
              endDate: new Date(item.endDate),
              title: item.title,
              desc: item.desc

            }
          });
        }

        this.setState({bigdate: events});
      });
    }).catch(err => {
      throw new Error(err);
    });
  };

  loadRemindOnCurrentDate = date => {
    const getdate = date => (new Date(date).getDate()-1)+" "+ new Date().getMonthNameByDay((new Date(date).getMonth()+1));
    fetch(`${process.env.API_URL}/remind/${this.props.user.info._id}?isPlan=true`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({remindsLoaded: true});
        return;
      }

      if (response.status === 412){
        this.setState({remindis:[] ,remindsLoaded: true});
        return;
      }

      response.json().then(data => {
        if (response.status === 204 || response.status === 412)
          this.setState({remindis: [], remindsLoaded: true});
        else {
          let ev = [];
          if (data.data.length > 0) {
            ev = data.data.map((item,index) => {
              return {
                id: index,
                type: item.type,
                sDate: getdate(item.startDate) + "  -  " + getdate(item.endDate) ,
                sTime: item.startDate.toString().split(':')[0].split('T')[1]+":"+ item.endDate.toString().split(':')[1],
                title: item.title,
                desc: item.desc
              }
            });
          }

          this.setState({remindis: ev, remindsLoaded: true});
        }
      }).catch(err => {});
    }).catch(err => {
      throw new Error(err);
    });
  };

  _isSpecialist = () => {
    fetch(`${process.env.API_URL}/security/${this.props.user.info._id}/doctor`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) return;

      this.setState({isSpecialist: response.status === 200});
    });
  };

  _handleUserChange = (userID) => {
    if (!userID) {
      this.reloadTreatment();
      return;
    }

    fetch(`${process.env.API_URL}/doctor/remind/${this.props.user.info._id}?userID=${userID}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({reminds: '', remindsLoaded: true});
        return;
      }

      if (response.status === 412){
        this.setState({remindsLoaded: true});
        return;
      }

      response.json().then(data => {
        let events =[];
        if (data.data.length > 0) {
          events = data.data.map((item, index) => {
            return {
              id: index,
              type: item.type,
              startDate: new Date(item.startDate),
              endDate: new Date(item.endDate),
              title: item.title,
              desc: item.desc
            }
          });
        }

        this.setState({bigdate: events});
      });
    }).catch(err => {
      throw new Error(err);
    });
  };

  onChange = date => {
    let dates = {
      from: date[0],
      to: date[1]
    };

    this.loadRemindOnCurrentDate(dates);
  };

  render() {

    return (
      <div style={{height: '100%'}} className="treatment-plan">
        <Card cardShadow='no-shadow' className="treatment-plan__card">
          <TabControl>
            <TabList>
              <Tab tabFor="one">Календарь</Tab>
              {!this.state.isSpecialist ?  <Tab tabFor="two">В виде списка</Tab>:""}
              <Tab tabFor="three"> {this.state.isSpecialist?"Пациенты":"Общение с врачом"}</Tab>
            </TabList>
            <TabPanel tabId="one">
              {this.state.isSpecialist ? <TreatmentChooseUser onAddSuccess={this._handleUserChange} userChange={this._handleUserChange}/> : null}
              <BigCalendar event={this.state.bigdate}/>
            </TabPanel>

            {!this.state.isSpecialist ? <TabPanel tabId="two">
              <Row style={{height: '100%'}}>
                <Col lg="8" md={"12"}>
                  <div className={"d-block d-lg-none d-xl-none Mobile__block__TreatmentPlan"}>
                    <CalendarIO value={this.state.date} id="cal" locale="ru-RU" onChange={this.onChange}
                                selectRange={true} className={"Mobile__CalendarIO__TreatmentPlan"}/>
                  </div>

                  <ReminderBigList className=" treatment-plan__card__ReminderBigList">
                    {!this.state.remindsLoaded ? (
                      <CardEmptyTitle>Нет напоминаний</CardEmptyTitle>) : this.state.remindsLoaded && this.state.remindis.length > 0 ?
                      this.state.remindis.map(item => ((
                        <ReminderBig key={item.id} time={item.sTime} date={item.sDate} title={item.title}
                                     type={item.type} className={"TreatmentPlan__Reminder"}/>))) :
                      (<CardEmptyTitle>Нет напоминаний</CardEmptyTitle>)
                    }
                  </ReminderBigList>
                </Col>

                <Col lg="4" className="treatment-plan__card__Right d-none d-lg-block d-xl-block">
                  <CalendarIO value={this.state.date} id="cal" locale="ru-RU" onChange={this.onChange}
                              selectRange={true}/>
                </Col>
              </Row>

              <Legend/>
            </TabPanel>:""
            }
            <TabPanel tabId="three">
              {this.state.isSpecialist ? <TreatmentSpecialistWork/>: <TreatmenComment/>}
            </TabPanel>
          </TabControl>
        </Card>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(TreatmentPlan);

