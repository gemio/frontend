import React from 'react';
import { connect } from 'react-redux';
import Card from '../Card';
import Col from '../Col';
import Row from '../Row';
import Shapes from '../Shapes';
import ResetPasswordForm from '../ResetPasswordForm'
import '../../Styles/resetpassword.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class resetpassword extends React.Component {
  componentWillMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onClose();
  }

  render() {
    return (
      <div style={{height: '100%'}}>
        <Row style={{height: 'calc(100% - 55px)', overflow: 'hidden'}}>
          <Shapes />
          <Col xs="1" md="3"/>
          <Col style={{height: '100%'}} className="reset-password__column" xs="10" md="6">
            <Card className="reset-password__card">
              <Row style={{height: '100%'}}>
                <Col className="reset-password__top" xs="12">
                  <h4 className="reset-password__title">Сброс пароля</h4>
                  <ResetPasswordForm/>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col xs="1" md="3"/>
        </Row>
      </div>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(resetpassword);
