import React, {Component} from 'react';
import {connect} from 'react-redux';
import '../../Styles/question-add.scss';
import Card from '../Card';
import InputSimple from '../InputSimple';
import DropdownSimple from '../DropdownSimple';
import APIMessage from '../APIMessage';
import Button from '../Button';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class QuestionAdd extends Component {
  state = {
    structure: {},
    isAPILoaded: false,
    specialist: undefined,
    theme: 'Общие вопросы',
    message: ''
  };

  createQuestion = () => {
    let { specialist, theme } = this.state;
    const text = document.getElementById('question').value;
    if (specialist === undefined) specialist = 'Врач';
    if (!specialist || !theme || !text) {
      this.setState({message: 'Пожалуйста, заполните все поля'}, () => {
        document.getElementById('question').value = text;
      });
      return;
    }

    fetch(`${process.env.API_URL}/specialist/question/${this.props.user.info._id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `specialist=${specialist}&theme=${theme}&text=${text}`
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      response.json().then(data => {
        if (response.status === 200 && data.data.id) window.location.href = `/question?id=${data.data.id}`;
        else this.setState({message: data.message.message});
      });
    });
  };

  componentWillMount() {
    const query = require('../../lib/query-string');
    let queryParsed = query.parse(window.location.search);
    this._getSpecialists(() => {
      // if specialist is in url
      if (queryParsed.specialist) {
          if (this.state.structure.some(spec => spec.specialist === queryParsed.specialist))
            this.setState({specialist: queryParsed.specialist})
      }
    });
  }

  _getSpecialists = (cb = undefined) => {
    fetch(`${process.env.API_URL}/specialist/structure`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      response.json().then(data => {
        let structure = data.data;
        structure[structure.findIndex(spec => spec.specialist === '/')].specialist = 'Общие темы';

        this.setState({isAPILoaded: true, structure: response.status === 200 ? structure : data.message.message}, () => {
          if (response.status === 200 && cb) cb();
        });
      });
    }).catch((response) => {});
  };

  specialistChanged = () => {
    let question = document.getElementById('question').value;
    this.setState({specialist: document.getElementsByClassName('question-add__specialists')[0].value}, () => {
      document.getElementById('question').value = question;
    });
  };

  themeChanged = () => {
    let question = document.getElementById('question').value;
    this.setState({theme: document.getElementsByClassName('question-add__theme')[0].value}, () => {
      console.log(this.state.theme);
      document.getElementById('question').value = question;
    })
  };

  render() {
    const { isAPILoaded, structure, specialist } = this.state;
    const indexOfSpecialist = specialist ? structure.findIndex(spec => spec.specialist === specialist) : 0;

    return (
      <Card cardShadow='no-shadow' className='question-add'>
        <h3 className="question-add__title">Создание вопроса</h3>
        <InputSimple className='question-add__text' id="question" labelText="Текст вопроса" cols="20" rows="5" isMultiline={true}/>

        <div className="question-add__field">
          <label>Специалист: </label>
          <DropdownSimple onChange={this.specialistChanged} className='question-add__specialists' items={isAPILoaded ? structure.map(spec => spec.specialist) : []} selectedItem={specialist ? specialist : structure.length > 0 ? structure[0].specialist : ''}/>
        </div>

        <div className="question-add__field">
          <label>Тема: </label>
          <DropdownSimple onChange={this.themeChanged} className='question-add__theme' items={isAPILoaded && structure.length > 0 ? structure[indexOfSpecialist].themes.map(theme => theme.text) : []}/>
        </div>

        <APIMessage status={!this.state.message ? 'success' : 'error'} text={this.state.message}/>
        <Button onClick={this.createQuestion} className='button--primary-small-round question-add__submit' text='Создать вопрос'/>
      </Card>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },
  })
)(QuestionAdd);
