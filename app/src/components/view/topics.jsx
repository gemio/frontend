import React from 'react';

import Card from  '../Card';
import TopicComment from '../TopicComment';
import {Icon} from 'react-fa';
import InputSimple from '../InputSimple';
import Button from '../Button';
import "../../Styles/topics.scss";
import {connect} from "react-redux";

class topics extends React.Component {
  state = {
    Loaded: false,
    topic: [],
    comments: [],
    indexActive: 0,
    countelements: 5,
    commentcount: 0,
    col: []
  };

  componentWillMount() {
    fetch(`${process.env.API_URL}/forum/topic?topicId=${this.props.match.params.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({topic: '', Loaded: true});
        return;
      }

      response.json().then(data => {
        this.setState({topic: data.data, Loaded: true});
      }).catch(err => {
        throw new Error(err);
      })
    }).catch(err => {
    });

    this.GetComments(this.state.indexActive);
  }

  onPageChange = (id) => {
    this.setState({indexActive: id - 1}, () => {
      this.GetComments(id);
    });
  };

   GetComments = (index) => {
     fetch(`${process.env.API_URL}/forum/comments-only?topicId=${this.props.match.params.id}&offset=${(this.state.indexActive * this.state.countelements)}&count=${this.state.countelements}`, {
       method: 'GET',
       headers: {
         'Content-Type': 'application/json'
       }
     }).then(response => {
       if (response.status === 500 || response.status === 404) {
         this.setState({comments:'', Loaded: true});
         return;
       }

       response.json().then(data => {
         let dat = [];
         for (let i = 0; i < data.data.count / this.state.countelements; i++)
           dat.push(i + 1);

         this.setState({col: dat, comments: data.data.comments, commentcount: data.data.count, Loaded: true});
       }).catch(err => {
         throw new Error(err);
       })
     }).catch(err => {
     });
   };

  NormalDate = (date) =>{
    let Month=['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
    let Norm =  new Date(date).getDate()+" "+Month[new Date(date).getMonth()+1]+" "+new Date(date).getFullYear()+", "+new Date(date).getHours()+":"+new Date(date).getMinutes()+":"+new  Date(date).getSeconds();
    return Norm;
  };

  render() {
    const sendMessage = () => {
      let user = this.props.user.info;
      let message = {
        fromUser: user._id,
        textMessage: document.getElementById('sendMessageComment').value.trim()
      };

      if (message.textMessage === '') return;

      fetch(`${process.env.API_URL}/forum/comment/${message.fromUser}?topicId=${this.state.topic._id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `comment=${message.textMessage}`
      }).then(response => {
        this.GetComments(this.state.indexActive);
      });

      document.getElementById('sendMessageComment').value = "";
    };

    return (

      <Card className={"topic"}>
        <div className={"topic__Cancel"}>
           <a href={`/forum/${this.state.topic.themeId}`} className={"topic__Cancel__Href"}>
             <Icon name={"angle-left"} className={"topic__Cancel__Href__Icon"}/>
             Назад
           </a>
        </div>
        <div className={"topic__header"}>
          <img src={this.state.topic.owner ?this.state.topic.owner.avatar:""} className={"topic__header__image"}/>
          <div className={"topic__header__block"}>
            <p className={"topic__header__block__header"}>{this.state.topic.title}</p>
            <p className={"topic__header__block__author"}>{  (this.state.topic.owner ? this.state.topic.owner.fullName : "")}</p>
          </div>
          <div className={"topic__header__EndBlock"}>
            {this.state.topic.themeName}
          </div>
        </div>
        <div className={"topic__content"}>
          <p className={"topic__content__text"}>{this.state.topic.text}</p>
        </div>
        <div className={"topic__comment"}>
          {
             this.state.Loaded && this.state.comments.length>0  ?
              this.state.comments.map((item,index) => (<TopicComment key={index} text={item.text} date={this.NormalDate(item.date)} href={item.user.userID} author={item.user.fullName} image={item.user.avatar} />))
              :<div/>
          }
        </div>
        <div className={"topic__ScrollComent"}>
          {
            this.state.commentcount>this.state.countelements?
              this.state.col.map((item,index) => (<span key={item} id={item} className={'topic__ScrollComent__item ' + (index === this.state.indexActive ? 'topic__ScrollComent__item--active' : '')} onClick={() => {this.onPageChange(item)}}>{item}</span>)):""
          }
        </div>
        <div className={"topic__AddComment"}>
          <InputSimple id="sendMessageComment" isMultiline={true} rows="1" cols="70"/>
          <Button onClick={sendMessage} className="button--primary dialog__send" text="Отправить"/>
        </div>
      </Card>
    )
  }
}

export default connect(
  state => ({
    user: state.user
  })
)(topics);