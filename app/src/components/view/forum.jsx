import React from 'react';
import {connect} from "react-redux";
import ForumItem from "../ForumItem/ForumItem";
import Card from "../Card";
import CardTitle from '../Card/CardEmptyTitle';
import '../../Styles/forum.scss';

const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class forum extends React.Component {
  state = {
    Loaded: false,
    structure: [],
    counts:[]
  };

  componentDidMount() {
    this.props.onLoad();
  }

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }

    fetch(`${process.env.API_URL}/forum/structure`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({structure: '', Loaded: true});
        return;
      }

      response.json().then(data => {
        this.setState({structure: data.data.structure,counts: data.data.counts, Loaded: true})
      }).catch(err => {
        throw new Error(err);
      })
    }).catch(err => {
    });
  }

  render() {
    return (
      <Card cardShadow='no-shadow' className="container Forum">
        <h2 className={"Forum__header"}>Форум</h2>
        <div className={"Forum__blog"}>
          { this.state.structure.length>0 ?this.state.structure.map((item,index) => ((
          <ForumItem key={item._id} text={item.root} element={item.subroot} count={this.state.counts[index]} /> )))
            :<div className={"Forum--none"}><CardTitle>Нет тем</CardTitle></div>
          }
        </div>
      </Card>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },
  })
)(forum);