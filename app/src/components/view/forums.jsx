import React from 'react';
import {connect} from "react-redux";
import ForumItem from "../ForumItem/PrevSubject";
import  Card from "../Card";
import Button from '../Button';
import '../../Styles/forum.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class forums extends React.Component {
  state = {
    Loaded: false,
    topicsmessage: [],
    Header:"",
    hrefs:[]
  };

  componentDidMount() {
    this.props.onLoad();
  }

  componentWillMount() {
    // Получение данных по id форума
    fetch(`${process.env.API_URL}/forum?themeId=${this.props.match.params.id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({topicsmessage: '', Loaded: true});
        return;
      }

      response.json().then(data => {
        this.setState({topicsmessage: data.data});

      }).catch(err => {
        throw new Error(err);
      })
    }).catch(err => {

    });

    // Полученние министруктуры
    fetch(`${process.env.API_URL}/forum/mini-structure?themeId=${this.props.match.params.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({topicsmessage: '', Loaded: true});
        return;
      }

      response.json().then(data => {
        let hrefs = [];
        for (let id of data.data) {
          if (id.isCurrent) this.setState({Header:id.root});
          else hrefs.push(id);
        }
        this.setState({hrefs: hrefs});
      }).catch(err => {
        throw new Error(err);
      })
    }).catch(err => {
    });
  }

  render() {
    
    return (
      <Card cardShadow='no-shadow' className="container Forum">
        <h2 className={"Forum__header"}>{this.state.Header}</h2>
        <div>{
            this.state.hrefs.length > 0 ?
              this.state.hrefs.map((item,index) => ((<span key={item.id}><a href={index === 0 ? "/forum" : `/forum/${item.id}`} className={"Forum__href"}>{item.root}</a> / </span>))):<div/>
          }</div>
        <div className={"Forum__blog"}>
          {<ForumItem element={this.state.topicsmessage} id={this.props.match.params.id}/>}
        </div>

        <Button onClick={() => {window.location.href = `/forum-topic-add/${this.props.match.params.id}`}} text='Создать топик' className='button--gray-ghost Forum__add-topic'/>
      </Card>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },
  })
)(forums);