import React, {Component} from 'react';
import {connect} from 'react-redux';
import Card from '../Card';
import '../../Styles/userHelp.scss';
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody,
} from 'react-accessible-accordion';
import '../../Styles/accordion.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class UserHelp extends Component {
  state = {
    isAdmin: false
  };

  componentDidMount() {
    if (this.props.user.authorized) this.props.onLoad();
    else this.props.onHideMenu();

    if (this.props.user.authorized) {
      // getting status about admin rights
      // Validate role by access to admin panel
      fetch(`${process.env.API_URL}/security/${this.props.user.info._id}/admin`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        console.log(response.status);
        if (response.status === 200) this.setState({isAdmin: true});
      }).catch(err => {
        throw new Error(err);
      });
    }
  }

  render() {
    let adminHelp;
    if (this.state.isAdmin) {
      adminHelp = (
        <AccordionItem>
          <AccordionItemTitle>Администрирование</AccordionItemTitle>
          <AccordionItemBody>
            <h4 className="help__subtitle">В административной панели сайта, присутствует разделение действий по категориям</h4>

            <AccordionItem>
              <AccordionItemTitle>1. Пользователи</AccordionItemTitle>
              <AccordionItemBody>
                <div className="help__text">
                  В данном разделе происходит управление пользователями

                  <h4 className="help__subtitle">Удаление пользователя из системы</h4>
                  <p> Удалить можно только пользователя, который полностью <strong>подтвержден</strong> (т.е подтверил e-mail и был подтвержден администратором)</p>
                  <ol>
                    <li>Перейти в раздел "Пользователи" -> "Подтвержденные пользователи"</li>
                    <li>Напротив нужного пользователя нажать на кнопку "Удалить аккаунт"</li>
                  </ol>

                  <h4 className="help__subtitle">Деактивация аккаунта</h4>
                  <p> В отличие от удаления, деактивация аккаунта запрещает доступ на вход полностью сохраняя все данные о пользователе </p>
                  <p> Деактивировать можно только пользователя, который полностью <strong>подтвержден</strong> (т.е подтверил e-mail и был подтвержден администратором)</p>
                  <ol>
                    <li>Перейти в раздел "Пользователи" -> "Подтвержденные пользователи"</li>
                    <li>Напротив нужного пользователя нажать на кнопку "Деактивация пользователя"</li>
                  </ol>

                  <h4 className="help__subtitle">Подтверждение/отклонение аккаунта</h4>
                  <div className="help__text">
                    Для того, чтобы пользователь мог войти в систему нужно его подтвердить. В данном разделе вы можете это сделать.
                    <p>Пользователи для подтверждения появляются здесь только после того, как он подтвердил свой E-mail</p>

                    <h4 className="help__subtitle">Для подтверждения/отклонения аккаунта</h4>
                    <ol>
                      <li>Перейти в раздел "Пользователи" -> "Подтверждение пользователей"</li>
                      <li>Проверить медицинские документы. Если нужно увеличить изображение, то щелкните по нему</li>
                      <li>Нажать на кнопку "Подтвердить аккаунт" или "Отклонить заявку" в зависимости от выбранного решения</li>
                    </ol>
                    <p>При отклонении запроса на подтверждение все данные об этом пользоваетеле остаются в системе! <strong>(см. Деактивация аккаунта)</strong></p>
                  </div>
                </div>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>2. Документы</AccordionItemTitle>
              <AccordionItemBody>
                <div className="help__text">
                  В данном разделе происходит подтверждение/отклонение статуса медицинских документов
                  <p>Здесь отображаются все документы, которые нужно подтвердить</p>
                  <h4 className="help__subtitle">Подтверждение/отклонение статуса документов</h4>
                  <ol>
                    <li>Перейти в раздел "Документы"</li>
                    <li>Под нужным документом нажать на кнопку "Подтвердить" или "Отклонить" в зависимости от выбранного решения</li>
                  </ol>
                </div>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>3. Вакансии</AccordionItemTitle>
              <AccordionItemBody>
                <div className="help__text">
                  В данном разделе происходит подтверждение/отклонение вакансий, которые ожидают модерации
                  <h4 className="help__subtitle">Подтверждение/отклонение статуса вакансии</h4>
                  <ol>
                    <li>Перейти в раздел "Вакансии"</li>
                    <li>Под нужной вакансией нажать на кнопку "Принять" или "Отклонить" в зависимости от решения</li>
                  </ol>
                </div>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>4. Форум</AccordionItemTitle>
              <AccordionItemBody>
                <div className="help__text">
                  В данном разделе происходит управление структурой форума
                  <h4 className="help__subtitle">Добавление категории</h4>
                  Категория - это "главная" тема.
                  <ol>
                    <li>Перейти в раздел "Форум"</li>
                    <li>Нажать на кнопку "Добавить категорию"</li>
                    <li>Ввести название и нажать на "Создать"</li>
                  </ol>

                  <h4 className="help__subtitle">Удаление</h4>
                  <ol>
                    <li>Перейти в раздел "Форум"</li>
                    <li>Нажать на значок "-" возле нужного раздела</li>
                    <li>Подтвердить удаление</li>
                  </ol>

                  <h4 className="help__subtitle">Добавление подраздела</h4>
                  <ol>
                    <li>Перейти в раздел "Форум"</li>
                    <li>Нажать на значок "+" возле нужного раздела</li>
                    <li>Ввести название и нажать на "Создать"</li>
                  </ol>
                </div>
              </AccordionItemBody>
            </AccordionItem>
          </AccordionItemBody>
        </AccordionItem>
      );
    }

    return (
      <Card cardShadow='no-shadow' className='help'>
        <h4 className='help__title'>Справочный центр Гемонет</h4>
        <Accordion accordion={false}>
          <AccordionItem>
            <AccordionItemTitle>Регистрация</AccordionItemTitle>
            <AccordionItemBody>
              <h4 className="help__subtitle">Для регистрации на сайте Гемонет</h4>

              <ol>
                <li>Перейдите на формау авторизации, щелкнув на кнопку "ВОЙТИ", которая находится в шапке сайта</li>
                <li>Нажмите на кнопку "Зарегиструйтесь"</li>
                <li>Введите свои данные в поля</li>
                <li>Нажмите на кнопку "Зарегистрироваться"</li>
                <li>После успешной регистрации, вам нудо будет подтвердить свой E-mail</li>
              </ol>
            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>Подтверждение аккаунта</AccordionItemTitle>
            <AccordionItemBody>
              <h4 className="help__subtitle">Подверждение по E-mail</h4>
              <div className="help__text">
                После успешной регистрации аккаунта, вам на почту придет письмо с активационной ссылкой.
                Для того, чтобы подвердить ваш e-mail необходимо перейти по этой ссылке
              </div>

              <h4 className="help__subtitle">Подверждение со стороны администратора</h4>

              <div className="help__text">
                "Гемонет" является закрытым сообществом, по-этому мы должны подтвердить ваш аккаунт. Для этого, после входа в ваш аккаунт,
                вас перекинет на страницу, где вы должны отправить медицинские документы, которые подтверждают вашу болезнь

                <h4 className="help__subtitle">Отправка документов</h4>
                <ol>
                  <li>Ваш E-mail должен быть подтвержден;</li>
                  <li>Авторизируйтесь в свой аккаунт через форму входа;</li>
                  <li>Нажмите на кнопку "Добавить";</li>
                  <li>Выберете нужное изображение и нажмите "Открыть".</li>
                </ol>

                <p>
                  После этого, выбранное изображение отобразится в списке и будет иметь статус "Не подтверждено".
                  Для обновления статуса можно либо нажать на кнопку "Обновить", либо обновить страницу.
                </p>

                <p>После того, как администратор подтвердит аккаунт, вы автоматически будете пененаправлены на свою страницу</p>
              </div>
            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>Авторизация</AccordionItemTitle>
            <AccordionItemBody>
              <h4 className="help__subtitle">Условия:</h4>

              <ol>
                <li>Вы должны быть зарегитрированы.</li>
              </ol>

              <h4 className="help__subtitle">Для того, чтобы авторизироватся на проекте Гемонет </h4>
              <ol>
                <li>Перейдите на форму авторизации, щелкнув на кнопку "ВХОД", которая находится в шапке сайта;</li>
                <li>Введите свои аутентификационные данные в поля;</li>
                <li>Нажмите на кнопку "Войти".</li>
              </ol>

            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>Восстановление пароля</AccordionItemTitle>
            <AccordionItemBody>
              В случае, если вы забыли свой пароль, вы можете его восстановить

              <h4 className="help__subtitle">Для восстановления</h4>
              <ol>
                <li>Перейдите на форму входа, нажав на кнопку "Вход", которая находится в шапке сайта</li>
                <li>Нажмите на кнопку "Забыли пароль?"</li>
                <li>Введите E-mail, который вы указывали при регистрации</li>
                <li>Нажмите на кнопку "Восстановить пароль". После этого, вам на почту придет письмо с ссылкой для восстановления пароля</li>
                <li>Перейдите по ссылке в этом письме</li>
                <li>Введите новый пароль и нажмите на кнопку "Восстановить пароль"</li>
              </ol>

              <p>После этих шагов, вы можете авторизоваться под новым паролем</p>
            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>Журнал</AccordionItemTitle>
            <AccordionItemBody>
              Пользователь может создавать определенное напоминание

              <h4 className="help__subtitle">Имеется 2 типа напоминаний</h4>
              <AccordionItem>
                <AccordionItemTitle>1. Профилактика</AccordionItemTitle>
                <AccordionItemBody>
                  Данный тип напоминания служит для того, чтобы вы не забыли принять лекарство с нужной дозой

                  <h4 className="help__subtitle">Для того, чтобы создать напоминание с типом "Профилактика"</h4>
                  <ol>
                    <li>В форме "Добавить напоминание" выбрать тип "профилактика"</li>
                    <li>Выбрать нужную дату и время, преппарат, а также его дозировку</li>
                    <li>Нажать на кнпоку "Добавить"</li>
                  </ol>
                </AccordionItemBody>
              </AccordionItem>

              <AccordionItem>
                <AccordionItemTitle>2. Напоминание</AccordionItemTitle>
                <AccordionItemBody>
                  Данный тип служит для того, чтобы вы могли написать произвольный текст напоминания

                  <h4 className="help__subtitle">Для того, чтобы создать напоминание с типом "Напоминание"</h4>
                  <ol>
                    <li>В форме "Добавить напоминание" выбрать тип "напоминание"</li>
                    <li>Ввести нужный текст</li>
                    <li>Нажать на кнпоку "Добавить"</li>
                  </ol>
                </AccordionItemBody>
              </AccordionItem>
            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>План лечения</AccordionItemTitle>
            <AccordionItemBody>
              Здесь пользователь может смотреть назначения, которые составил лечащий врач

              <h4 className="help__subtitle">Особенности для каждой роли</h4>
              <AccordionItem>
                <AccordionItemTitle>1. Пользователь</AccordionItemTitle>
                <AccordionItemBody>
                  В разделе календарь вы можете просмотреть напоминания, которые создал ваш лечащий врач

                  <h4 className="help__subtitle">Для того, чтобы отправить запрос врачу на становление вашим лечащим</h4>
                  <ol>
                    <li>Перейти во вкладку "Общение с врачом"</li>
                    <li>Из выпадающего списка выбрать нужного специалиста и нажать на кнопку "Выбрать этого врача"</li>
                    <li>Выполнить 2 шага: специалист должен быть у вас в друзьях и только затем можно отправлять запрос</li>
                  </ol>

                  <h4 className="help__subtitle">Для того, чтобы написать специалисту сообщение</h4>
                  <ol>
                    <li>Перейти во вкладку "Общение с врачом"</li>
                    <li>Ввести нужный текст и нажать на кнопку "Отправить"</li>
                  </ol>
                </AccordionItemBody>
              </AccordionItem>

              <AccordionItem>
                <AccordionItemTitle>2. Специалист-консультант</AccordionItemTitle>
                <AccordionItemBody>
                  <h4 className="help__subtitle">Для того, чтобы принять запрос на становление лечащим врачом пользователя</h4>
                  <ol>
                    <li>Нажать на пункт "Пациенты", а затем перейти на "Запросы"</li>
                    <li>Возле нужного пользователя нажать на кнопку "Принять"</li>
                  </ol>

                  <h4 className="help__subtitle">Для чтения/отправки сообщения пользователю</h4>
                  <ol>
                    <li>Нажать на пункт "Пациенты", а затем перейти на "Общение"</li>
                    <li>Возле нужного пользователя нажать на кнопку "Открыть чат"</li>
                  </ol>

                  <h4 className="help__subtitle">Чтобы назначить консультацию пользователю</h4>
                  <ol>
                    <li>Нажать на пункт "Календарь"</li>
                    <li>Нажать на заголовок "Выбрать пользователя"</li>
                    <li>Нажать нужного пользователя, а затем - "Добавить напоминание"</li>
                    <li>Ввести данные и нажать на кнопку "Создать"</li>
                  </ol>
                </AccordionItemBody>
              </AccordionItem>
            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>Друзья</AccordionItemTitle>
            <AccordionItemBody>
              Здесь вы можете просматривать свой список друзей или же добавлять кого-то из общего списка пользователя

              <h4 className="help__subtitle">Для добавления пользователя в друзья</h4>
              <ol>
                <li>Перейдите в раздел "Поиск"</li>
                <li>Перейдите в профиль нужного человека, нажав на ФИО пользователя</li>
                <li>Нажмите на кнопку "Добавить в друзья" под аватаркой пользователя</li>
              </ol>
            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>Настройки</AccordionItemTitle>
            <AccordionItemBody>
              Личные данные пользователя можно изменять
              <h4 className="help__subtitle">Для перехода к редактированию личных данных необходимо</h4>
              <ul>
                <li>Авторизоваться на сайте</li>
                <li>Нажать на пункт меню "Настройки" в левой части сайта</li>
              </ul>

              <AccordionItem>
                <AccordionItemTitle>1. Основные</AccordionItemTitle>
                <AccordionItemBody>
                  В данном разделе можно редактировать основную личностную информацию
                  <h4 className="help__subtitle">Данные для редактирования</h4>
                  <ol>
                    <li>ФИО</li>
                    <li>Дата рождения</li>
                    <li>Город проживания</li>
                    <li>Мобильный телефон</li>
                    <li>Тип гемофилии</li>
                    <li>Специализация специалиста (только для роли "Специалист-консультант")</li>
                    <li>Информация о себе</li>
                    <li>Ссылки на социальные сети (Vk, Facebook, Google+)</li>
                  </ol>

                  <h4 className="help__subtitle">Для изменения данных</h4>
                  <ol>
                    <li>Изменить нужные данные</li>
                    <li>Нажать на кнопку "Сохранить" внизу страницы</li>
                  </ol>
                </AccordionItemBody>
              </AccordionItem>

              <AccordionItem>
                <AccordionItemTitle>2. Приватность</AccordionItemTitle>
                <AccordionItemBody>
                  В данном разделе можно изменять настройки приватности
                  <h4 className="help__subtitle">Пункты приватности</h4>
                  <ol>
                    <li>
                      <strong>Разрешить просмотр анкеты</strong>
                      <i style={{display: 'block', marginTop: '5px'}}> Данный пункт отвечает за присутствие вашего профиля в поиске </i>
                    </li>

                    <li>
                      <strong>Разрешить просмотр контактов</strong>
                      <p> </p>
                    </li>

                    <li>
                      <strong>Разрешить запрос на добавление</strong>
                      <i style={{display: 'block', marginTop: '5px'}}> Данный пункт отвечает за возможность добавления вас в друзья </i>
                    </li>

                    <li>
                      <strong>Разрешить комментарии</strong>
                      <i style={{display: 'block', marginTop: '5px'}}> Данный пункт отвечает за возможность комментирования ваших постов </i>
                    </li>

                    <li>
                      <strong>Разрешить сообщения</strong>
                      <i style={{display: 'block', marginTop: '5px'}}> Данный пункт отвечает за возможность отправки вам сообщений </i>
                    </li>
                  </ol>

                  <h4 className="help__subtitle">Для изменения настроек</h4>
                  <ol>
                    <li>Изменить нужные данные</li>
                    <li>Нажать на кнопку "Сохранить" внизу страницы</li>
                  </ol>
                </AccordionItemBody>
              </AccordionItem>

              <AccordionItem>
                <AccordionItemTitle>3. Документы</AccordionItemTitle>
                <AccordionItemBody>
                  В данном разделе вы можете просматривать и отправлять администратору медицинские документы

                  <h4 className="help__subtitle">Для отправки документа</h4>
                  <ol>
                    <li>Нажать на кнопку "Добавить", которая находится в последнем блоке с документами</li>
                    <li>Выбрать нужный файл и нажать на "Открыть"</li>
                  </ol>

                  После добавления статус документа будет - Верификация еще не пройдена.
                  Это означает, что документ ожидает модерации со стороны администратора
                </AccordionItemBody>
              </AccordionItem>
            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>Консультации</AccordionItemTitle>
            <AccordionItemBody>
              Если вы хотите задать вопрос специалисту, то можете сделать это здесь
              <h4 className="help__subtitle">Для того, чтобы задать вопрос</h4>
              <ul>
                <li>Перейти на страницу "Консультации"</li>
                <li>Нажать на кнопку "Задать вопрос"</li>
                <li>Ввести нужные данные и нажать на кноку "Создать вопрос"</li>
              </ul>

              После этого вы будете перенаправлены на созданный вопрос.
              Отвечать на него можете только вы, а также специст, который был выбран.

              Для пользователя, который не является специалистом и пользователем, который создал вопрос - поле для комментариев будет отсутствовать

              <h4 className="help__subtitle">Чтобы ответить на вопрос</h4>
              <ul>
                <li>Перейти на страницу вопроса</li>
                <li>В нужное поле ввести текст и нажать на кнопку "Отправить"</li>
              </ul>

              Также можно закрыть вопрос, если ответ был получен. После закрытия вопроса, отвечать на него никто не сможет
              <h4 className="help__subtitle">Чтобы закрыть вопрос</h4>
              <ul>
                <li>Перейти на страницу вопроса</li>
                <li>Нажать на кноку "Закрыть вопрос"</li>
              </ul>
            </AccordionItemBody>
          </AccordionItem>

          <AccordionItem>
            <AccordionItemTitle>Форум</AccordionItemTitle>
            <AccordionItemBody>
              С помощью форума, пользователи сайта могут свободно общаться, оставляя комментарии в топике
              <h4 className="help__subtitle">Для создания топика</h4>
              <ol>
                <li>Перейти на страницу "Форум"</li>
                <li>Выбрать нужную тему</li>
                <li>Нажать на кнопку "Создать топик"</li>
                <li>Ввести название и текст топика и нажать на кноку "Создать"</li>
              </ol>

              После этого вы будете перенаправлены на созданный топик. Комментировать топик могут все пользователи

              <h4 className="help__subtitle">Для комментирования топика</h4>
              <ol>
                <li>Перейти на страницу топика</li>
                <li>В специальную форму ввести текст и нажать на кнопку "Отправить"</li>
              </ol>
            </AccordionItemBody>
          </AccordionItem>

          {adminHelp}
        </Accordion>
      </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },

    onHideMenu() {
      dispatch(SlideMenuAction.default.hideMenu());
    }
  })
)(UserHelp);
