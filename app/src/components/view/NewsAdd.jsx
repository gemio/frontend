import React from 'react';
import {connect} from "react-redux";
import Card from "../Card";
import CardTitle from '../Card/CardEmptyTitle';
import Button from '../Button';
import "../../Styles/NewsAdd.scss";
import InputSimple from "../InputSimple/InputSimple";
import Radio from '../RadioButton/Radio';
import APIMessage from '../APIMessage';
import image from  '../../img/None.png';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class NewsAdd extends React.Component {
  state = {
    gender: 'Публичная',
    message: '',
    image:image
  };

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();
  }

  FileUpload = () => {
    let file = document.querySelector('#myFile').files[0];
    const reader = new FileReader();
    if (file.type === "image/png"|| file.type === "image/jpeg") {
      reader.onloadend = () => {
        this.setState({image: reader.result});
      };

      if (file) reader.readAsDataURL(file);
    }
  };
  createNews = () => {
    const       text = document.getElementById('text').value, previewText = document.getElementById('previewText').value,
      title = document.getElementById('title').value;

    if (!text || !previewText || !title) {
      this.setState({message: 'Заполните все поля'});
      return;
    }

    let data = new FormData();
    data.append('image',this.state.image);
    data.append('news', JSON.stringify({
      date: new Date(),
      text: document.getElementById('text').value,
      previewText: document.getElementById('previewText').value,
      title: document.getElementById('title').value,
      visibility: this.state.gender === 'Публичная' ? 'all' : 'registerOnly'
    }));

    fetch(`${process.env.API_URL}/news/${this.props.user.info._id}`, {
      method: 'POST',
      body: data
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      response.json().then(data => {
        if (response.status === 200) {
          window.location.href = `/news/${data.data.id}`;
        } else console.log(data.message);
      });
    }).catch(err => {console.log(err)});
  };

  render() {
    const onChangeRadio = (e) => {this.setState({gender: e.target.value});};

    return (
        <Card cardShadow='no-shadow' className={"news-create"}>
          <CardTitle>Создание новости</CardTitle>

          <InputSimple id='title' labelText='Заголовок' className="news-create__field"/>

          <InputSimple id="previewText" labelText='Превью новости' className="news-create__field news-create__text" cols="20" rows="5" isMultiline={true}/>
          <InputSimple id="text" labelText='Полный текст' className="news-create__field news-create__text" cols="20" rows="5" isMultiline={true}/>
      
          <div className="summary__gender">
            <span>Видимость новости: </span>
            <Radio name='visibility' value='Публичная' checked={this.state.gender === 'Публичная'} handleChange={onChangeRadio}/>
            <Radio name='visibility' value='Приватная' checked={this.state.gender === 'Приватная'} handleChange={onChangeRadio}/>
          </div>

          <div className={"summary__Image"}>
            <img src={this.state.image} alt='book'/>
            <input id="myFile" type="file" onChange={this.FileUpload} accept="image/jpeg,image/png,image"/>
            <APIMessage status='error' text={this.state.message}/>
          </div>
          <Button onClick={this.createNews} text='Создать' className='button--primary-small-round news-create__submit'/>
        </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user,
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(NewsAdd);
