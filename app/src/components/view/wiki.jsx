import React from 'react';
import Card from '../Card';
import WikiStruct from  '../WikiStruct';
import WikiPost from  '../WikiPost';
import {connect} from 'react-redux';
import Modal from 'react-modal';
import {Icon} from 'react-fa';
import Input from '../Input';
import Button from "../Button/Button";
import '../WikiStruct/WikiStuct.scss';
import APIMessage from '../APIMessage';
import '../../Styles/wiki.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Wiki extends React.Component {
  state = {
    structure: [],
    Object: [],
    isObject: false,
    modalIsOpen: false,
    ErrorList: "",
    isAdmin: false
  };

  componentDidMount() {
    this.props.onLoad();

    this.updateStructure();
    this._getAdminStatus();
  }

  openModal = () => {
    this.setState({modalIsOpen: true});
  };

  closeModal = () => {
    this.setState({modalIsOpen: false});
  };

  update = (data) => {
    this.setState({Object: data, isObject: true});
  };

  updateStructure = () => {
    fetch(`${process.env.API_URL}/lib/structure`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.statusText !== "No Content") {
        response.json().then((data) => {
          this.setState({structure: data.data});
        });
      }
      else this.setState({structure: []});
    });
  };

  _getAdminStatus = () => {
    fetch(`${process.env.API_URL}/security/${this.props.user.info._id}/admin`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      this.setState({isAdmin: response.status === 200});
    }).catch(err => {
      throw new Error(err);
    });
  };

  AddTheme = () => {
    const theme = document.getElementById('header_wiki').value;

    if (theme) {
      fetch(`${process.env.API_URL}/lib/folder/${this.props.user.info._id}?parentId=/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body:`title=${theme}`
      }).then(response => {
        this.updateStructure();
        this.closeModal();
      }).catch(e => {
        this.setState({ErrorList: e});
      });
    }
  };

  render() {
    const customStylesModal = {
      content : {
        padding: '50px',
        display:'flex',
        height:'100%',
        width:'100%',
        algorithm: 'center',
        flexDirection: 'column',
        justifyContent:'center',
        alignItems:'center'
      },

      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };

    let adminButton;
    if (this.state.isAdmin)
      adminButton = (<div className={"wiki__tree__add"} onClick={this.openModal}>Добавить категорию</div>);

    return (
      <Card cardShadow='no-shadow' className="wiki">
        <div className={"wiki__tree"}>
          {this.state.structure.length > 0 ? this.state.structure.map(item=>
            (<WikiStruct isAdmin={this.state.isAdmin} 
                         onUpdate={this.updateStructure} 
                         handle={this.update} 
                         key={item._id} 
                         id={item._id} 
                         children={item.children} 
                         nodeLabel={item.name} />)):<div/>}
          {adminButton}
        </div>

        <div className={"wiki__content"} style={{width:'100%'}}>
          {this.state.isObject ? <WikiPost isAdmin={this.state.isAdmin} onUpdate={this.updateStructure} dat={this.state.Object}/> : <span/>}
        </div>

        <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal} className={"ForumConsultation__modal"}>
          <Card className='wiki-structure__add-header' cardShadow='no-shadow'>
            <Icon name={"times"} className={"Icon__close__wiki"} onClick={this.closeModal} />
            <APIMessage text={this.state.ErrorList} status='error'/>

            <div className="wiki-structure__subtitle">Заголовок раздела</div>
            <Input id="header_wiki" className="wiki-structure__add-title" type="text" />

            <Button onClick={this.AddTheme} text={'Добавить'} className='button--primary wiki-structure__add-button' />
          </Card>
        </Modal>
      </Card>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    }
  })
)(Wiki);
