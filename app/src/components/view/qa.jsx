//Model
import React, {Component} from 'react';
import {connect} from "react-redux";
import Row from '../Row';
import Col from '../Col';
import '../../Styles/qa.scss';
import {Icon} from 'react-fa';
import image from '../../img/request.png';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class qa extends Component {
  componentDidMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onClose();
  }

  render() {
    return (
    <div className="container Qa">
     <a href="home">
        <div className="Qa__click">
          <Icon name=" fa-arrow-left" /> <div className="Qa__click__text">{"    Вернуться к профилю"} </div>
        </div>
      </a>
      <Row>
        <Col xs="12" sm={"12"} md={"12"}  className="Qa__Left">
          <h2 className="Qa__Left__Header">Консультации</h2>
          <p  className="Qa__Left__Text">Получение консультации профессионала на проекте гемофилик.рф осуществляется в 3 шага.</p>
          <ol className="Qa__Left__List">
            <li>Выбор специалиста</li>
            <li>Составление online заявки на консультацию</li>
            <li>Получение консультации</li>
          </ol>
          <p>Начните прямо сейчас!</p>
        </Col>
        <Col lg="3" className="Qa__Right d-none d-lg-block d-xl-block">
          <ol className="Qa__Right__List">
            <li><a href="./home">выбор специалиста</a></li>
            <li><a href="./treatment">составление online заявки на консультацию</a></li>
            <li><a href="./home">ожидание получения консультации</a></li>
          </ol>
        </Col>
      </Row>
      <div className="Qa__image">
        <img src={image} className="Qa__image__img" alt = "Врач" />
      </div>
    </div>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(qa);
