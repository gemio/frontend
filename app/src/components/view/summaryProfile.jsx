import React, {Component} from 'react';
import {connect} from 'react-redux';
import Card from '../Card';
import AvatarCircle from '../AvatarCircle';
import { Link } from 'react-router-dom';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import '../../Styles/summaryProfile.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class SummaryProfile extends Component {
  state = {
    summary: [],
    isAPILoaded: false
  };

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();
  }

  componentDidMount() {
    fetch(`${process.env.API_URL}/user/${this.props.match.params.id}/jobSummary`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({summary: '', isAPILoaded: true});
        return;
      }

      response.json().then(data => {
        this.setState({summary: response.status === 200 ? data.data  : 'none', isAPILoaded: true});
      });
    }).catch(err => {
      throw new Error(err);
    });
  }

  render() {
    let adLanguage = [];
    if (this.state.isAPILoaded && this.state.summary.id) {
      for (let i = 1; i < this.state.summary.language.length; i++)
        adLanguage.push((
          <li key={i}>Дополнительный: {this.state.summary.language[i].name} - {this.state.summary.language[i].level}</li>
        ));
    }

    return (
      this.state.isAPILoaded && this.state.summary.id ?
      <Card cardShadow='no-shadow' className='summary-profile'>
        <div className="summary-profile__header">
          <AvatarCircle img={this.state.summary.avatar}/>
          <Link to={`/id/${this.props.match.params.id}`} className="summary-profile__full-name">{this.state.summary.fullName}</Link>
        </div>

        <div className="summary-profile__content">
          <h4>Общая информация</h4>
          <ul className='summary-profile__info'>
            <li>Телефон: {this.state.summary.phone}</li>
            <li>Пол: {this.state.summary.gender}</li>
            <li>Город: {this.state.summary.city}</li>
            <li>Гражданство: {this.state.summary.nationality}</li>
          </ul>

          <h4>Владение языками</h4>
          <ul className='summary-profile__info'>
            <li>Основной язык: {this.state.summary.language[0].name}</li>
            {adLanguage}
          </ul>
        </div>
      </Card> :
        !this.state.isAPILoaded ? (<CardEmptyTitle>Загрузка</CardEmptyTitle>) :
          (<CardEmptyTitle>Неправильный UserId</CardEmptyTitle>)
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(SummaryProfile);
