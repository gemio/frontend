import React, {Component} from 'react';
import {connect} from 'react-redux';
import Card from '../Card';
import InputSimple from '../InputSimple';
import DropdownSimple from '../DropdownSimple';
import APIMessage from '../APIMessage';
import Button from '../Button';
import LocationSearch from  '../LocationSearchInput';
import '../../Styles/vacancyAdd.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class VacancyAdd extends Component {
  state = {
    message: {
      type: 'success',
      message: ''
    }
  };

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();
  }

  addVacancy = () => {
    fetch(`${process.env.API_URL}/jobs/${this.props.user.info._id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `job=${JSON.stringify({
        title: document.getElementById('title').value,
        company: document.getElementById('company').value,
        address: document.getElementById('address').value,
        city: document.getElementById('city').value,
        text: document.getElementById('text').value,
        contacts: document.getElementById('contacts').value,
        exp: document.getElementsByClassName('vacancy__exp')[0].value,
        price: `От ${document.getElementById('priceStart').value} руб. до ${document.getElementById('priceEnd').value} руб `
      })}`
    }).then(response => {
      if (response.status === 404 || response.status ===  500) console.log('Сервер не отвечает');
      else {
        response.json().then(data => {
          this.setState({message: data.message, type: response.status === 200 ? 'success' : 'error'});
        });
      }
    });
  };

  render() {
    const exp = ['не требуется', 'менее 1 года', 'от 1 до 3 лет', 'от 3 до 6 лет', 'более 6 лет'];

    return (
      <Card cardShadow='no-shadow' className='vacancy-add'>
        <h2 className="vacancy-add__title">Создание вакансии</h2>

        <InputSimple id='title' labelText='Название:' className='vacancy-add__field'/>
        <InputSimple id='company' labelText='Компания:' className='vacancy-add__field'/>
        <InputSimple id='address' labelText='Адрес компании:' className='vacancy-add__field'/>
        <InputSimple id='contacts' labelText='Контакты:' className='vacancy-add__field'/>
        <InputSimple id='text' isMultiline={true} rows={9} cols={12} labelText='Описание:' className='vacancy-add__field'/>

        <div className="vacancy-add__field vacancy-add__price">
          <label>Зарплата: </label>
          <InputSimple id='priceStart' onlyNum={true} onlyNumLabel='руб.' max={9999999} min={0} labelText='От-' className='vacancy-add__field'/>
          <InputSimple id='priceEnd' onlyNum={true} onlyNumLabel='руб.' max={9999999} min={0} labelText='До-' className='vacancy-add__field'/>
        </div>

        <div className="vacancy-add__field vacancy-add__city">
          <label>Город: </label>
          <LocationSearch idInput='city'/>
        </div>

        <div className="vacancy-add__field vacancy-add__exp">
          <label>Опыт: </label>
          <DropdownSimple className='vacancy__exp' items={exp} selectedItem={exp[0]}/>
        </div>

        <APIMessage className="vacancy-add__message" status={this.state.message.type} text={this.state.message.message}/>
        <Button onClick={this.addVacancy} text='Добавить вакансию' className='button--primary vacancy-add__submit'/>
      </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(VacancyAdd);
