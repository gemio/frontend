//Model
import React, {Component} from 'react';
import {connect} from "react-redux";
import Promo from "../JobsPromo";
import SmallJobs from  '../VacancySmall';
import Card from '../Card';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import Button from '../Button';
import JobsFilter from '../JobsFilter';
import "../../Styles/Jobs.scss";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Jobs extends Component {
  state = {
    jobs: [],
    Loaded: false
  };

  componentWillMount() {
    if (this.props.user.authorized) this.props.onLoad();
    else this.props.onClose();

    fetch(`${process.env.API_URL}/jobs`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({reminds: '', remindsLoaded: true});
        return;
      }

      if (response.status === 412){
        this.setState({remindsLoaded: true});
        return;
      }

      response.json().then(data => {
        this.setState({jobs: data.data,Loaded:true});
      });
    }).catch(err => {
      throw new Error(err);
    });
  }

  onFilter = (response) => {
    if (response.status === 204) this.setState({jobs: []});
    else {
      response.json().then(data => {
        if (response.status === 200) this.setState({jobs: data.data});
      });
    }
  };

  render() {
    return (
      <Card cardShadow='no-shadow' className="Jobs">
        <Promo col={this.state.jobs.length}/>
        <JobsFilter onFilter={this.onFilter}/>
        <div className={"Jobs__row"}>
          { !this.state.Loaded ? (<CardEmptyTitle>Загрузка...</CardEmptyTitle>) : this.state.Loaded && this.state.jobs.length > 0 ?
            this.state.jobs.map(item => ((
              <SmallJobs key={item.id} name={item.title} textot={item.price} tag={item.company} href={`/jobs/${item.id}`}/>))) :
            (<CardEmptyTitle>Нет вакансий</CardEmptyTitle>)
          }

          <Button onClick={() => {window.location.href = '/vacancy-create'}} className='button--gray-ghost jobs__add-vacancy' text='Разместить вакансию'/>
        </div>
      </Card>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.hideMenu());
    }
  })
)(Jobs);