//Model
import React, { Component } from "react";
import { connect } from 'react-redux';
import Card from '../Card';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import TabControl from '../TabControl';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import LastMessage from '../LastMessage';
import moment from 'moment';
import "../../Styles/dialogs.scss";
import Button from "../Button/Button";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Messages extends Component {
  state = {
    message: [],
    isMessage: false,
  };

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();

  	if (!this.state.isMessage) {
      fetch(`${process.env.API_URL}/message/last/${this.props.user.info._id}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        response.json().then((data) => {
          let messages = data.data.map(msg => {
          	return {
          		text: msg.message.text,
          		date:  moment(new Date().fromNow(new Date(msg.message.date))).fromNow(),
          		fullName: msg.message.userID === this.props.user.info.id ? msg.message.fromUser : msg.message.toUser,
              id: msg.message.userID === this.props.user.info.id ? msg.message.fromUserID : msg.message.userID,
              avatar: msg.message.avatar
            };
          });
          this.setState({message: messages, isMessage: true , errorText: data.message.message, isLoginSuccess: response.status === 200});
        });
      }).catch((response) => {});
	  }
  }

  render() {
    let messages;
    if (!this.state.isMessage)
      messages = (<CardEmptyTitle>Загрузка...</CardEmptyTitle>);
    else if (this.state.isMessage && this.state.message.length > 0) {
      messages = this.state.message.map((item, index) => ((
        <LastMessage key={index} avatar={item.avatar} idDialog={item.id} name={item.fullName} date={item.date} text={item.text}/>)));
    }
    else
      messages = (<CardEmptyTitle>Нет сообщений</CardEmptyTitle>);
    return (
      <Card cardShadow='no-shadow' className="messages">
        <h4 className="card__title">Сообщения</h4>
        <Button onClick={() => {window.location.href = '/friends?tab=all'}} className="button--primary button--rounded messages__write" text="Написать"/>
        <div className={"messages__body"}>  {messages}</div>

      </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(Messages);
