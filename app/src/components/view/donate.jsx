import React, {Component} from 'react';
import Container from '../Container';
import Row from '../Row';
import Col from '../Col';
import Button from '../Button';
import {connect} from 'react-redux';
import '../../Styles/donate.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Donate extends Component {
  componentDidMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onClose();
  }

  render() {
    return (
      <Container className="donate">
        <Row>
          <Col xs="12" className={"donate__fel"}>
            <h1 className="donate__title">Спонсорская помощь больным гемофилией</h1>
            <p>Если вы хотите изменить жизнь конкретного больного, приглашаем Вас воспользоваться спонсорской программой проекта <strong>гемонет</strong></p>
            <p className={"donate__Child2"}>Основные факты по работе с системой:</p>

            <div className="donate__list">
              <div className="donate__item">
                <div className="donate-item__title">1. Таргетированная помощь</div>
                <div className="donate-item__description">Вы выбираете конкретного человека, которому хотите помочь.</div>
              </div>

              <div className="donate__item">
                <div className="donate-item__title">2. Прозрачная и понятная система</div>
                <div className="donate-item__description">Система поиска претендентов и осуществления помощи максимально прозрачна благодаря полностью интерактивному интерфейсу. После регистрации за вами закрепляется личный куратор, который будет помогать и оповещать о ходе вашего пожертвования и лечения больного.</div>
              </div>

              <div className="donate__item">
                <div className="donate-item__title">3. Гибкость и удобство</div>
                <div className="donate-item__description">Вы можете как полностью финансировать лечение, так и принять частичное участие.</div>
              </div>
            </div>

            <h4>Для участия в программе необходимо зарегистрироваться на проекте в качествве спонсора</h4>
            <Button onClick={() => {window.location.href = '/login'}} className="button--primary" text="Хочу помочь"/>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(Donate);
