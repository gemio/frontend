import React, {Component} from 'react';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import { connect } from 'react-redux';
import Card from '../Card';
import Form from '../Form';
import Button from '../Button';

import TabControl from '../TabControl';
import Radio from '../RadioButton/Radio';
import InputSimple from '../InputSimple';
import DropdownSimple from '../DropdownSimple';
import DateTimePicker from '../DateTimePicker';

import '../../Styles/profileSetting.scss';
import ProfilePicture from "../ProfilePicture";
import APIMessage from "../APIMessage/APIMessage";
import AddProfileDocument from '../AddProfileDocument';
import ProfileDocument from '../ProfileDocument';

import moment from 'moment';
import 'moment/locale/ru';
import PDF from "../PDF/PDF";

const UserAction = require('../../actions/UserAction.js');
const SlideMenuAction = require('../../actions/SlidemenuAction.js');
moment.locale('ru');

class ProfileSetting extends Component {
  state = {
    documents: [],
    documentsMessage: {type: '', message: ''},
    privacyReadProfile: this.props.user.info.privacy.allowReadForm === 'true',
    privacyContactsProfile: this.props.user.info.privacy.allowReadContacts === 'true',
    privacyFriendRequestProfile: this.props.user.info.privacy.allowFriendRequest === 'true',
    privacyCommentsProfile: this.props.user.info.privacy.allowComments === 'true',
    privacyMessageProfile: this.props.user.info.privacy.allowMessages === 'true',
  };

  componentWillMount() {
    if (!this.props.user.authorized && !this.props.user.info && !this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();
    this.receiveDocuments();
  }

  receiveDocuments = (data = false) => {
    // set api message if we have an information about last added stastus
    if (data) this.setState({documentsMessage: data.message});

    // Receive documents
    fetch(`${process.env.API_URL}/user/${this.props.user.info._id}/document`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({documents: ''});
        return;
      }

      response.json().then(data => {
        if (response.status === 200) this.setState({documents: data.data});
      })
    }).catch(err => {
      console.log(err);
    });
  };

  dropApiMessageState = () => {
    const documentsMessage = {type: '', message: ''}
    this.setState({documentsMessage: documentsMessage});
  };

  privacyReadProfileHandle = (e) => {
    this.setState({privacyReadProfile: e.target.value === 'Да'});
  };

  privacyContactsHandle = (e) => {
    this.setState({privacyContactsProfile: e.target.value === 'Да'});
  };

  privacyFriendRequestHandle = (e) => {
    this.setState({privacyFriendRequestProfile: e.target.value === 'Да'});
  };

  privacyCommentsHandle = (e) => {
    this.setState({privacyCommentsProfile: e.target.value === 'Да'});
  };

  privacyMessagesHandle = (e) => {
    this.setState({privacyMessageProfile: e.target.value === 'Да'});
  };

  render() {
    let documents;
    if (this.state.documents.length > 0)
      documents = this.state.documents.map(item => {
        return (
          <ProfileDocument key={item.id} className={"profile-edit__documents profile-edit__documents--" + item.status}>
            {item.img.includes('pdf') ? <PDF/> : <img alt="gem-document" src={item.img}/>}
          </ProfileDocument>);
      });

    return (
      <Card cardShadow='no-shadow' className="profile-edit">
        <h4 className="card__title">Настройки</h4>
        <TabControl onChange={this.dropApiMessageState} className="profile-edit__tabs">
          <TabList>
            <Tab tabFor="basicProfile">Основные</Tab>
            <Tab tabFor="privacyProfile">Приватность</Tab>
            <Tab tabFor="documentsProfile">Документы</Tab>
          </TabList>

          <TabPanel tabId="basicProfile">
            <ProfilePicture isEditable={true} className="profile__picture-block profile-edit__picture"/>

            <h4>Личная информация</h4>
            <Form className="profile-edit__basic-form">
              <InputSimple id="surname" value={this.props.user.info.public.fullName.surname} className="profile-edit__basic-input" alignment="horizontal" labelText="Фамилия"/>
              <InputSimple id="name" value={this.props.user.info.public.fullName.name} className="profile-edit__basic-input" labelText="Имя"/>
              <InputSimple id="lastname" value={this.props.user.info.public.fullName.lastname} className="profile-edit__basic-input" labelText="Отчетство"/>
              <DateTimePicker startDate={this.props.user.info.public.birthday === '' || !this.props.user.info.public.birthday ? moment() : moment(this.props.user.info.public.birthday)} title="Дата рождения" className="profile-edit__basic-birthdate input--simple profile-edit__basic-input"/>
              <InputSimple id="city" value={this.props.user.info.public.city} className="profile-edit__basic-input" labelText="Город"/>
              <InputSimple value={this.props.user.info.phone} alwaysShowMask={true} isMask={true} mask='+7 (999) 999-99-99' inputRef={(node) => this._phone = node} className="profile-edit__basic-input" labelText="Мобильный телефон"/>
              <InputSimple isReadOnly={true} id="email" value={this.props.user.info.email} className="profile-edit__basic-input" labelText="Электронная почта"/>
              <InputSimple isReadOnly={true} id="password" value="********" className="profile-edit__basic-input" labelText="Пароль"/>

              <div className="input--simple profile-edit__basic-input">
                <label className='input--simple__label input--simple__label--horizontal'>Тип гемофилии</label>
                <DropdownSimple className='profile__hemo-status' selectedItem={this.props.user.info.hemoStatus} items={['Гемофилия А', 'Гемофилия B', 'Гемофилия C']}/>
              </div>

              {this.props.user.info.roleSpecialist ?
                <div className="input--simple profile-edit__basic-input">
                  <label className='input--simple__label input--simple__label--horizontal'>Специалист</label>
                  <DropdownSimple className='profile__hemo-specialist' selectedItem={this.props.user.info.roleSpecialist} items={['Врач', 'Юрист']}/>
                </div>: null
              }
            </Form>

            <h4>Дополнительно</h4>
            <Form className="profile-edit__basic-form">
              <InputSimple id="about" value={this.props.user.info.public.about} labelText="О себе" cols="45" rows="30" isMultiline={true}/>
            </Form>

            <h4>Социальные сети</h4>
            <Form className="profile-edit__basic-form">
              <InputSimple alwaysShowMask={true} isMask={true} mask='https://vk.com/id99999999' inputRef={(node) => this._vk = node} value={this.props.user.info.public.social.vk} className="profile-edit__basic-input profile-edit__social-input" labelText="Вконтакте" />
              <InputSimple alwaysShowMask={true} isMask={true} mask='https://www.f\acebook.com/************************' inputRef={(node) => this._facebook = node} value={this.props.user.info.public.social.facebook} className="profile-edit__basic-input profile-edit__social-input" labelText="Facebook" />
              <InputSimple alwaysShowMask={true} isMask={true} mask='https://plus.google.com/u/0/999999999999999999999' inputRef={(node) => this._googlePlus = node} value={this.props.user.info.public.social.googlePlus} className="profile-edit__basic-input profile-edit__social-input" labelText="Google+" />
            </Form>

            <APIMessage status={this.props.message.type === 'error' ? 'error' : 'success'} text={this.props.message.message}/>
            <Button onClick={() => this.props.updateInfo(this.props.user.info, {phone: this._phone.value, vk: this._vk.value.trim(), facebook: this._facebook.value.trim(), googlePlus: this._googlePlus.value.trim()})} className="button--primary profile-edit__basic-save" text="Сохранить"/>
          </TabPanel>

          <TabPanel tabId="privacyProfile">
            <h4>Моя анкета</h4>

            <div className="privacyProfile__field profile-edit__privacy-toggle">
              <span>Разрешить просмотр анкеты: </span>
              <Radio name='allow-read-profile' value='Да' checked={this.state.privacyReadProfile === true} handleChange={this.privacyReadProfileHandle}/>
              <Radio name='allow-read-profile' value='Нет' checked={this.state.privacyReadProfile === false} handleChange={this.privacyReadProfileHandle}/>
            </div>

            <div className="privacyProfile__field profile-edit__privacy-toggle">
              <span>Разрешить просмотр контактов: </span>
              <Radio name='allow-read-contact' value='Да' checked={this.state.privacyContactsProfile === true} handleChange={this.privacyContactsHandle}/>
              <Radio name='allow-read-contact' value='Нет' checked={this.state.privacyContactsProfile === false} handleChange={this.privacyContactsHandle}/>
            </div>

            <div className="privacyProfile__field profile-edit__privacy-toggle">
              <span>Разрешить запрос на добавление: </span>
              <Radio name='allow-friend-request' value='Да' checked={this.state.privacyFriendRequestProfile === true} handleChange={this.privacyFriendRequestHandle}/>
              <Radio name='allow-friend-request' value='Нет' checked={this.state.privacyFriendRequestProfile === false} handleChange={this.privacyFriendRequestHandle}/>
            </div>

            <div className="privacyProfile__field profile-edit__privacy-toggle">
              <span>Разрешить комментарии: </span>
              <Radio name='allow-comments' value='Да' checked={this.state.privacyCommentsProfile === true} handleChange={this.privacyCommentsHandle}/>
              <Radio name='allow-comments' value='Нет' checked={this.state.privacyCommentsProfile === false} handleChange={this.privacyCommentsHandle}/>
            </div>

            <div className="privacyProfile__field profile-edit__privacy-toggle">
              <span>Разрешить сообщения: </span>
              <Radio name='allow-message' value='Да' checked={this.state.privacyMessageProfile === true} handleChange={this.privacyMessagesHandle}/>
              <Radio name='allow-message' value='Нет' checked={this.state.privacyMessageProfile === false} handleChange={this.privacyMessagesHandle}/>
            </div>

            <Button onClick={() => this.props.updatePrivacy(this.props.user.info, this.state)} className="button--primary profile-edit__privacy-submit" text="СОХРАНИТЬ"/>
            <APIMessage status={this.props.message.type === 'error' ? 'error' : 'success'} text={this.props.message.message}/>
          </TabPanel>

          <TabPanel tabId="documentsProfile">
            <h4>Верификационные документы</h4>
            <div className="profile-edit__document-container">
              {documents}
              <AddProfileDocument addDocumentHandler={this.receiveDocuments}/>
            </div>

            <APIMessage className='profile-edit__api-documents' status={this.state.documentsMessage.type === 'error' ? 'error' : 'success'} text={this.state.documentsMessage.message}/>
          </TabPanel>
        </TabControl>
      </Card>
    );
  }
}

export default connect(
  state => ({
    user: state.user,
    message: state.message
  }),
  dispatch => ({
    updateInfo(credits, info) {
      let birthDateRaw = document.getElementById('DatePick').value.split('.');
      let userPublicInfo = {
        login: credits.email,
        token: credits._id,
        fullName: {
          name: document.getElementById('name').value,
          surname: document.getElementById('surname').value,
          lastname: document.getElementById('lastname').value,
        },
        birthday: new Date(+birthDateRaw[2], +birthDateRaw[1] - 1, +birthDateRaw[0]).toISOString(),
        city: document.getElementById('city').value,
        about: document.getElementById('about').value,
        status: credits.public.status,
        hemoStatus: document.getElementsByClassName('profile__hemo-status')[0].value,
        social: {
          vk: info.vk,
          facebook: info.facebook,
          google: info.googlePlus,
        },
        phone: info.phone
      };

      if (credits.role === 'Специалист-консультант')
        userPublicInfo.roleSpecialist = document.getElementsByClassName('profile__hemo-specialist')[0].value;

      dispatch(UserAction.default.updateUserInformation(userPublicInfo));
    },

    updatePrivacy(credits, state) {
      let privacyInfo = {
        login: credits.email,
        token: credits._id,
        allowReadForm: state.privacyReadProfile,
        allowReadContacts: state.privacyContactsProfile,
        allowFriendRequest: state.privacyFriendRequestProfile,
        allowComments: state.privacyCommentsProfile,
        allowMessages: state.privacyMessageProfile
      };
      dispatch(UserAction.default.updatePivacyInformation(privacyInfo));
    },

    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(ProfileSetting);
