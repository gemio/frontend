//Model
import React, { Component } from "react";
import { connect } from 'react-redux';
import Card from '../Card';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import Row from '../Row';
import NewsLastItem from '../NewsLastItem';
import "../../Styles/news.scss";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');
class news extends Component {
  state = {
    news: [],
    topnews: [],
    Loaded:false
  };

  componentDidMount() {
    if (this.props.user.authorized) this.props.onLoad();
    else this.props.onClose();

    let idNews = this.props.match.params.id;
    fetch(`${process.env.API_URL}/news?id=${idNews}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        if (response.status === 412 || response.status === 500) {
          window.location.href = '/news';
          return;
        }

        let months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Ноября', 'Декабря'];
        
        response.json().then((data) => {
          let datnews = data.data;
          let newsreplicon = {
            date: new Date(datnews.date).getDate()+" "+months[new Date(datnews.date).getMonth()]+" "+new Date(datnews.date).getFullYear(),
            time: datnews.date.split('T')[1].split(':')[0]+":"+datnews.date.split(':')[1],
            image: datnews.img,
            title: datnews.title,
            text: datnews.text
          };

         this.setState({news: newsreplicon});
        });
      }).catch((response) => {});

       fetch(`${process.env.API_URL}/news/preview?count=10`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        response.json().then((data) => {
          let topsnews = [];
          if (data.message.type === "error") {
            this.setState({Loaded: true, error: data.message.message});
            return;
          }

          let months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Ноября', 'Декабря'];
          if (response.status !== 412) {
            topsnews = data.data.map(msg => {
              return {
                day: new Date(msg.date).getDate(),
                date: new Date(msg.date).getDate()+" "+months[new Date(msg.date).getMonth()],
                time: msg.date.split('T')[1].split(':')[0]+":"+msg.date.split(':')[1],
                title: msg.title,
                link : msg.link,
                id: msg._id
              };
            });
          }
          this.setState({topnews: topsnews,Loaded:true});
        });
      }).catch((response) => {});
  }

  render() {
    return (
      <div className="news-full">
        <Row className="news-full__block">
          <Card className="col-xl-8 col-lg-7 col-md-12 news-full__block__Left">
            <div className="news-full__header">
              <span className="news-full__date">{this.state.news.time}</span>
              <span className="news-full__time">{this.state.news.date}</span>
              <h1 className="news-full__header__title">{this.state.news.title} </h1>
            </div>

            <img alt="gem-news" src={this.state.news.image} className="news-full__image"/>
            <div className="news-full__content">{this.state.news.text}</div>
          </Card>

          <Card className="news-full__All col-xl-3 col-lg-4 col-md-12">
            <div className="news-full__All__header">Последние новости</div>
            { !this.state.Loaded ? (<CardEmptyTitle>Загрузка...</CardEmptyTitle>) : this.state.Loaded && this.state.topnews.length > 0 ?
            this.state.topnews.map(item => ((
              <NewsLastItem key={item.link} date={item.date} time={" "+item.time} title={item.title} href={item.link} />))) :
            (<CardEmptyTitle>Нет новостей</CardEmptyTitle>)
            }
          </Card>
        </Row> 
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.hideMenu());
    }
  })
)(news);
