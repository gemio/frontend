import React from 'react';
import {connect} from 'react-redux';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import TabControl from '../TabControl';
import Col from '../Col';
import Row from '../Row';
import Container from '../Container';
import Card from '../Card';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import FriendList from '../FriendList';
import FriendItem from '../FriendItem';
import InputSimple from '../InputSimple';
import '../../Styles/friends.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');
const NotificationAction = require('../../actions/NotificationAction.js');

class Friends extends React.Component {
  state = {
    friends: [],
    users: [],
    friendsRequest: [],
    isAPILoaded: false
  };

  /**
   * Method for getting info about user friends
   * */
  loadFriends() {
    fetch(`${process.env.API_URL}/friend/${this.props.user.info._id}/accepted`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then(response => {
      // API is not accessible
      if (response.status === 500 || response.status === 404) {
        this.setState({friends: '', isAPILoaded: true});
        return;
      }

      // Parse response json
      response.json().then(data => {
        this.setState({isAPILoaded: true, friends: response.status === 200 ? data.data.friends : data.message.message});
      }).catch(err => {
        console.warn(`Failed to parse a json! Error text: ${err}`);
      })
    }).catch(err => {  });
  };

  /**
   * Method for getting info about users, who allow friend request
   * */
  loadUsers() {
    fetch(`${process.env.API_URL}/user/${this.props.user.info._id}/allowFriend`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then(response => {
      // API is not accessible
      if (response.status === 500 || response.status === 404) {
        this.setState({users: '', isAPILoaded: true});
        return;
      }

      // Parse response json
      response.json().then(data => {
        this.setState({isAPILoaded: true, users: response.status === 200 ? data.data : data.message.message});
      }).catch(err => {
        console.warn(`Failed to parse a json! Error text: ${err}`);
      })
    }).catch(err => { window.location.replace(`${process.env.URL_FOLDER}Error500`); });
  };

  /**
   * Method for getting info about users, who allow friend request
   * */
  loadFriendsRequest() {
    fetch(`${process.env.API_URL}/friend/${this.props.user.info._id}/requested`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then(response => {
      // API is not accessible
      if (response.status === 500 || response.status === 404) {
        this.setState({users: '', isAPILoaded: true});
        return;
      }

      // Parse response json
      response.json().then(data => {
        this.setState({isAPILoaded: true, friendsRequest: response.status === 200 ? data.data : data.message.message});
      }).catch(err => {
        console.warn(`Failed to parse a json! Error text: ${err}`);
      })
    }).catch(err => { window.location.replace(`${process.env.URL_FOLDER}Error500`); });
  };

  componentDidMount() {
    // Check for authorized user
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }

    // Open a slide menu
    this.props.onLoad();
    this.loadFriends = this.loadFriends.bind(this);
    this.loadUsers = this.loadUsers.bind(this);
    this.loadFriendsRequest = this.loadFriendsRequest.bind(this);

    // Load friend's info from api
    this.loadFriends();
    this.loadUsers();
    this.loadFriendsRequest();

    document.getElementById('search-user').addEventListener('keypress', (e) => {
      let key = e.which || e.keyCode;

      // If user press ENTER key inside input
      // Then we send a request with search param
      if (key === 13) {
        let value = document.getElementById('search-user').value;
        fetch(`${process.env.API_URL}/user/${this.props.user.info._id}/search?search=${encodeURIComponent(value)}`, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          }
        }).then(response => {
          // API is not accessible
          if (response.status === 500 || response.status === 404) {
            this.setState({users: '', isAPILoaded: true});
            return;
          }

          // Parse response json
          response.json().then(data => {
            this.setState({isAPILoaded: true, users: response.status === 200 ? data.data : data.message.message}, () => {
              document.getElementById('search-user').value = value;
            });
          }).catch(err => {
            console.warn(`Failed to parse a json! Error text: ${err}`);
          })
        }).catch(err => { window.location.replace(`${process.env.URL_FOLDER}Error500`); });
      }
    });
  }

  render() {
    let friends;
    if (!this.state.isAPILoaded) friends = (<CardEmptyTitle>Загрузка...</CardEmptyTitle>);
    else if (this.state.isAPILoaded && Array.isArray(this.state.friends)) friends = this.state.friends.map(friend => (<FriendItem key={friend.id} {...friend}/>));
    else friends = (<CardEmptyTitle text='Нет друзей'/>);

    let users;
    if (!this.state.isAPILoaded) users = (<CardEmptyTitle>Загрузка...</CardEmptyTitle>);
    else if (this.state.isAPILoaded && Array.isArray(this.state.users)) users = this.state.users.map(friend => (<FriendItem key={friend.id} {...friend}/>));
    else users = (<CardEmptyTitle text='Нет пользователей'/>);

    let friendsRequest, friendTab;
    if (this.state.isAPILoaded && Array.isArray(this.state.friendsRequest)) {
      friendsRequest = (
        <TabPanel tabId="friendsReq">
          <div className="friendsRequest">
            <FriendList>
              {this.state.friendsRequest.map(friend => (<FriendItem acceptHandler={() => {this.loadFriends(); this.loadUsers(); this.loadFriendsRequest(); this.props.updateNotification(this.props.user)}} token={this.props.user.info._id} isFriendRequest={true} key={friend.id} {...friend}/>))}
            </FriendList>
          </div>
        </TabPanel>
      );
      friendTab = (<Tab tabFor="friendsReq">Запросы в друзья ({this.state.friendsRequest.length})</Tab>);
    }

    return (
      <Container style={{marginTop: '25px'}}>
        <Row>
          <Col style={{paddingLeft: '0'}} xs="12">
            <Card style={{padding: '15px'}} cardShadow='no-shadow'>
              <TabControl>
                <TabList>
                  <Tab tabFor="all">Все друзья ({typeof this.state.friends === 'string' ? '0' : friends.length})</Tab>
                  <Tab tabFor="search">Поиск</Tab>
                  {friendTab}
                </TabList>

                <TabPanel tabId="all">
                  <div className="friends">
                    <FriendList>
                      {friends}
                    </FriendList>
                  </div>
                </TabPanel>

                <TabPanel tabId="search">
                  <Card cardShadow='no-shadow' className="friends">
                    <InputSimple id='search-user' placeholder="Введите имя пользователя"/>
                    <FriendList>
                      {users}
                    </FriendList>
                  </Card>
                </TabPanel>

                {friendsRequest}
              </TabControl>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },

    updateNotification(user) {
      dispatch(NotificationAction.default.getNotification(user));
    }
  }),
)(Friends);
