import React, { Component } from "react";
import { connect } from 'react-redux';
import ProfilePicture from '../ProfilePicture';
import ProfileInformation from '../ProfileInformation';
import ProfileTips from '../ProfileTips';
import ProfileTipItem from '../ProfileTipItem';
import Reminder from '../Reminder';
import CalendarTip from '../../img/profile-tip/calendar.png';
import PlanTip from '../../img/profile-tip/plan.png';
import SpecialistTip from '../../img/profile-tip/specialist.png';
import TabControlHome from '../TabControlHome';
import PostForm from '../PostForm';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import Card from '../Card';
import Container from '../Container';
import Row from '../Row';
import Col from '../Col';
import Entry from '../Entry';
import moment from 'moment';
import "../../Styles/home.scss";
import Button from "../Button/Button";
moment.suppressDeprecationWarnings = true;
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Home extends Component {
  state = {
    reminds: [],
    remindsLoaded: false,
    News:[],
    posts: [],
    ImageNewPost:[],
    Forum:[]
  };

  componentWillMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();
    this.updatePosts();

    // Get reminds
    fetch(`${process.env.API_URL}/remind/${this.props.user.info._id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({reminds: '', remindsLoaded: true});
        window.location.replace(`Error500`);
        return;
      }

      response.json().then(data => {
        this.setState({reminds: data.reminds, remindsLoaded: true});
      });
    }).catch(err => {
      throw new Error(err);
    });
    // get Tem
    fetch(`${process.env.API_URL}/forum/structure`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({Forum: '', Loaded: true});
        return;
      }

      response.json().then(data => {

        this.setState({Forum: data.data.structure, Loaded: true})
      }).catch(err => {
        throw new Error(err);
      })
    }).catch(err => {
    });
    // Get news
    fetch(`${process.env.API_URL}/news/preview`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        response.json().then((data) => {
          let news = [];
          if (data.message.type === "error") {
            this.setState({remindsLoaded: true, error:data.message.message});
            return;
          }

          let months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Ноября', 'Декабря'];
         
          if (response.status !== 412) {
            news = data.data.map(msg => {
              return {
                date: new Date(msg.date).getDate()+" "+ String(months[new Date(msg.date).getMonth()+1]),
                name: msg.title,
                text: msg.text,
                image: msg.img,
                key:msg.link,
                link : msg.link,
                time: msg.date.split(':')[0].split('T')[1]+":"+msg.date.split(':')[1]
              };
            });
          }
          this.setState({remindsLoaded: true, News: news});
        });
      }).catch((response) => {});
  }

  updatePosts = () => {
    // Get posts
    fetch(`${process.env.API_URL}/post/${this.props.user.info._id}/sortedByDate`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({posts: '', remindsLoaded: true});
        return;
      }

      response.json().then(data => {
        let posts = response.status === 200 ? data.data : data.message.message;
        this.setState({posts: posts, remindsLoaded: true})
      }).catch(err => {
        throw new Error(err);
      })
    }).catch(err => {
    });

    // DESLENGHT
    fetch(`${process.env.API_URL}/post/${this.props.user.info.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      response.json().then(data => {
        this.setState({mypost: data.data});
      });
    });
  };

  getPostTextRef = (node) => { this._postText = node };

  AddPost = () => {
    if (this._postText.value.trim() === "") return;

    fetch(`${process.env.API_URL}/post/${this.props.user.info._id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `post=${JSON.stringify({text: this._postText.value})}`
    }).then(response => {
      this.updatePosts();
      this._postText.value = '';
    });
    this.setState({ImageNewPost:[]});
  };

  // File dialogs
  FileUpload = () => {
    if (this.state.ImageNewPost > 10) {
      alert("Записать можно не более 10 фотографий");
      return;
    }

    let copyImages  = this.state.ImageNewPost.slice();
    let file = document.querySelector('#myFile').files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      copyImages.push(reader.result);
      this.setState({ImageNewPost: copyImages});
    };

    console.log(file);
    if (file) reader.readAsDataURL(file);
  };

  render() {
    const deletePost = (id) => {
      fetch(`${process.env.API_URL}/post/${this.props.user.info._id}?postId=${id}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }).then(response => {
        if (response.status === 404 || response.status ===  500) return;
        else if (response.status === 200) this.updatePosts();
      }).catch(err => {
        console.log(err);
      });
    };

    const stickPost = (id) => {
      fetch(`${process.env.API_URL}/post/${this.props.user.info._id}/sticky/${id}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }).then(response => {
        if (response.status === 404 || response.status ===  500) return;
        else if (response.status === 200) this.updatePosts();
      }).catch(err => {
        console.log(err);
      });
    };

    let myposts;
    if(this.state.mypost && this.state.mypost.posts && this.state.mypost.posts.length > 0)
      myposts = this.state.mypost.posts.map((item, index) => {
        let diff = moment(new Date()).diff(moment(new Date(item.date)), 'days');
          let date = moment(new Date(item.date)).format('DD.MM.YYYY');
          return (<Entry key={index}
                         name={this.state.mypost.user.fullName}
                         img={this.props.user.info.avatar}
                         image={item.images}
                         date={diff < 2 ? moment(new Date().fromNow(new Date(item.date))).fromNow() : date}
                         post={item.comments}
                         id={item.id}
                         text={item.text}
                         Myentry={true}
                         isSticky={item.isSticky || false}
                         onDeletePost={() => {deletePost(item.id); }}
                         onStickyPost={() => {stickPost(item.id); }}
          />)
        }
      );

    let posts;
    if (!this.state.remindsLoaded)
      posts = (<h4 className="NaN">Загрузка</h4>);
    else if (this.state.remindsLoaded && Array.isArray(this.state.posts) && this.state.posts.length > 0)
      posts = this.state.posts.map((item, index) => {
        let diff = moment(new Date()).diff(moment(new Date(item.post.date)), 'days');
        let date = moment(new Date(item.post.date)).format('DD.MM.YYYY');
        return (<Entry key={index}
                name={item.user.fullName}
                img={item.user.avatar}
                image={item.post.images}
                date={diff < 2 ? moment(new Date().fromNow(new Date(item.post.date))).fromNow() : date}
                post={item.post.comments}
                id={item.post.id}
                text={item.post.text}
                Myentry={false}
        />)

        }
      );

    //let postImageUpload = (<input id="myFile" type="file" className={"home__mypost__addimage"} onChange={() => {this.FileUpload()}}/>);
    let postImageUpload;

    return (
      <Container style={{marginTop: '25px'}}>
        <Row>
          <Col style={{padding: '0'}} xs="12" lg="8"><ProfileInformation count={this.state.Forum.length} /></Col>
          <Col style={{paddingRight: '0'}} xs="12" lg="4"><ProfilePicture isEditable={true} className="profile__picture-block d-none d-lg-block" /></Col>
        </Row>

        <Row>
          <Col style={{padding: '0'}} xs="12">
            <ProfileTips>
              <ProfileTipItem iconPath={CalendarTip} title="Заполнить дневник" link="/Treatment" text="зафиксировать изменения, посмотреть записи врача" act="заполнить"/>
              <ProfileTipItem iconPath={PlanTip} title="План лечения" link="/Treatment-Plan?tab=one" text="посмотреть ближайшие назначения" act="посмотреть"/>
              <ProfileTipItem iconPath={SpecialistTip} title="Консультация" link="/questions" text="задайте вопрос специалисту" act="написать"/>
            </ProfileTips>
          </Col>
        </Row>

        <Row>
          <Col style={{paddingLeft: '3px'}} md="12" xl={"8"} className="home">
            <TabControlHome defaultTabID="one" >
             <TabList>
                <Tab tabFor="one">Мои записи</Tab>
                <Tab tabFor="two">Новости</Tab>
             </TabList>
               <TabPanel tabId="one">
                <Card cardShadow='no-shadow' className={"home__mypost"}>
                   <p>Новая запись</p>
                     <div className={"home__mypost__row"}>
                     <textarea ref={this.getPostTextRef} id={"sub"} className={"home__mypost__area"}/>
                     <div className={"home__mypost__image"}>
                       {
                         this.state.ImageNewPost.length > 0 ?
                           this.state.ImageNewPost.map(item => (<img alt='post' key={item.index} src={item} className={"home__mypost__image__img"} />)) : <div/>
                       }
                     </div>
                   <Button className='button--primary home__add-post-button' text='Создать' onClick={this.AddPost} />
                   {postImageUpload}
                 </div>
               </Card>
              {myposts}
              </TabPanel>

             <TabPanel tabId="two">
                 <PostForm>
                   {posts}
                 </PostForm>
             </TabPanel>
           </TabControlHome>
          </Col>
          <Col md={"12"} xl={"4"} className="profile__Left__Reminds d-none d-lg-flex">
            <div className={"d-block d-md-block  d-xl-none profile__Left__Reminds__header"}>Мои напоминания</div>
            { !this.state.remindsLoaded ? (<h4 className="NaN">Загрузка...</h4>) : this.state.remindsLoaded && this.state.reminds && this.state.reminds.length > 0 ?
              this.state.reminds.map((item, index) => (( item.isPlan === true? <Reminder key={index} date={item.date} type={item.type} text={item.title} id={item.id} className={"Remind__Consult"} /> : <Reminder key={index} date={item.date} type={item.type} text={item.text} id={item.id}  />))) :
                (<h4 className="NaN">Нет напоминаний</h4>)
            }
          </Col>
        </Row>
      </Container>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(Home);
