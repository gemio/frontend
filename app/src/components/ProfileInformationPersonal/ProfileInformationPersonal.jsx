import React from 'react';
import {Icon} from 'react-fa';
import { connect } from 'react-redux';
import AvatarCircle from '../AvatarCircle';
import InputSimple from '../InputSimple';
import './ProfileInformationPersonal.scss';
import UserAction from "../../actions/UserAction";
import Button from "../Button/Button";

class ProfileInformationPersonal extends React.Component {
  state = {
    edit: false
  };

  render() {
    const editStatus = () => {
      this.setState({edit: true});
    };

    const applyStatus = () => {
      let status = document.getElementById('status');
      fetch(`${process.env.API_URL}/user/${this.props.user.info._id}/status`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `newStatus=${status.value || '!@#CLEAR!@#'}`
      }).then(response => {
        if (response.status === 404 || response.status ===  500) {
          console.log('Сервер не отвечает');
          closeStatus();
          return;
        }

        response.json().then(data => {
          if (response.status === 200) {
            this.props.onUpdateStatus(data.data);
            if (this.props.handleChange) this.props.handleChange();
          }
          closeStatus();
        }).catch(err => {
          console.warn(err);
          closeStatus();
        });
      }).catch(err => {
        throw new Error(err);
      });
    };

    const closeStatus = () => {
      this.setState({edit: false});
    };

    let icons;
    if (this.state.edit)
      icons = (
        <div className='profile-info__status-icons'>
          <Icon className='profile-info__edit-icon' onClick={applyStatus} name="check"/>
          <Icon className='profile-info__edit-icon' onClick={closeStatus} name="times"/>
        </div>
      );
    else if (this.props.isEditable)
      icons = (
        <div className='profile-info__status-icons'>
          <Icon onClick={editStatus} name="pencil"/>
        </div>
      );

    return (
      <div className={this.props.className}>
        <AvatarCircle img={this.props.avatar ? this.props.avatar : this.props.user.info.avatar} className='d-lg-none profile-avatar--mobile'/>
        <div className="profile-info__fullname">{this.props.name}</div>
        <div className="profile-info__status">
          <span id='profile-status-span'>{this.state.edit ? '' : this.props.status}</span>

          <InputSimple id="status" value={this.props.status} className={'profile-info__change-status ' + (this.state.edit ? '' : 'profile-info__change-status--hidden')}/>
          {icons}
        </div>

        <div style={{display: !this.props.userID ? 'none': 'block'}} className='profile-info__actions d-lg-none'>
          <Button onClick={() => {window.location.href = `/dialog/${this.props.userID}`}}
                  className='button--primary-small-round'
                  text={'Отправить сообщение'}/>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onUpdateStatus(user) {
      dispatch(UserAction.updateStatus(user));
    }
  })
)(ProfileInformationPersonal);