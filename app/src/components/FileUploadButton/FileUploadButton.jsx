import React from 'react';
import Button from '../Button';
import './FileUploadButton.scss';

export default class FileUploadButton extends React.Component {
  render() {
    return (
      <label className={'file-upload ' + (this.props.className || '')}>
        <Button type="submit" onClick={this.props.onClick} className={this.props.buttonClass} text={this.props.text}/>
        <input onChange={this.props.uploadHandler} type="file" name={this.props.fileName} />
      </label>
    );
  }
}
