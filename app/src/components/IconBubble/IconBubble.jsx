import React, {Component} from 'react';
import {Icon} from 'react-fa';
import './IconBubble.scss';

export default class IconBubble extends Component {
  render() {
    return (
      <div onClick={this.props.onClick} style={{background: this.props.background, border: `1px solid ${this.props.border}`}} className={"bubble-icon " + (this.props.className || '')}>
        {this.props.iconName ? (<Icon style={{color: this.props.border}} name={this.props.iconName}/>) : <img src={this.props.imgName} alt="icon" />}
        <div className='bubble-icon__text'>{this.props.text}</div>
      </div>
    );
  }
}
