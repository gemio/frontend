import React from 'react';

class SlideMenuNav extends React.Component {
  render() {
    return (
      <ul className={(this.props.className || '') + ' slide-menu__nav'}>
        {this.props.children}
      </ul>
    );
  }
}

export default SlideMenuNav;