import React from 'react';
import {NavLink} from 'react-router-dom';
import { connect } from 'react-redux';
import './LoginForm.scss';

import Button from '../Button';
import Form from '../Form';
import Input from '../Input';
import LoginSocialGroup from "../LoginSocialGroup/LoginSocialGroup";

const UserAction = require('../../actions/UserAction');

class LoginForm extends React.Component {
  componentWillMount() {
    if (this.props.user.authorized)
      window.location.href = '/home';
  }

  render() {
    let apiAuth = () => {
      let user = {
        login: document.getElementById('login').value,
        password: document.getElementById('password').value
      };
      this.props.onAuthorize(user);
    };

    return (
      <Form submitCallback={apiAuth} className="login__form">
        <div className="login__error">{this.props.message.message}</div>
        <Input required={true} id="login" className="login__input" type="text" labelText="Email"/>
        <Input required={true} id="password" className="login__input" type="password" labelText="Пароль"/>
        <NavLink className="login__remind-password" to="./ForgotPassword">Забыли пароль?</NavLink>
        <Button type="submit" text="ВОЙТИ" className="button--primary login__submit"/>

        <div className='login__or' style={{margin: '10px 0', color: '#9da1a7', fontSize: '14px'}}>или</div>

        <LoginSocialGroup>
          <Button isIcon={true} iconName="google-plus" type="submit" className="button--primary"/>
          <Button isIcon={true} iconName="vk" type="submit" className="button--primary"/>
          <Button isIcon={true} iconName="facebook" type="submit" className="button--primary"/>
        </LoginSocialGroup>
      </Form>
    );
  }
}

export default connect(
  state => ({
    user: state.user,
    message: state.message
  }),
  dispatch => ({
    onAuthorize: (user) => {
      dispatch(UserAction.default.authorize(user));
    }
  })
)(LoginForm);
