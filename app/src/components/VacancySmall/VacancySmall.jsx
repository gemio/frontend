import React from 'react';
import './VacancySmall.scss';

export default class VacancySmall extends React.Component {
  render() {
    return (
      <a href={this.props.href} className={"VacancySmall__d"}>
        <div className={"VacancySmall"}>
          <div className={"VacancySmall--flex"}>
            <div>
              <h3 className={"VacancySmall__header"}>{this.props.name}</h3>
            </div>
            <div className={"VacancySmall__Right"}>
              <h4 className={"VacancySmall__money"}>{this.props.textot}</h4>
            </div>
          </div>
            <p className={"VacancySmall__name"}>{this.props.tag}</p>
        </div>
      </a>
    );
  }
}
