import React from 'react';
import Card from '../Card';
import './VacancyId.scss';
import Button from "../Button/Button";
import AvatarCircle from "../AvatarCircle/AvatarCircle";

export default class VacancyId extends React.Component {
  render() {
    let adminButtons;
    if (this.props.isModeration) {
      adminButtons = (
        <div className='vacancy__actions'>
          <Button onClick={this.props.onAccepted} className='button--primary-ghost vacancy__button' text='Принять'/>
          <Button onClick={this.props.onDeleted} className='button--orange-ghost vacancy__button' text='Отклонить'/>
        </div>
      )
    }

    return (
      <Card cardShadow='no-shadow' className="vacancy">
        {this.props.isModeration ?
          <div className="vacancy__profile">
            <AvatarCircle img={this.props.userInfo.avatar}/>
            <a href={`/id/${this.props.userID}`} className="vacancy__full-name">{this.props.userInfo.fullNameUser}</a>
          </div> : null
        }

        <div className="vacancy__title">{this.props.header}</div>
        <div className="vacancy__price"><strong>Зарплата: </strong>{this.props.price}</div>
        <div className="vacancy__person"><strong>Работодатель: </strong>{this.props.person}</div>

        <div className="vacancy__Conditions">
          <div className="vacancy__Conditions__header"><strong>Описание:</strong></div>
          <div className="vacancy__Conditions__text">{this.props.text}</div>
          <div className="vacancy__Conditions__work"><strong>Опыт работы: </strong>{this.props.exp}</div>
        </div>

        <div className="vacancy__Address">
          <div className="vacancy__Conditions__header"><strong>Контактная информация:</strong></div>
          <div className="vacancy__Address__adr"><strong>Адрес: </strong>{this.props.city + " "+ this.props.adr}</div>
          <div className="vacancy__Address__adr"><strong>Телефон: </strong>{this.props.phone}</div>
        </div>

        {adminButtons}
      </Card>
    );
  }
}