import React from 'react';
import Card from '../Card';
import './IndexCommunicationCard.scss'

export default class IndexCommunicationCard extends React.Component {
  render() {
     return (
      <Card cardShadow='blured' className={"Communication__block_Card " + (this.props.className || '')}>
        <h1 className="Communication__block_Card__header">{this.props.text}</h1>
        <p className="Communication__block_Card__text">{this.props.name}</p>
        <p className="Communication__block_Card__text">{this.props.tag}</p>
        <a className="Communication__block_Card__href" href={this.props.nav}>{this.props.log}</a>
      </Card>
    );
  }
}
