import React from 'react';
import GemoLogo from '../GemoLogo';
import FooterNav from '../FooterNav';
import FooterNavItem from '../FooterNavItem';
import { connect } from 'react-redux';

import sponsor__fasie from '../../img/sponsors/fasie.png';

// Bootstrap
import Col from '../Col';
import Container from '../Container';
import Row from '../Row';

import "./Footer.scss";

class Footer extends React.Component {
  render() {
    return (
    	<Container fluid={true}>
    		<Row className={"footer " + (this.props.isAuthPage ? 'footer--auth-page' : '')}>
    			<Col lg="7" xs="12">
    				<FooterNav>
              <FooterNavItem link="/help" text="Помощь"/>
            </FooterNav>
    			</Col>
    			<Col lg="5" xs="12" className="footer__logo-column">
            <GemoLogo redirect='/' isPrimary={false}/>
    			</Col>
    		</Row>
        <Row>
          <Col xs="12"  className="footer__logo-column footer__sponsor">
            <div className="footer__info footer__address">
              Общество с ограниченной ответственностью «Гемонет»<br/>
              193232, г. Санкт-Петербург ул. Крыленко, д.31, квартира 12<br/>
              Тел. +79043310100<br/>
              e-mail: gemofilik@yandex.ru<br/>
            </div>
            
            <a href="http://www.fasie.ru/" className="footer__info footer__link" target="_blank">
              <img src={sponsor__fasie} className="footer__sponsor-logo" alt="Фонда содействия развитию малых форм предприятий в научно-технической сфере" />
              <span className="footer__sponsor-text">Сервис создан при поддержке Фонда содействия развитию малых форм предприятий в научно-технической сфере</span>
            </a>
          </Col>
        </Row>
    	</Container>
    )
	}
}
export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({})
)(Footer);
