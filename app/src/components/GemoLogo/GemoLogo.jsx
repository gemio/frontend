import React from 'react';
import './GemoLogo.scss';
import {Link} from 'react-router-dom';

// Logo image
import imgPrimary from '../../img/logo/logo-primary.png';
import imgWhite from '../../img/logo/logo-white.png';

export default class GemoLogo extends React.Component {
  render() {
    let logo = this.props.isPrimary ? imgPrimary : imgWhite;

    return (
      <Link className={'logo logo' + (this.props.isPrimary ? '--primary ' : '--white ') + (this.props.className || '')} to={this.props.redirect || '/'}>
        <img src={logo} alt="gem-logo"/>

        <div className="logo__text">
          <div className="logo__title">Гемофилик</div>
          <div className="logo__description">сообщество больных гемофилией</div>
        </div>
      </Link>
    );
  }
}
