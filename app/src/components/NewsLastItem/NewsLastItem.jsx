import React from 'react';
import { Link } from 'react-router-dom';
import './NewsLastItem.scss';

export default class NewsLastItem extends React.Component {
  render() {
    return (
      <a href={this.props.href || '#'}  className="news-last__item">
        <div className="news-last__title">{this.props.title}</div>

        <div className="news-last__dat">
          <div className="news-last__date">{this.props.date}</div>
          <div className="news-last__time">{this.props.time}</div>
        </div>
      </a>
    );
  }
}
