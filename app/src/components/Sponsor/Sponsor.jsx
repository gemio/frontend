import React from 'react';
import { Link } from 'react-router-dom';
//
import sponsor__one from '../../img/sponsors/sponsor__one.png';
import sponsor__two from '../../img/sponsors/sponsor__two.png';
import sponsor__three from '../../img/sponsors/sponsor__three.png';
import sponsor__four from '../../img/sponsors/sponsor__four.png';
import sponsor__five from '../../img/sponsors/sponsor__five.png';
import sponsor__six from '../../img/sponsors/sponsor__six.png';
import sponsor__seven from '../../img/sponsors/sponsor__seven.png';
import egg from '../../img/shapes/shape-egg.svg';
//
import "./Sponsor.scss";
//
class Sponsor extends React.Component {
  render() {
    return (
    	<div>
        <div  className="sponsor__egg">
          <img alt="gem-egg" src={egg} className="sponsor__egg__eg" />
        </div>
    		<div className="About__all__block_sponsor__line1">
          <Link to="https://www.wfh.org/"> <img alt="gem-sponsor" src={sponsor__one}   className="About__all__block_sponsor__line1__image__1"/> </Link>
          <Link to="http://www.baxter.com.ru/"> <img alt="gem-sponsor" src={sponsor__two}   className="About__all__block_sponsor__line1__image__2"/> </Link>
          <Link to="https://www.cslbehring.com/"> <img alt="gem-sponsor" src={sponsor__three} className="About__all__block_sponsor__line1__image__3"/> </Link>
          <Link to="https://www.novosevenrt.com/"> <img alt="gem-sponsor" src={sponsor__four}  className="About__all__block_sponsor__line1__image__4"/> </Link>
        </div>
        
        <div className="About__all__block_sponsor__line2">
          <Link to="https://www.nationalgeographic.org/grants/">  <img alt="gem-sponsor" src={sponsor__five}  className="About__all__block_sponsor__line2__image__1"/></Link>
          <Link to="http://www.octapharma.com/en/healthcare-professionals/products-therapies/haematology.html">  <img alt="gem-sponsor" src={sponsor__six} className="About__all__block_sponsor__line2__image__2"/></Link>
          <Link to="https://www.hemophilia.ru/"><img alt="gem-sponsor" src={sponsor__seven} className="About__all__block_sponsor__line2__image__3"/></Link>
        </div>
      </div>
    );
  }
}

export default Sponsor;
