import React from 'react';
import { Link } from 'react-router-dom';
import './FooterNavItem.scss';

export default class FooterNavItem extends React.Component {
  render() {
    return (
      <li className="footer__nav-item"><Link to={this.props.link} className="footer__nav-link">{this.props.text}</Link></li>
    );
  }
}
