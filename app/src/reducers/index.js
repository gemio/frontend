import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools  } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import user from './User';
import message from './Message'
import notification from './Notification';
import slideMenu from './Slidemenu';

window.io = require('socket.io-client');
const reducers = combineReducers({
  user: user,
  message: message,
  notification: notification,
  slideMenu: slideMenu
});

const cachedInfo = window.localStorage.getItem('redux') ? JSON.parse(window.localStorage.getItem('redux')) : {};
if (cachedInfo && cachedInfo.user && cachedInfo.user.authorized) {
  window.socket = window.io(process.env.LIVE_HOST, {
    path: process.env.LIVE_PATH,
    query: {
      token: cachedInfo.user.info._id
    },
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax: 5000,
    reconnectionAttempts: Infinity,
    pingInterval: 2000,
    pingTimeout: 5000
  });
}

const composeEnhancers = composeWithDevTools({});
const store = createStore(reducers, cachedInfo, composeEnhancers(applyMiddleware(thunk)));
store.subscribe(() => {
  let state = Object.assign({}, store.getState());
  delete state.message;
  window.localStorage.setItem('redux', JSON.stringify(state));
});

export default store;
