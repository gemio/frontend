const initialState = {
  authorized: false,
  info: {}
};

export default function userReducer(state = initialState, action) {
  
    if(action.type ==='AUTH_REQUESTED')
      return Object.assign({}, state, {loading: true});
    if(action.type ==='AUTH_SUCCESS')
      return Object.assign({}, state, action.payload);
    if(action.type ==='AUTH_NEED_DOCUMENTS')
      return Object.assign({}, state, action.payload);
    if(action.type === 'AUTH_FAIL')
      return Object.assign({}, state, {error: action.payload});
    if(action.type === 'UPDATE_USER_REQUESTED')
      return Object.assign({}, state, {loading: true});
    if(action.type === 'UPDATE_USER_FAILED')
      return Object.assign({}, state, {error: action.payload});
    if(action.type === 'LOG_OUT')
      return initialState;

  return state;
}
