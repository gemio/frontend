const initialState = {
  open: true
};

export default function slideMenuReducer(state = initialState, action) {
  if (action.type === 'HIDE_MENU')
    return {open: false}
  else if (action.type === 'UNHIDE_MENU')
    return {open: true}
  else
    return state;
}
