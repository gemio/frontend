const initialState = {
  type: '',
  message: ''
};

export default function messageReducer(state = initialState, action) {
    if(action.type === 'SET_MESSAGE')
      return action.payload;
  return state;
}
